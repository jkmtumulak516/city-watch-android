package com.sofeng.citywatch.setup;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.sofeng.citywatch.R;
import com.sofeng.citywatch.dashboard.DashboardActivity;
import com.sofeng.citywatch.model.AvatarEnum;
import com.sofeng.citywatch.util.ImageLoader;

public class SetupActivity
        extends AppCompatActivity
        implements ISetupView{

    private static final int REQUEST_CODE_AUTOCOMPLETE = 9;

    private ISetupPresenter setupPresenter;

    private TextView lblChangeAvatar;

    private AvatarEnum selectedAvatar;

    private Dialog avatarSelect;

    private ImageLoader imageLoader;

    private ImageView imgSelectedAvatar;

    private TextView location_input;

    private RelativeLayout location_selected;

    private TextView txtLocationSelected;

    private ImageView btnLocationDeselect;

    private EditText txtDisplayName;

    /**
     * Holds the bounds of the Philippines
     */
    private LatLngBounds BOUNDS_PH = new LatLngBounds(
            new LatLng(4.6145711, 119.6272661), new LatLng(19.966096, 124.173694));

    /**
     * Array of the avatar imageViews' id
     */
    private final int selectAvatarIds[] = {R.id.avatar1, R.id.avatar2, R.id.avatar3, R.id.avatar4,
            R.id.avatar5, R.id.avatar6, R.id.avatar7, R.id.avatar8};

    /**
     * Holds the current city that is selected
     */
    private Place place = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setup);

        setupPresenter = new SetupPresenter(this);

        selectedAvatar = AvatarEnum.Avatar1;

        imgSelectedAvatar = (ImageView) findViewById(R.id.imgSelectedAvatar);

        lblChangeAvatar = (TextView) findViewById(R.id.lblChangeAvatar);

        location_input = (TextView) findViewById(R.id.location_input);

        location_selected = (RelativeLayout) findViewById(R.id.location_selected);

        txtLocationSelected = (TextView) findViewById(R.id.txtLocationSelected);

        btnLocationDeselect = (ImageView) findViewById(R.id.btnDeselectLocation);

        txtDisplayName = (EditText) findViewById(R.id.txtDisplayName);

        lblChangeAvatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               showAvatarSelectDialog();
            }
        });

        btnLocationDeselect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toggleInputLocationLayout();
                place = null;
            }
        });

        location_input.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openPlacesAutocompleteActivity();
            }
        });

        imageLoader = ImageLoader.getInstance();

        loadSelectedImage();

        initializeDialog();

        setupPresenter = new SetupPresenter(this);

    }

    /**
     * Initializes the selectAvatar dialog
     */
    private void initializeDialog(){
        avatarSelect = new Dialog(this,android.R.style.Theme_Material_Dialog);
        avatarSelect.setContentView(R.layout.dialog_avatar_select);
        avatarSelect.setCanceledOnTouchOutside(true);
        avatarSelect.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        for(int i=0; i < selectAvatarIds.length; i++){
            imageLoader.loadImageFromUrl(this, AvatarEnum.getAvatar(i+1).url, (ImageView) avatarSelect.findViewById(selectAvatarIds[i]));
            avatarSelect.findViewById(selectAvatarIds[i]).setOnClickListener(new AvatarSelectListener());
        }

    }

    /**
     * Loads the selected image in the dialog
     */
    private void loadSelectedImage(){
        imageLoader.loadImageFromUrl(this,selectedAvatar.url, imgSelectedAvatar);
    }

    /**
     * Sets the textView to the selected location and toggles the explore
     * @param location
     */
    private void setSelectedLocation(String location){
        toggleSelectedLocationLayout();
        txtLocationSelected.setText(location);
    }

    /**
     * Sets the selected layout visible
     */
    private void toggleSelectedLocationLayout(){
        location_selected.setVisibility(View.VISIBLE);
        location_input.setVisibility(View.GONE);
    }

    /**
     * Set the location layout visible
     */
    private void toggleInputLocationLayout(){
        location_selected.setVisibility(View.GONE);
        location_input.setVisibility(View.VISIBLE);
        txtLocationSelected.setText("");
    }

    /**
     * Displays the incomplete error message
     */
    private void displayIncompleteInputMessage(){
        Toast.makeText(this, "Please fill in all the fields.", Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.setup_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case R.id.menu_next:
                if (place != null && !TextUtils.isEmpty(txtDisplayName.getText())){
                    setupPresenter.updateAccount(txtDisplayName.getText().toString(),
                            place, selectedAvatar);
                    gotoMain(); // <-- temp
                }else {
                    displayIncompleteInputMessage();
                }
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    /**
     * Shows the avatar select dialog
     */
    private void showAvatarSelectDialog() {
        avatarSelect.show();
    }

    /**
     * Closes the avatar select dialog
     */
    public void hideAvatarSelectDialog() {
        avatarSelect.hide();
    }

    @Override
    public void gotoMain() {
        Intent i = new Intent(this, DashboardActivity.class);
        startActivity(i);
        finish();
    }

    /**
     * Change the selected avatar in setup
     * @param avatar
     */
    private void changeSelectedAvatar(AvatarEnum avatar) {
        selectedAvatar = avatar;
        loadSelectedImage();
        hideAvatarSelectDialog();

    }




    /**
     * Opens the places autocomplete as a new activity
     */
    private void openPlacesAutocompleteActivity() {
        try {

            // The autocomplete activity requires Google Play Services to be available. The intent
            // builder checks this and throws an exception if it is not the case.
            Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                    .setFilter(new AutocompleteFilter.Builder().setTypeFilter(AutocompleteFilter.TYPE_FILTER_CITIES).build())
                    .setBoundsBias(BOUNDS_PH)
                    .build(this);
            startActivityForResult(intent, REQUEST_CODE_AUTOCOMPLETE);
        } catch (GooglePlayServicesRepairableException e) {
            // Indicates that Google Play Services is either not installed or not up to date. Prompt
            // the user to correct the issue.
            GoogleApiAvailability.getInstance().getErrorDialog(this, e.getConnectionStatusCode(),
                    0 /* requestCode */).show();
        } catch (GooglePlayServicesNotAvailableException e) {
            // Indicates that Google Play Services is not available and the problem is not easily
            // resolvable.
            String message = "Google Play Services is not available: " +
                    GoogleApiAvailability.getInstance().getErrorString(e.errorCode);

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == REQUEST_CODE_AUTOCOMPLETE){
            if(resultCode == RESULT_OK){
                // Get the user's selected place from the Intent.
                Place place = PlaceAutocomplete.getPlace(this, data);
                setSelectedLocation(place.getAddress().toString());
                this.place = place;
            }
        }
    }

    /**
     * Class that handles the click event of the avatars in selectAvatar dialog
     */
    class AvatarSelectListener implements View.OnClickListener{

        @Override
        public void onClick(View view) {
            AvatarEnum avatarModel;
            Log.d("debug", "Clicked " + view.getId());
            switch (view.getId()) {
                case R.id.avatar1:
                    avatarModel = AvatarEnum.Avatar1;
                    break;
                case R.id.avatar2:
                    avatarModel = AvatarEnum.Avatar2;
                    break;
                case R.id.avatar3:
                    avatarModel = AvatarEnum.Avatar3;
                    break;
                case R.id.avatar4:
                    avatarModel = AvatarEnum.Avatar4;
                    break;
                case R.id.avatar5:
                    avatarModel = AvatarEnum.Avatar5;
                    break;
                case R.id.avatar6:
                    avatarModel = AvatarEnum.Avatar6;
                    break;
                case R.id.avatar7:
                    avatarModel = AvatarEnum.Avatar7;
                    break;
                case R.id.avatar8:
                    avatarModel = AvatarEnum.Avatar8;
                    break;
                default:
                    avatarModel = AvatarEnum.Avatar1;
            }

            changeSelectedAvatar(avatarModel);
        }
    }


}
