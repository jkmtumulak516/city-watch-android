package com.sofeng.citywatch.setup;


import com.sofeng.citywatch.model.AvatarEnum;


/**
 * Created by JKMT on 07/19/2017.
 */

public interface ISetupData {

    /**
     * Updates the users initial empty data to that of the parameters.
     * @param userName User's display name.
     * @param locationId User's initial location id.
     * @param locationName User's initial location name.
     * @param avatar User's selected avatar.
     */
    void updateUser(String userName, String locationId, String locationName, AvatarEnum avatar);
}
