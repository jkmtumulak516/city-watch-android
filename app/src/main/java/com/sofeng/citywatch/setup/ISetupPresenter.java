package com.sofeng.citywatch.setup;

import com.google.android.gms.location.places.Place;
import com.sofeng.citywatch.model.AvatarEnum;

/**
 * Created by JKMT on 07/19/2017.
 */

public interface ISetupPresenter {

    /**
     * Handles the updating of the account
     * @param userName
     * @param location
     * @param avatar
     */
    void updateAccount(String userName, Place location, AvatarEnum avatar);
}
