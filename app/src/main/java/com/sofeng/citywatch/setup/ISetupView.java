
package com.sofeng.citywatch.setup;


/**
 * Created by JKMT on 07/19/2017.
 */
public interface ISetupView {

    /**
     * Goes to Main Activity
     */
    void gotoMain();

}
