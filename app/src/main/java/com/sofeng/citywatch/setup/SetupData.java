package com.sofeng.citywatch.setup;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.sofeng.citywatch.model.AvatarEnum;
import com.sofeng.citywatch.model.CityModel;

/**
 * Created by JKMT on 07/19/2017.
 */

public class SetupData implements ISetupData {

    private DatabaseReference rootReference;

    public SetupData() {

        rootReference = FirebaseDatabase.getInstance().getReference();
    }

    @Override
    public void updateUser(String userName, String cityId, String cityName, AvatarEnum avatar) {

        DatabaseReference userReference = rootReference.child("users").child(FirebaseAuth.getInstance().getCurrentUser().getUid());

        CityModel city = new CityModel(cityId, cityName);

        userReference.child("displayName").setValue(userName);
        userReference.child("currentCity").setValue(city);
        userReference.child("cities").child(cityId).setValue(city);
        userReference.child("avatar").setValue(avatar.id);
    }
}
