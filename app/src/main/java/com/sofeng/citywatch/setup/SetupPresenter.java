package com.sofeng.citywatch.setup;

import com.google.android.gms.location.places.Place;
import com.sofeng.citywatch.model.AvatarEnum;

/**
 * Created by JKMT on 07/19/2017.
 */

public class SetupPresenter implements ISetupPresenter {

    private ISetupView setupView;
    private ISetupData setupData;

    public SetupPresenter(ISetupView setupView) {

        this.setupView = setupView;

        setupData = new SetupData();
    }

    @Override
    public void updateAccount(String userName, Place location, AvatarEnum avatar) {
        setupData.updateUser(userName, location.getId(), location.getAddress().toString(), avatar);
        setupView.gotoMain();
    }
}
