package com.sofeng.citywatch.model.filter;

import com.sofeng.citywatch.model.RankEnum;
import com.sofeng.citywatch.model.TagEnum;

import java.io.Serializable;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by JKMT on 09/15/2017.
 */

public class PostFilter implements IFilter, Serializable{

    private final String cityId;
    private String userId;
    private boolean followingFlag;
    private Set<TagEnum> tags;
    private int minRank;
    private int maxRank;

    public String getCityId() {
        return cityId;
    }

    public String getUserId() {
        return userId;
    }

    public boolean isFollowingFlag() {
        return followingFlag;
    }

    public int getMinRank() {
        return minRank;
    }

    public int getMaxRank() {
        return maxRank;
    }

    public PostFilter(String cityId) {

        this.cityId = cityId;

        userId = "";
        followingFlag = false;
        minRank = -1;
        maxRank = 6;

        tags = new HashSet<>();
    }

    public PostFilter setUserId(String userId) {

        this.userId = userId;

        return this;
    }

    public PostFilter addTag(TagEnum tag) {

        tags.add(tag);

        return this;
    }

    public PostFilter addTags(Set<TagEnum> tags) {

        this.tags.addAll(tags);

        return this;
    }

    public PostFilter setTags(Set<TagEnum> tags){
        this.tags = tags;

        return  this;
    }

    public PostFilter setMinRank(int minRank) {

        this.minRank = minRank;

        return this;
    }

    public PostFilter setMaxRank(int maxRank) {

        this.maxRank = maxRank;

        return this;
    }

    public PostFilter setRankRange(int minRank, int maxRank) {

        this.minRank = minRank;
        this.maxRank = maxRank;

        return this;
    }

    public PostFilter setFollowingFlag(boolean followingFlag) {

        this.followingFlag = followingFlag;

        return this;
    }

    public Set<TagEnum> getTags(){
        return tags;
    }

    @Override
    public Map<String, String> toMap() {

        Map<String, String> map = new LinkedHashMap<>();

        map.put("userId", userId);
        map.put("cityId", cityId);
        map.put("followingFlag", followingFlag ? "1" : "0");
        map.put("minRank", Integer.toString(minRank));
        map.put("maxRank", Integer.toString(maxRank));

        int i = 0;

        if (tags != null) {
            for (TagEnum tag : this.tags)
                map.put("tags[" + (i++) + "]", tag.string);
        }

        return map;
    }
}
