package com.sofeng.citywatch.model;

/**
 * Created by JKMT on 09/19/2017.
 */

public class CommentModel {

    public String id;
    public String userId;
    public String postId;

    public double timestamp;
    public String text;
}
