package com.sofeng.citywatch.model;

import java.util.Map;

/**
 * Created by JKMT on 09/07/2017.
 */

public class RankProgressModel {

    public double progress;
    public double floor;
    public double ceil;

    /**
     * Converts a mapped JSON response into a RankProgressModel.
     * @param map Json response that represents a single RankProgressModel.
     * @return A RankProgressModel.
     */
    public static RankProgressModel convertSingle(Map map) {

        RankProgressModel rankProgressModel = new RankProgressModel();

        rankProgressModel.progress = (double) map.get("progress");
        rankProgressModel.floor = (double) map.get("floor");
        rankProgressModel.ceil = (double) map.get("ceil");

        return rankProgressModel;
    }
}
