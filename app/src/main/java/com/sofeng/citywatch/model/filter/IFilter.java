package com.sofeng.citywatch.model.filter;

import java.util.Map;

/**
 * Created by JKMT on 09/15/2017.
 */

public interface IFilter {

    /**
     *
     * @return
     */
    Map<String, String> toMap();
}
