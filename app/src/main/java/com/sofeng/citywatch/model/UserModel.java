package com.sofeng.citywatch.model;

import android.annotation.SuppressLint;
import android.util.LongSparseArray;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.GenericTypeIndicator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by JKMT on 07/19/2017.
 */

public class UserModel {

    public String displayName;
    public int rank;
    public RankProgressModel rankProgress;
    public int avatar;
    public CityModel currentCity;
    public Map<String, CityModel> cities;
    // first second third val 1,2,3
    public Map<String, Long> achievements;

    public String id;

    /**
     * Converts a mapped JSON response into a UserModel.
     * @param map Json response that represents a single UserModel.
     * @return A UserModel.
     */
    @SuppressLint("UseSparseArrays")
    public UserModel convertSingle(Map map) {

        UserModel userModel = new UserModel();

        userModel.id = (String)map.get("id");
        userModel.displayName = (String)map.get("displayName");
        userModel.rank = (int)map.get("rank");
        userModel.rankProgress = RankProgressModel.convertSingle((Map)map.get("rankProgress"));

        userModel.avatar = (int)map.get("avatar");
        userModel.currentCity = CityModel.convertSingle((Map)map.get("currentLocation"));

        Map<String, CityModel> cities = CityModel.convertMultiple((Map) map.get("cities")); //(Map<String, CityModel>) map.get("cities");
        Map<String, Long> achievements = (Map<String, Long>)map.get("achievements");

        userModel.cities = cities;

        userModel.achievements = achievements;

        return userModel;
    }

    /**
     * Converts a mapped JSON response into a list of UserModels.
     * @param map Json response that represents multiple UserModels.
     * @return List of UserModels.
     */
    public List<UserModel> convertMultiple(Map map) {

        List<UserModel> userList = new ArrayList<>(map.size());

        for (Object key : map.keySet()) {

            userList.add(convertSingle((Map)map.get(key)));
        }

        return userList;
    }
}
