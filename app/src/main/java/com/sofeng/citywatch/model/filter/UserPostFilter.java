package com.sofeng.citywatch.model.filter;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by JKMT on 09/16/2017.
 */

public class UserPostFilter implements IFilter {

    private final String userId;
    private boolean byCityFlag;
    private String cityId;

    public UserPostFilter(String userId) {

        this.userId = userId;

        byCityFlag = false;
        cityId = "";
    }

    public UserPostFilter setByCity(boolean byCityFlag) {

        this.byCityFlag = byCityFlag;

        return this;
    }

    public UserPostFilter setCityId(String cityId) {

        this.cityId = cityId;

        return this;
    }

    @Override
    public Map<String, String> toMap() {

        Map<String, String> map = new LinkedHashMap<>();

        map.put("userId", userId);
        map.put("cityFlag", byCityFlag ? "1" : "0");
        map.put("cityId", cityId);

        return map;
    }
}
