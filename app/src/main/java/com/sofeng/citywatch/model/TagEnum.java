package com.sofeng.citywatch.model;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by JKMT on 09/02/2017.
 */

public enum TagEnum {

    Accident(0, null, "Accident"),
    NaturalCalamity(1, null, "Natural Calamity"),
    SocialEvent(2, null, "Social Event"),
    Traffic(3, null, "Traffic"),
    Charity(4, null, "Charity"),
    Crime(5, null, "Crime"),

    /* Accidents */
    BodilyInjury(6, Accident, "Bodily Injury"),
    PropertyDamage(7, Accident, "Property Damage"),
    VehicularAccident(8, Accident, "Vehicular Accident"),

    /* Natural Calamities */
    Flood(9, NaturalCalamity, "Flood"),
    Earthquake(10, NaturalCalamity, "Earthquake"),
    Typhoon(11, NaturalCalamity, "Typhoon"),
    Tornado(12, NaturalCalamity, "Tornado"),
    Wildfire(13, NaturalCalamity, "Wildlife"),

    /* Social Event */
    Nightlife(14, SocialEvent, "Nightlife"),
    Concert(15, SocialEvent, "Concert"),
    Party(16, SocialEvent, "Party"),
    CommunityGathering(17, SocialEvent, "Community Gathering"),

    /* Traffic */
    LightTraffic(18, Traffic, "Light Traffic"),
    ModerateTraffic(19, Traffic, "Moderate Traffic"),
    HeavyTraffic(20, Traffic, "Heavy Traffic"),
    Roadblock(21, Traffic, "Roadblock"),
    Reroute(22, Traffic, "Reroute"),
    TrafficJam(23, Traffic, "Traffic Jam"),

    /* Charity */
    HealthCharity(24, Charity, "Health Charity"),
    EducationCharity(25, Charity, "Education Charity"),
    ArtCultureCharity(26, Charity, "Art & Culture Charity"),
    AnimalCharity(27, Charity, "Animal Charity"),
    EnvironmentalCharity(28, Charity, "Environmental Charity"),

    /* Crime */
    Assault(29, Crime, "Assault"),
    Robbery(30, Crime, "Robbery"),
    SexualAssault(31, Crime, "Sexual Assault"),
    Homicide(32, Crime, "Homicide"),
    Kidnapping(33, Crime, "Kidnapping"),
    DrugRelated(34, Crime, "Drug Related");

    public final int id;
    public final TagEnum parent;
    public final String string;

    TagEnum(int id, TagEnum parent, String string) {

        this.id = id;
        this.parent = parent;
        this.string = string;
    }

    public static TagEnum getTag(int id) {

        return TagEnum.values()[id];
    }

    @NonNull
    public static CharSequence[] getAllNames(){
        ArrayList<CharSequence> list = new ArrayList<>();
        for(TagEnum tagEnum : TagEnum.values()){
            list.add(tagEnum.string);
        }
        return list.toArray(new CharSequence[0]);
    }
}
