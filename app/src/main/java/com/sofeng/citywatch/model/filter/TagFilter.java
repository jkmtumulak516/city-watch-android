package com.sofeng.citywatch.model.filter;

import com.sofeng.citywatch.model.TagEnum;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

/**
 * Author: King
 * Date: 10/3/2017
 */

public class TagFilter implements IFilter {

    private Set<TagEnum> tags;

    public Set<TagEnum> getTags() {

        return tags;
    }

    public void setTags(Collection<TagEnum> tags) {

        if (tags != null)
            this.tags = new LinkedHashSet<>(tags);
    }

    @Override
    public Map<String, String> toMap() {

        Map<String, String> map = new LinkedHashMap<>();

        int i = 0;

        if (tags != null) {
            for (TagEnum tag : this.tags)
                map.put("tags[" + (i++) + "]", tag.string);
        }

        return map;
    }
}
