package com.sofeng.citywatch.model;

import android.support.annotation.NonNull;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by JKMT on 08/19/2017.
 */

public class PostModel implements Serializable, Comparable<PostModel> {

    public static final double NONE = 0;
    public static final double IMAGE = 1;
    public static final double VIDEO = 2;

    public String id;
    public String userId;
    public String cityId;
    public double timestamp;

    public String title;
    public String description;

    public double mediaType;
    public String mediaUrl;

    public String locationName;

    public double lat;
    public double lng;

    public double voteCount;

    public Map<String, Boolean> votes;

    public Map<String, Boolean> tags;
    public String mood;

    /**
     * Converts a mapped JSON response into a PostModel.
     * @param map Json response that represents a single PostModel.
     * @return A PostModel.
     */
    public static PostModel convertSingle(Map map) {

        PostModel postModel = new PostModel();

        postModel.id = (String)map.get("id");
        postModel.userId = (String)map.get("userId");
        postModel.cityId = (String)map.get("cityId");
        postModel.timestamp = (double)map.get("timestamp");

        postModel.title = (String)map.get("title");
        postModel.description = (String)map.get("description");

        postModel.mediaType = (double)map.get("mediaType");
        postModel.mediaUrl = (String)map.get("mediaUrl");

        postModel.mood = (String)map.get("mood");

        postModel.locationName = (String)map.get("locationName");

        postModel.lat = (double)map.get("lat");
        postModel.lng = (double)map.get("lng");

        postModel.voteCount = (double)map.get("voteCount");

        Map<String, Boolean> votes = (Map<String, Boolean>)map.get("votes");

        Map<String, Boolean> tags = (Map<String, Boolean>)map.get("tags");

        postModel.votes = votes;
        postModel.tags = tags;

        return postModel;
    }

    /**
     * Converts a mapped JSON response into a list of PostModels.
     * @param list Json response that represents multiple PostModels.
     * @return List of PostModels.
     */
    public static List<PostModel> convertMultiple(List<Map> list) {

        if (list == null)
            return null;

        List<PostModel> postList = new ArrayList<>(list.size());

        for (Map post : list) {

            postList.add(convertSingle(post));
        }

        return postList;
    }

    @Override
    public int compareTo(@NonNull PostModel o) {
        return (int)(o.timestamp - timestamp);
    }
}

