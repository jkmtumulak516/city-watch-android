package com.sofeng.citywatch.model;

/**
 * Created by JKMT on 09/19/2017.
 */

public enum VoteEnum {

    Up(true),
    None(null),
    Down(false);

    public final Boolean value;

    VoteEnum(Boolean value) {
        this.value = value;
    }

    public static VoteEnum getVoteEnum(Boolean value){
        VoteEnum vote = VoteEnum.None;
        if(value){
            vote = VoteEnum.Up;
        }else{
            vote = VoteEnum.Down;
        }

        return vote;
    }

    public VoteEnum resolve(VoteEnum otherVote) {

        VoteEnum result;

        switch (this) {
            case Up:
                result = (this == otherVote) ? None : Up;
                break;
            case Down:
                result = (this == otherVote) ? None : Down;
                break;
            default:
                result = None;
        }

        return result;
    }
}
