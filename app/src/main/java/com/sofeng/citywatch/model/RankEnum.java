package com.sofeng.citywatch.model;

/**
 * Created by JKMT on 09/07/2017.
 */

public enum RankEnum {

    Troll(-1, "Troll"),
    Watcher(0, "Watcher"),
    Bystander(1, "Bystander"),
    Scout(2, "Scout"),
    Inspector(3, "Inspector"),
    Enforcer(4, "Enforcer"),
    Commander(5, "Commander"),
    Sentinel(6, "Sentinel");

    public int id;
    public String name;

    RankEnum(int id, String name) {

        this.id = id;
        this.name = name;
    }

    public static RankEnum getRank(int id) {

        return values()[id + 1];
    }
}
