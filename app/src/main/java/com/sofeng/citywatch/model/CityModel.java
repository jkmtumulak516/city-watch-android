package com.sofeng.citywatch.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by JKMT on 09/02/2017.
 */

public class CityModel implements Serializable{

    public CityModel() {}

    public CityModel(String id, String name) {

        this.id = id;
        this.name = name;
    }

    public String id;
    public String name;

    /**
     * Converts a mapped JSON response into a CityModel.
     * @param map Json response that represents a single CityModel.
     * @return A CityModel.
     */
    public static CityModel convertSingle(Map map) {

        CityModel cityModel = new CityModel();

        cityModel.id = (String) map.get("id");
        cityModel.name = (String) map.get("name");

        return cityModel;
    }

    /**
     * Converts a mapped JSON response into a list of CityModels.
     * @param map Json response that represents multiple CityModels.
     * @return List of CityModels.
     */
    public static Map<String, CityModel> convertMultiple(Map map) {

        Map<String, CityModel> cityList = new LinkedHashMap<>(map.size());

        for (Object key : map.keySet()) {

            cityList.put((String)key, convertSingle((Map)map.get(key)));
        }

        return cityList;
    }
}
