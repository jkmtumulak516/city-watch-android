package com.sofeng.citywatch.model;

/**
 * Created by JKMT on 09/02/2017.
 */

public enum  MoodEnum {

    Happy(0, "Happy"),
    Excited(1, "Excited"),
    Sad(2, "Sad"),
    Angry(3, "Angry"),
    Worried(4, "Worried"),
    Annoyed(5, "Annoyed"),
    Frustrated(6, "Frustrated"),
    Disappointed(7, "Disappointed"),
    Scared(8, "Scared"),
    Indifferent(9, "Indifferent");

    public final int num;
    public final String string;

    MoodEnum(int num, String string) {

        this.num = num;
        this.string = string;
    }
}
