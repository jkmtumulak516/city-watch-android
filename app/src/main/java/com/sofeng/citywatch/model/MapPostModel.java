package com.sofeng.citywatch.model;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Author: King
 * Date: 10/2/2017
 */

public class MapPostModel implements ClusterItem {

    public String postId;
    public String title;
    public String description;
    public double lat;
    public double lng;

    /**
     * Converts a mapped JSON response into a MapPostModel.
     * @param map Json response that represents a single MapPostModel.
     * @return A MapPostModel.
     */
    public static MapPostModel convertSingle(Map map) {

        MapPostModel mapPostModel = new MapPostModel();

        mapPostModel.postId = (String)map.get("postId");
        mapPostModel.title = (String)map.get("title");
        mapPostModel.description = (String)map.get("description");

        mapPostModel.lat = (double)map.get("lat");
        mapPostModel.lng = (double)map.get("lng");

        return mapPostModel;
    }

    /**
     * Converts a mapped JSON response into a list of MapPostModels.
     * @param list Json response that represents multiple MapPostModels.
     * @return List of MapPostModels.
     */
    public static List<MapPostModel> convertMultiple(List<Map> list) {

        if (list == null)
            return null;

        List<MapPostModel> mapPostList = new ArrayList<>(list.size());

        for (Map post : list) {

            mapPostList.add(convertSingle(post));
        }

        return mapPostList;
    }

    @Override
    public LatLng getPosition() {
        return new LatLng(lat,lng);
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public String getSnippet() {
        return description;
    }
}
