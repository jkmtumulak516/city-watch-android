package com.sofeng.citywatch.model;

import com.google.firebase.database.DataSnapshot;

/**
 * Author: King
 * Date: 10/3/2017
 */

public class CityStatisticsModel {

    public CityStatisticsModel() {}

    public CityStatisticsModel(DataSnapshot dataSnapshot) {

        postCount = 0;
        userCount = 0;
        accidentCount = 0;
        naturalCalamityCount = 0;
        socialEventCount = 0;
        trafficCount = 0;
        charityCount = 0;
        crimeCount = 0;

        if (dataSnapshot.child("post").exists())
            postCount = dataSnapshot.child("post").getValue(Long.class);
        if (dataSnapshot.child("user").exists())
            userCount = dataSnapshot.child("user").getValue(Long.class);
        accidentCount = dataSnapshot.child(TagEnum.Accident.string).exists() ?
                dataSnapshot.child(TagEnum.Accident.string).getValue(Long.class) : 0;
        naturalCalamityCount = dataSnapshot.child(TagEnum.NaturalCalamity.string).exists() ?
                dataSnapshot.child(TagEnum.NaturalCalamity.string).getValue(Long.class) : 0;
        socialEventCount = dataSnapshot.child(TagEnum.SocialEvent.string).exists() ?
                dataSnapshot.child(TagEnum.SocialEvent.string).getValue(Long.class): 0;
        trafficCount = dataSnapshot.child(TagEnum.Traffic.string).exists() ?
                dataSnapshot.child(TagEnum.Traffic.string).getValue(Long.class) : 0;
        charityCount = dataSnapshot.child(TagEnum.Charity.string).exists() ?
                dataSnapshot.child(TagEnum.Charity.string).getValue(Long.class) : 0;
        crimeCount = dataSnapshot.child(TagEnum.Crime.string).exists() ?
                dataSnapshot.child(TagEnum.Crime.string).getValue(Long.class) : 0;
    }

    public long postCount;
    public long userCount;
    public long accidentCount;
    public long naturalCalamityCount;
    public long socialEventCount;
    public long trafficCount;
    public long charityCount;
    public long crimeCount;
}
