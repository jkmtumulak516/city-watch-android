package com.sofeng.citywatch.model;

/**
 * Created by JKMT on 07/19/2017.
 */

public enum AvatarEnum {

    Avatar1(1, "https://firebasestorage.googleapis.com/v0/b/citywatchandroid.appspot.com/o/Avatars%2Favatar1.png?alt=media&token=7c7b5bb0-42ff-4000-ac29-6305a5c0c9a2"),
    Avatar2(2, "https://firebasestorage.googleapis.com/v0/b/citywatchandroid.appspot.com/o/Avatars%2Favatar2.png?alt=media&token=45ab3c54-f132-4e23-9c59-c271a127727f"),
    Avatar3(3, "https://firebasestorage.googleapis.com/v0/b/citywatchandroid.appspot.com/o/Avatars%2Favatar3.png?alt=media&token=c69eff6c-eb69-42db-96da-24582a2f337e"),
    Avatar4(4, "https://firebasestorage.googleapis.com/v0/b/citywatchandroid.appspot.com/o/Avatars%2Favatar4.png?alt=media&token=92725ae0-ff46-44e7-9ccb-1d6308e1a0d1"),
    Avatar5(5, "https://firebasestorage.googleapis.com/v0/b/citywatchandroid.appspot.com/o/Avatars%2Favatar5.png?alt=media&token=cc3e1462-2bd7-4be9-b305-b97a509c343f"),
    Avatar6(6, "https://firebasestorage.googleapis.com/v0/b/citywatchandroid.appspot.com/o/Avatars%2Favatar6.png?alt=media&token=1204eb66-f819-4293-9f8e-ed390b799a51"),
    Avatar7(7, "https://firebasestorage.googleapis.com/v0/b/citywatchandroid.appspot.com/o/Avatars%2Favatar7.png?alt=media&token=afb6bbad-6d5e-48b2-b150-3e491c8da481"),
    Avatar8(8, "https://firebasestorage.googleapis.com/v0/b/citywatchandroid.appspot.com/o/Avatars%2Favatar8.png?alt=media&token=3374394a-9778-429d-9a75-d448f934fa7e");

    public final int id;

    public final String url;

    AvatarEnum(int id, String url) {

        this.id = id;
        this.url = url;
    }

    public static AvatarEnum getAvatar(int id) {

        switch (id) {
            case 1:
                return Avatar1;
            case 2:
                return Avatar2;
            case 3:
                return Avatar3;
            case 4:
                return Avatar4;
            case 5:
                return Avatar5;
            case 6:
                return Avatar6;
            case 7:
                return Avatar7;
            case 8:
                return Avatar8;
            default:
                return null;
        }
    }
}
