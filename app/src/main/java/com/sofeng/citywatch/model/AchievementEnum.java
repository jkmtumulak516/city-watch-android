package com.sofeng.citywatch.model;

import com.sofeng.citywatch.R;

/**
 * Created by JKMT on 10/05/2017.
 */

public enum  AchievementEnum {

    // @TODO fill the URLs for images
    NumberOfPosts("", "", "first", new Integer[]{
            R.mipmap.post_bronze,
            R.mipmap.post_silver,
            R.mipmap.post_gold,
        }, new int[]{10, 50, 100}, 3),
    NumberOfTrendingPosts("", "", "second", new Integer[]{
            R.mipmap.trending_bronze,
            R.mipmap.trending_silver,
            R.mipmap.trending_gold,
        }, new int[]{5, 15, 30}, 3),
    NumberOfVotes("", "", "third", new Integer[]{
            R.mipmap.upvotes_bronze,
            R.mipmap.upvotes_silver,
            R.mipmap.upvotes_gold,
        }, new int[]{50, 200, 500}, 3);

    public final String name;
    public final String description;
    public final String key;
    public final Integer[] images;
    public final int[] thresholds;
    public final int maxLevel;

    AchievementEnum(String name, String description, String key, Integer[] images, int[] thresholds, int maxLevel) {

        this.name = name;
        this.description = description;
        this.key = key;
        this.images = images;
        this.thresholds = thresholds;
        this.maxLevel = maxLevel;
    }

    public static AchievementEnum getAchievement(String key) {
        switch (key) {
            case "first": return NumberOfPosts;
            case "second": return NumberOfTrendingPosts;
            case "third": return NumberOfVotes;
            default: return null;
        }
    }

    public Integer getIcon(int value) {
        if (value <= 0 || value > maxLevel)
            return null;
        else
            return images[value - 1];
    }

    public int getThreshold(int value) {
        if (value <= 0 || value > maxLevel)
            return 0;
        else
            return thresholds[value - 1];
    }
}
