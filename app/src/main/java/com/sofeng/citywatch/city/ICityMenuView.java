package com.sofeng.citywatch.city;

import com.sofeng.citywatch.model.CityModel;

import java.util.List;

/**
 * Author: King
 * Date: 10/2/2017
 */

public interface ICityMenuView {

    /**
     * Updates recycler view with new city models
     * @param cityModels List of new cities
     */
    void updateRecyclerView(List<CityModel> cityModels);

}
