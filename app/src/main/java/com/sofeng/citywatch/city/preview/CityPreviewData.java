package com.sofeng.citywatch.city.preview;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.sofeng.citywatch.model.CityModel;
import com.sofeng.citywatch.model.CityStatisticsModel;
import com.sofeng.citywatch.net.ResponseListener;

/**
 * Author: King
 * Date: 10/3/2017
 */

public class CityPreviewData implements ICityPreviewData {

    private DatabaseReference rootReference;

    public CityPreviewData() {

        rootReference = FirebaseDatabase.getInstance().getReference();
    }

    @Override
    public void getCityStatistics(String cityId, final ResponseListener<CityStatisticsModel> listener) {

        DatabaseReference statsReference = rootReference.child("posts").child(cityId).child("statistics").child("count");

        statsReference.addListenerForSingleValueEvent(new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                if (dataSnapshot != null) {

                    CityStatisticsModel stats = new CityStatisticsModel(dataSnapshot);
                    listener.onSuccess(stats);
                } else {
                    listener.onError();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

                listener.onCancel();
            }
        });
    }

    @Override
    public void addCity(String userId, final String cityId, final String cityName) {

        DatabaseReference cityReference = rootReference
                .child("users").child(userId).child("cities").child(cityId);

        cityReference.setValue(new CityModel(cityId, cityName));
    }

    @Override
    public void setCurrentCity(String userId, String cityId, String cityName) {

        DatabaseReference cityReference = rootReference.child("users").child(userId).child("currentCity");

        cityReference.setValue(new CityModel(cityId, cityName));
    }

    @Override
    public void isCurrentCity(String userId, final String cityId, final ResponseListener<Boolean> listener) {

        DatabaseReference cityIdReference = rootReference.child("users").child(userId).child("currentCity").child("id");

        cityIdReference.addListenerForSingleValueEvent(new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                String currentCityId = dataSnapshot.getValue(String.class);

                listener.onSuccess(cityId.equals(currentCityId));
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

                listener.onCancel();
            }
        });
    }

    @Override
    public void hasAddedCity(String userId, String cityId, final ResponseListener<Boolean> listener) {

        DatabaseReference cityIdReference = rootReference.child("users").child(userId).child("cities").child(cityId);

        cityIdReference.addListenerForSingleValueEvent(new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                CityModel city = dataSnapshot.getValue(CityModel.class);

                listener.onSuccess(city != null);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

                listener.onCancel();
            }
        });
    }
}
