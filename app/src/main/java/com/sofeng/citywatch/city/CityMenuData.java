package com.sofeng.citywatch.city;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.ValueEventListener;
import com.sofeng.citywatch.model.CityModel;
import com.sofeng.citywatch.net.ResponseListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Author: King
 * Date: 10/2/2017
 */

public class CityMenuData implements ICityMenuData {

    private DatabaseReference rootReference;

    public CityMenuData() {

        rootReference = FirebaseDatabase.getInstance().getReference();
    }

    @Override
    public void getCities(String userId, final ResponseListener<List<CityModel>> listener) {

        DatabaseReference citiesReference = rootReference.child("users").child(userId).child("cities");

        citiesReference.addListenerForSingleValueEvent(new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // sets the type indicator to map for use later
                GenericTypeIndicator<Map<String, CityModel>> type =
                        new GenericTypeIndicator<Map<String, CityModel>>() {
                        };
                // converts the datasnapshot into a map then initializes a list of CityModels
                Map<String, CityModel> convertedDatasnapshot = dataSnapshot.getValue(type);
                List<CityModel> cities = new ArrayList<>(convertedDatasnapshot.values());

                // calls the listener and passes a list of cities
                listener.onSuccess(cities);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

                listener.onCancel();
            }
        });
    }

}
