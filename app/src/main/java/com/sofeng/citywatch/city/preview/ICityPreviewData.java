package com.sofeng.citywatch.city.preview;

import com.sofeng.citywatch.model.CityStatisticsModel;
import com.sofeng.citywatch.net.ResponseListener;

/**
 * Author: King
 * Date: 10/3/2017
 */

public interface ICityPreviewData {

    /**
     * Gets a city's statistics and then passes it to the listener.
     * @param cityId City's Id.
     * @param listener Listener to be executed.
     */
    void getCityStatistics(String cityId, final ResponseListener<CityStatisticsModel> listener);

    /**
     * Adds the city to the user's list of cities.
     * @param userId User's Id.
     * @param cityId City's Id.
     * @param cityName City's name.
     */
    void addCity(String userId, final String cityId, final String cityName);

    /**
     * Sets the city as the user's current city.
     * @param userId User's Id.
     * @param cityId City's Id.
     * @param cityName City's name.
     */
    void setCurrentCity(String userId, String cityId, String cityName);

    /**
     *
     * @param userId
     * @param cityId
     * @param listener
     */
    void isCurrentCity(String userId, final String cityId, final ResponseListener<Boolean> listener);

    /**
     *
     * @param userId
     * @param cityId
     * @param listener
     */
    void hasAddedCity(String userId, String cityId, final ResponseListener<Boolean> listener);
}
