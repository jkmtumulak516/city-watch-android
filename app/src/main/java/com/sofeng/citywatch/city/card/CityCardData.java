package com.sofeng.citywatch.city.card;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.ValueEventListener;
import com.sofeng.citywatch.city.ICityMenuData;
import com.sofeng.citywatch.model.CityModel;
import com.sofeng.citywatch.net.ResponseListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Author: King
 * Date: 10/2/2017
 */

public class CityCardData implements ICityCardData {

    private DatabaseReference rootReference;

    public CityCardData() {

        rootReference = FirebaseDatabase.getInstance().getReference();
    }

    @Override
    public void getCityWatcherCount(String cityId, final ResponseListener<Long> listener) {

        DatabaseReference countReference = rootReference.child("posts")
                .child(cityId).child("statistics").child("count").child("post");

        countReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                Long count = dataSnapshot.getValue(Long.class);

                if (count != null) {
                    listener.onSuccess(count);
                } else {
                    listener.onSuccess(0L);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

                listener.onCancel();
            }
        });
    }

    @Override
    public void setCurrentCity(String userId, String cityId, String cityName) {

        DatabaseReference cityReference = rootReference.child("users").child(userId).child("currentCity");

        cityReference.setValue(new CityModel(cityId, cityName));
    }
}
