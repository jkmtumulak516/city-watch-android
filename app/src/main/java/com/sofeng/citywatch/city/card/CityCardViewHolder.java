package com.sofeng.citywatch.city.card;

import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.sofeng.citywatch.R;
import com.sofeng.citywatch.city.preview.CityPreviewActivity;
import com.sofeng.citywatch.model.CityModel;
import com.sofeng.citywatch.util.CityFormatter;

/**
 * Created by Francis on 10/2/2017.
 */

public class CityCardViewHolder extends RecyclerView.ViewHolder implements ICityCardView{

    private TextView cityLocation, cityWatchers;
    private ImageView cityPin;
    private CardView cityCard;
    private View view;
    private ICityCardPresenter cityCardPresenter;


    public CityCardViewHolder(View itemView) {
        super(itemView);
        this.view = itemView;
        this.cityCardPresenter = new CityCardPresenter(this);
    }

    public void setView(final CityModel cityModel){
        cityLocation = (TextView) view.findViewById(R.id.city_location);
        cityWatchers =(TextView) view.findViewById(R.id.city_watchers);
        cityPin =(ImageView) view.findViewById(R.id.city_pin);
        cityCard =(CardView) view.findViewById(R.id.city_card);

        cityLocation.setText(CityFormatter.getMainAddress(cityModel.name));
        cityCard.setClickable(true);

        cityCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(view.getContext(), CityPreviewActivity.class);
                i.putExtra("cityModel", cityModel);
                view.getContext().startActivity(i);
            }
        });


        cityCardPresenter.updateWatcherCount(cityModel.id);
    }

    @Override
    public void setWatcherCount(long watcherCount) {
        cityWatchers.setText(watcherCount + " Watchers");
    }

    public void setCurrentCity(){
        //TODO implement change of color and with pin
        cityCard.setCardBackgroundColor(view.getResources().getColor(R.color.colorAccentTertiary));
        cityPin.setVisibility(View.VISIBLE);
    }
}
