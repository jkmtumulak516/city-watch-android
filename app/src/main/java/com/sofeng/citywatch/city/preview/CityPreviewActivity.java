package com.sofeng.citywatch.city.preview;

import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.dzaitsev.android.widget.RadarChartView;
import com.google.firebase.auth.FirebaseAuth;
import com.sofeng.citywatch.R;
import com.sofeng.citywatch.model.CityModel;
import com.sofeng.citywatch.model.CityStatisticsModel;
import com.sofeng.citywatch.util.CityFormatter;

import org.w3c.dom.Text;

import java.util.LinkedHashMap;
import java.util.Map;

public class CityPreviewActivity
        extends AppCompatActivity
        implements ICityPreviewView {

    ICityPreviewPresenter cityPreviewPresenter;
    CityModel city;
    CollapsingToolbarLayout mCollapsingToolbarLayout;
    Toolbar toolbar;
    private Map<String, Float> axis;
    private RadarChartView chartView;
    private TextView numPost;
    private TextView numWatchers;
    private Button cityAdd;
    private Button citySet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_city_preview);
        cityPreviewPresenter = new CityPreviewPresenter(this);
        city = (CityModel) getIntent().getExtras().getSerializable("cityModel");
        initializeAppBar();
        initializeStats();
        initializeControl();
        cityPreviewPresenter.loadCityStatistics(city.id);
    }

    public void initializeControl(){
        cityAdd = (Button) findViewById(R.id.city_add);
        citySet = (Button) findViewById(R.id.city_set);

        cityAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cityPreviewPresenter.addCity(FirebaseAuth.getInstance().getCurrentUser().getUid(),
                        city.id, city.name);
                cityAdd.setVisibility(View.GONE);
                citySet.setVisibility(View.VISIBLE);
            }
        });

        citySet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cityPreviewPresenter.setCurrentCity(FirebaseAuth.getInstance().getCurrentUser().getUid(),
                        city.id, city.name);
                citySet.setVisibility(View.GONE);
            }
        });

        cityPreviewPresenter.updateControls(FirebaseAuth.getInstance().getCurrentUser().getUid(),
                city.id);
    }

    private void initializeAppBar() {
        mCollapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.ctl_City_Preview);
        setCollapsingTitle(CityFormatter.getMainAddress(city.name));
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_back);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
    }

    private void setCollapsingTitle(String title) {
        mCollapsingToolbarLayout.setTitle(title);
        mCollapsingToolbarLayout.setExpandedTitleTextAppearance(R.style.ExpandedAppBar);
        mCollapsingToolbarLayout.setCollapsedTitleTextAppearance(R.style.CollapsedAppBar);

    }

    private void initializeStats() {
        // Set your data to the view
        chartView = (RadarChartView) findViewById(R.id.radar_chart);
        chartView.setAutoSize(true);// auto balance the chart
        numPost = (TextView) findViewById(R.id.tv_NumPosts);
        numWatchers = (TextView) findViewById(R.id.tv_NumWatchers);


    }

    @Override
    public void displayStatistics(CityStatisticsModel statisticsModel) {
            axis = new LinkedHashMap<>(7);
            axis.put("Accident", Float.valueOf(statisticsModel.accidentCount));
            axis.put("Calamity", Float.valueOf(statisticsModel.naturalCalamityCount));
            axis.put("Social", Float.valueOf(statisticsModel.socialEventCount));
            axis.put("Traffic", Float.valueOf(statisticsModel.trafficCount));
            axis.put("Charity", Float.valueOf(statisticsModel.charityCount));
            axis.put("Crime", Float.valueOf(statisticsModel.crimeCount));

            chartView.setAxis(axis);

            numPost.setText(statisticsModel.postCount + "");
            numWatchers.setText(statisticsModel.userCount + "");
    }

    @Override
    public void displayAdd() {
        cityAdd.setVisibility(View.VISIBLE);
    }

    @Override
    public void displaySet() {
        citySet.setVisibility(View.VISIBLE);
    }
}
