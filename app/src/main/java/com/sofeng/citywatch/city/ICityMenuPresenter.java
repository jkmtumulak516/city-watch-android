package com.sofeng.citywatch.city;

/**
 * Author: King
 * Date: 10/2/2017
 */

public interface ICityMenuPresenter {

    /**
     * Retrieves city list of the user from server
     * @param userId Id of the user
     */
    void updateCities(String userId);

}
