package com.sofeng.citywatch.city.card;

import com.sofeng.citywatch.model.CityModel;

import java.util.List;

/**
 * Author: King
 * Date: 10/2/2017
 */

public interface ICityCardView {

    void setWatcherCount(long watcherCount);
}
