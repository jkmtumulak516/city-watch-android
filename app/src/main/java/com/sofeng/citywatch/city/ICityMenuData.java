package com.sofeng.citywatch.city;

import com.sofeng.citywatch.model.CityModel;
import com.sofeng.citywatch.net.ResponseListener;

import java.util.List;

/**
 * Author: King
 * Date: 10/2/2017
 */

public interface ICityMenuData {

    /**
     * Get's a list of the user's cities.
     * @param userId User's Id.
     * @param listener Listener to be executed.
     */
    void getCities(String userId, final ResponseListener<List<CityModel>> listener);

}
