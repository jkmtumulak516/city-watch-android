package com.sofeng.citywatch.city.card;

import com.sofeng.citywatch.model.CityModel;
import com.sofeng.citywatch.net.ResponseListener;

import java.util.List;

/**
 * Author: King
 * Date: 10/2/2017
 */

public interface ICityCardData {


    /**
     * Set's the user's current city to the current city
     * @param userId User's Id.
     * @param cityId City's Id.
     * @param cityName City's name.
     */
    void setCurrentCity(String userId, String cityId, String cityName);

    /**
     * Get's the number of Watchers in a city.
     * @param cityId City's Id.
     * @param listener Listener to be executed.
     */
    void getCityWatcherCount(String cityId, final ResponseListener<Long> listener);
}
