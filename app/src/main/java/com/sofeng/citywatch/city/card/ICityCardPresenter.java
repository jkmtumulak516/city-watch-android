package com.sofeng.citywatch.city.card;

/**
 * Author: King
 * Date: 10/2/2017
 */

public interface ICityCardPresenter {

    /**
     * Sets this city to current city
     * @param userId Id of the user
     * @param cityId Id of the city
     * @param cityName Name of the city
     */
    void updateCurrentCity(String userId, String cityId, String cityName);

    /**
     * Gets the number of watchers of the city
     * @param cityId Id of the city
     */
    void updateWatcherCount(String cityId);
}
