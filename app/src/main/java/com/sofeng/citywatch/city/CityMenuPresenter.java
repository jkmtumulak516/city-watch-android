package com.sofeng.citywatch.city;

import com.sofeng.citywatch.model.CityModel;
import com.sofeng.citywatch.net.ResponseListener;
import com.sofeng.citywatch.util.FirebaseUtil;

import java.util.List;

/**
 * Author: King
 * Date: 10/2/2017
 */

public class CityMenuPresenter implements ICityMenuPresenter {

    private ICityMenuView cityMenuView;
    private ICityMenuData cityMenuData;

    public CityMenuPresenter(ICityMenuView view){
        this.cityMenuView = view;
        this.cityMenuData = new CityMenuData();
    }

    @Override
    public void updateCities(String userId) {
        cityMenuData.getCities(userId, new CityListener());
    }



    private class CityListener implements ResponseListener<List<CityModel>> {
        @Override
        public void onSuccess(List<CityModel> object) {
            cityMenuView.updateRecyclerView(object);
        }

        @Override
        public void onCancel() {

        }

        @Override
        public void onError() {

        }
    }
}
