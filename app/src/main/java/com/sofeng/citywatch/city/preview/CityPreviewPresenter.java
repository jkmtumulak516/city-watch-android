package com.sofeng.citywatch.city.preview;

import com.sofeng.citywatch.model.CityStatisticsModel;
import com.sofeng.citywatch.net.ResponseListener;

/**
 * Author: King
 * Date: 10/3/2017
 */

public class CityPreviewPresenter implements ICityPreviewPresenter {

    ICityPreviewView cityPreviewView;
    ICityPreviewData cityPreviewData;
    String userId;
    String cityId;

    CityPreviewPresenter(ICityPreviewView view) {
        cityPreviewView = view;
        cityPreviewData = new CityPreviewData();
    }


    @Override
    public void loadCityStatistics(String cityId) {
        cityPreviewData.getCityStatistics(cityId, new CityStatisticsListener());
    }

    @Override
    public void addCity(String userId, String cityId, String cityName) {
        cityPreviewData.addCity(userId, cityId, cityName);
    }

    @Override
    public void setCurrentCity(String userId, String cityId, String cityName) {
        cityPreviewData.setCurrentCity(userId, cityId, cityName);
        //TODO Implement View
    }

    @Override
    public void updateControls(String userId, String cityId) {
        this.cityId = cityId;
        this.userId = userId;
        cityPreviewData.hasAddedCity(userId, cityId, new HasCityAddedListener());
    }

    private class CityStatisticsListener implements ResponseListener<CityStatisticsModel> {
        @Override
        public void onSuccess(CityStatisticsModel object) {
            cityPreviewView.displayStatistics(object);
        }

        @Override
        public void onCancel() {

        }

        @Override
        public void onError() {

        }
    }

    private class HasCityAddedListener implements ResponseListener<Boolean> {
        @Override
        public void onSuccess(Boolean object) {
            if (object) {
                cityPreviewData.isCurrentCity(userId, cityId, new IsCurrentListener());
            } else {
                cityPreviewView.displayAdd();
            }
        }

        @Override
        public void onCancel() {

        }

        @Override
        public void onError() {

        }

        private class IsCurrentListener implements ResponseListener<Boolean> {
            @Override
            public void onSuccess(Boolean object) {
                if (!object) {
                    cityPreviewView.displaySet();
                }
            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError() {

            }
        }
    }
}
