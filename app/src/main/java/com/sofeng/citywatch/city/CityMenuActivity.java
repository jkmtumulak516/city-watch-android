package com.sofeng.citywatch.city;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.util.ArraySet;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.firebase.auth.FirebaseAuth;
import com.sofeng.citywatch.R;
import com.sofeng.citywatch.city.preview.CityPreviewActivity;
import com.sofeng.citywatch.dashboard.IDashboardView;
import com.sofeng.citywatch.model.CityModel;
import com.sofeng.citywatch.model.PostModel;
import com.sofeng.citywatch.post.PostAdapter;

import java.util.ArrayList;
import java.util.List;

public class CityMenuActivity
        extends AppCompatActivity
        implements ICityMenuView {

    private  ICityMenuPresenter cityMenuPresenter;

    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private CityAdapter cityAdapter;
    private String cityId;
    private String userId;

    private static final int REQUEST_CODE_AUTOCOMPLETE = 9;
    /**
     * Holds the bounds of the Philippines
     */
    private LatLngBounds BOUNDS_PH = new LatLngBounds(
            new LatLng(4.6145711, 119.6272661), new LatLng(19.966096, 124.173694));


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_city_menu);
        cityMenuPresenter = new CityMenuPresenter(this);
        getSupportActionBar().setTitle("My Cities");
        setupRecycleView();
        setupPostAdapter();
        init(savedInstanceState);
    }

    /**
     * Setting up Recycle View with City Adapter
     */
    public void setupPostAdapter() {
        cityAdapter = new CityAdapter();
        mRecyclerView.setAdapter(cityAdapter);
        cityAdapter.notifyDataSetChanged();
    }

    /**
    * Setting Recycle View
     */
    public void setupRecycleView() {

        mRecyclerView = (RecyclerView) findViewById(R.id.rv_cities);

        if (mRecyclerView != null) {
            mRecyclerView.setHasFixedSize(true);
        }

        mLayoutManager = new GridLayoutManager(this,2);
        mRecyclerView.setLayoutManager(mLayoutManager);
    }

    @Override
    public void updateRecyclerView(List<CityModel> cityModels) {
        if(cityModels.size() <= 0) {
            // TODO showEmpty();
        }else{
            // TODO hideEmpty();
        }
        Log.d("Debug", "[CITY] Loading items...");
        CityAdapter cityAdapter = new CityAdapter(cityModels, cityId);
        mRecyclerView.setAdapter(cityAdapter);
        cityAdapter.notifyDataSetChanged();
    }

    protected void init(Bundle savedInstanceState){
        if(savedInstanceState == null) {
            Bundle b = getIntent().getExtras();
            cityId = b != null ? b.getString("cityId"): null;
            userId = FirebaseAuth.getInstance().getCurrentUser().getUid();
        } else{
            cityId = savedInstanceState.getString("cityId");
            userId = savedInstanceState.getString("userId");
        }
        refreshFeed();
    }

    protected void refreshFeed(){
        cityMenuPresenter.updateCities(userId);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("cityId", cityId);
        outState.putString("userId", userId);
    }

    @Override
    protected void onResume() {
        super.onResume();
        refreshFeed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_city, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.search_city){
            openPlacesAutocompleteActivity();
        }
        return true;
    }

    /**
     * Opens the places autocomplete as a new activity
     */
    private void openPlacesAutocompleteActivity() {
        try {

            // The autocomplete activity requires Google Play Services to be available. The intent
            // builder checks this and throws an exception if it is not the case.
            Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                    .setFilter(new AutocompleteFilter.Builder().setTypeFilter(AutocompleteFilter.TYPE_FILTER_CITIES).build())
                    .setBoundsBias(BOUNDS_PH)
                    .build(this);
            startActivityForResult(intent, REQUEST_CODE_AUTOCOMPLETE);
        } catch (GooglePlayServicesRepairableException e) {
            // Indicates that Google Play Services is either not installed or not up to date. Prompt
            // the user to correct the issue.
            GoogleApiAvailability.getInstance().getErrorDialog(this, e.getConnectionStatusCode(),
                    0 /* requestCode */).show();
        } catch (GooglePlayServicesNotAvailableException e) {
            // Indicates that Google Play Services is not available and the problem is not easily
            // resolvable.
            String message = "Google Play Services is not available: " +
                    GoogleApiAvailability.getInstance().getErrorString(e.errorCode);

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == REQUEST_CODE_AUTOCOMPLETE){
            if(resultCode == RESULT_OK){
                // Get the user's selected place from the Intent.
                Place place = PlaceAutocomplete.getPlace(this, data);
                CityModel cityModel = new CityModel(place.getId(), place.getName().toString());
                Intent i = new Intent(this, CityPreviewActivity.class);
                i.putExtra("cityModel", cityModel);
                startActivity(i);
            }
        }
    }
}