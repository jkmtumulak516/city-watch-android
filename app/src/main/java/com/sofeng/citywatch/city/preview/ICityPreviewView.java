package com.sofeng.citywatch.city.preview;

import com.sofeng.citywatch.model.CityStatisticsModel;

/**
 * Author: King
 * Date: 10/3/2017
 */

public interface ICityPreviewView {

    /**
     * Displays the statistics to view
     * @param statisticsModel Statistics of the city
     */
    void displayStatistics(CityStatisticsModel statisticsModel);

    void displayAdd();

    void displaySet();
}
