package com.sofeng.citywatch.city.preview;

/**
 * Author: King
 * Date: 10/3/2017
 */

public interface ICityPreviewPresenter {

    /**
     * Sets up city
     * @param cityId Id of the City
     */
    void loadCityStatistics(String cityId);

    /**
     * Adds city to its list
     * @param userId
     * @param cityId
     * @param cityName
     */
    void addCity(String userId, String cityId, String cityName);

    /**
     * Sets the city as the user's current city.
     * @param userId User's Id.
     * @param cityId City's Id.
     * @param cityName City's name.
     */
    void setCurrentCity(String userId, String cityId, String cityName);

    /**
     *
     * @param userId
     * @param cityId
     */
    void updateControls(String userId, String cityId);
}
