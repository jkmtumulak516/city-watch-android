package com.sofeng.citywatch.city.card;

import com.sofeng.citywatch.city.CityMenuData;
import com.sofeng.citywatch.city.ICityMenuView;
import com.sofeng.citywatch.model.CityModel;
import com.sofeng.citywatch.net.ResponseListener;

import java.util.List;

/**
 * Author: King
 * Date: 10/2/2017
 */

public class CityCardPresenter implements ICityCardPresenter {

    private ICityCardView cityCardView;
    private ICityCardData cityCardData;

    public CityCardPresenter(ICityCardView view){
        this.cityCardView = view;
        this.cityCardData = new CityCardData();
    }
    @Override
    public void updateCurrentCity(String userId, String cityId, String cityName) {
        cityCardData.setCurrentCity(userId, cityId, cityName);
    }

    @Override
    public void updateWatcherCount(String cityId) {
        cityCardData.getCityWatcherCount(cityId, new WatcherCountListener());
    }

    private class WatcherCountListener implements ResponseListener<Long> {
        @Override
        public void onSuccess(Long object) {
            cityCardView.setWatcherCount(object);
        }

        @Override
        public void onCancel() {

        }

        @Override
        public void onError() {

        }
    }
}
