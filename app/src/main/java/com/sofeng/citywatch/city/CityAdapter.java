package com.sofeng.citywatch.city;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sofeng.citywatch.R;
import com.sofeng.citywatch.city.card.CityCardViewHolder;
import com.sofeng.citywatch.model.CityModel;

import java.util.List;

/**
 * Created by Francis on 10/2/2017.
 */

public class CityAdapter extends RecyclerView.Adapter<CityCardViewHolder> {
    List<CityModel> cityList;
    String currentCity;
    public CityAdapter() {
    }


    public CityAdapter(List<CityModel> cityList, String currentCity){
        this.cityList = cityList;
        this.currentCity = currentCity;
    }

    @Override
    public CityCardViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.city_menu_item,parent,false);

        return new CityCardViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final CityCardViewHolder holder, final int position) {
        final CityModel city = cityList.get(position);
        holder.setView(city);
        if(city.id.equals(currentCity)){
            holder.setCurrentCity();
        }
    }

    @Override
    public int getItemCount() {
        return cityList != null ? cityList.size() : 0;
    }
}
