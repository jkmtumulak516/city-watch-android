package com.sofeng.citywatch.filter;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.ArraySet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;

import com.appyvet.rangebar.IRangeBarFormatter;
import com.appyvet.rangebar.RangeBar;
import com.sofeng.citywatch.R;
import com.sofeng.citywatch.model.RankEnum;
import com.sofeng.citywatch.model.TagEnum;
import com.sofeng.citywatch.model.filter.PostFilter;

import java.util.Iterator;
import java.util.Set;

public class FilterActivity extends AppCompatActivity {


    private Switch followingSwitch;
    private RangeBar rankBar;
    private Button btnApply;

    private PostFilter postFilter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);
        Bundle bundle = getIntent().getExtras();
        postFilter = (PostFilter) bundle.getSerializable("postFilter");
        initUi();
        initExistingFilter();
    }
    /**
     * Initializes the views
     */
    private void initUi(){
        followingSwitch = (Switch) findViewById(R.id.filter_follow);
        rankBar = (RangeBar) findViewById(R.id.filter_rank);
        btnApply = (Button) findViewById(R.id.filter_apply);

        rankBar.setOnRangeBarChangeListener(new RangeBar.OnRangeBarChangeListener() {
            @Override
            public void onRangeChangeListener(RangeBar rangeBar, int leftPinIndex,
                                              int rightPinIndex, String leftPinValue,
                                              String rightPinValue) {
                postFilter.setMinRank(Math.min(Integer.parseInt(leftPinValue),
                        Integer.parseInt(rightPinValue)));
                postFilter.setMaxRank(Math.max(Integer.parseInt(leftPinValue),
                        Integer.parseInt(rightPinValue)));

                Log.d("Debug", "[FILTER] Min rank: " + postFilter.getMinRank()
                        + ",  Max rank: " + postFilter.getMaxRank());

            }
        });

        //Returns the name of the rank
        rankBar.setFormatter(new IRangeBarFormatter() {
            @Override
            public String format(String value) {
                return RankEnum.getRank(Integer.parseInt(value)).name;
            }
        });

        followingSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                postFilter.setFollowingFlag(b);
                Log.d("Debug", "[FILTER] Following Flag: " + postFilter.isFollowingFlag());
            }
        });

        btnApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent();
                i.putExtra("postFilter", postFilter);
                FilterActivity.this.setResult(RESULT_OK,i);
                finish();
            }
        });
    }

    /**
     * Initializes the views according to the previous filter passed on
     */
    private void initExistingFilter() {
        initTagsState();
        followingSwitch.setChecked(postFilter.isFollowingFlag());
        rankBar.setRangePinsByValue(postFilter.getMinRank(), postFilter.getMaxRank());
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_in_2, R.anim.slide_out_2);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_2, R.anim.slide_out_2);
    }

    /**
     * Click listener for the tag header in tag selection
     * Expands and Retracts containers
     *
     * @param v
     */
    public void tagHeaderClick(View v) {
        //gets the linearlayout that holds all the headers and containers
        ViewGroup parent = (ViewGroup) v.getParent();
        // gets the container next to the header
        ViewGroup container = (ViewGroup) parent.getChildAt(parent.indexOfChild(v) + 1);
        triggerExpandRetract(container);
        triggerHeaderImage(v);
    }

    /**
     * Helper function that sets the visibility of the container
     *
     * @param container
     */
    private void triggerExpandRetract(ViewGroup container) {
        if (container.getVisibility() == View.GONE) {
            container.setVisibility(View.VISIBLE);

        } else {
            container.setVisibility(View.GONE);
        }
    }

    /**
     * Switches the arrow image beside the tag header
     *
     * @param v
     */
    private void triggerHeaderImage(View v) {
        // gets the frame layout that holds the two image
        ViewGroup imgContainer = (ViewGroup) ((ViewGroup) v).getChildAt(1);
        ImageView retract = (ImageView) imgContainer.getChildAt(0);
        ImageView expand = (ImageView) imgContainer.getChildAt(1);

        if (retract.getVisibility() == View.GONE && expand.getVisibility() == View.VISIBLE) {
            retract.setVisibility(View.VISIBLE);
            expand.setVisibility(View.GONE);
        } else {
            retract.setVisibility(View.GONE);
            expand.setVisibility(View.VISIBLE);
        }

    }

    /**
     * Handles the check event of tag selection
     *
     * @param v
     */
    public void onCheck(View v) {
        CheckBox cb = (CheckBox) v;
        int tagId = Integer.parseInt(v.getTag().toString());
        TagEnum tag = TagEnum.getTag(tagId);
        if (cb.isChecked()) {
            postFilter.addTag(tag);
            Log.d("Debug", "Selected tag: " + tag.string);
        } else {
            postFilter.getTags().remove(tag);
            Log.d("Debug", "Removed tag: " + tag.string);
        }

        //Log.d("Debug", "Current Tags: " + tagEnumSet.toString());
    }

    /**
     * Traverse all tags and checks its corresponding checkbox
     */
    private void initTagsState(){
        Iterator<TagEnum> i = postFilter.getTags().iterator();
        while(i.hasNext()){
            checkViewByTag(i.next().id + "");
        }
    }

    /**
     * Finds a checkbox by its tag and set it to checked
     * @param tag
     */
    private void checkViewByTag(String tag){
        View view = findViewById(R.id.root);
        CheckBox cb = (CheckBox) view.findViewWithTag(tag);
        cb.setChecked(true);
    }



}
