package com.sofeng.citywatch.net;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.sofeng.citywatch.model.filter.IFilter;

import java.io.UnsupportedEncodingException;
import java.util.Map;

/**
 * Created by JKMT on 08/20/2017.
 */

/**
 * Request object used to handle responses of multiple objects, e.g. an array of Models or Strings is returned.
 */
public class MultipleObjectsRequest<F extends IFilter> extends Request<Map> {

    private final Response.Listener<Map> listener;
    private final IFilter filterModel;
    private final Gson gson;

    public MultipleObjectsRequest(int method, String url, F filterModel, Response.Listener<Map> listener, Response.ErrorListener errorListener) {

        super(method, url, errorListener);

        this.filterModel = filterModel;
        this.listener = listener;

        gson = new Gson();
    }

    @Override
    protected Response<Map> parseNetworkResponse(NetworkResponse response) {
        try {
            String json = new String(
                    response.data,
                    HttpHeaderParser.parseCharset(response.headers));
            return Response.success(
                    gson.fromJson(json, Map.class),
                    HttpHeaderParser.parseCacheHeaders(response));
        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        } catch (JsonSyntaxException e) {
            return Response.error(new ParseError(e));
        }
    }

    @Override
    protected void deliverResponse(Map response) {
        listener.onResponse(response);
    }

    @Override
    protected Map<String, String> getParams() throws AuthFailureError {
        if (filterModel != null)
            return filterModel.toMap();
        else
            return super.getParams();
    }
}
