package com.sofeng.citywatch.net;

/**
 * Created by JKMT on 09/02/2017.
 */

/**
 * A basic, generic listener for asynchronous requests.
 * @param <T> Object type of response.
 */
public interface ResponseListener<T> {

    /**
     * Executed on request success.
     * @param object Response object.
     */
    void onSuccess(T object);

    /**
     * Executed on request error.
     */
    void onCancel();

    /**
     * Executed when a request is cancelled.
     */
    void onError();
}
