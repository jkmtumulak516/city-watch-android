package com.sofeng.citywatch.net;

/**
 * Created by JKMT on 08/21/2017.
 */

/**
 * Contains HTTP Endpoints from Firebase.
 */
public class Endpoints {

    public static class UserPosts {
        /**
         * Endpoint to get all of a User's posts.
         */
        public static final String ALL_USER_POSTS = "https://us-central1-citywatchandroid.cloudfunctions.net/getAllUserPosts";
        /**
         * Endpoint to get all of a User's posts within a particular city.
         */
        public static final String ALL_USER_POSTS_BY_CITY = "https://us-central1-citywatchandroid.cloudfunctions.net/getUserPostsByCity";
    }

    public static class Posts {
        /**
         * Endpoint to get all of the posts within a particular city that pass a given filter.
         */
        public static final String FEED_POSTS = "https://us-central1-citywatchandroid.cloudfunctions.net/getFeedPosts";
        /**
         * Endpoint to get posts related to a given post.
         */
        public static final String RELATED_POSTS = "https://us-central1-citywatchandroid.cloudfunctions.net/getRelatedPosts";
        /**
         * Endpoint to get posts that may be related to a new post.
         */
        public static final String POTENTIALLY_RELATED_POSTS = "https://us-central1-citywatchandroid.cloudfunctions.net/getPotentiallyRelatedPosts";
        /**
         * Endpoint to get trending posts in a city.
         */
        public static final String TRENDING_POSTS = "https://us-central1-citywatchandroid.cloudfunctions.net/getTrendingPosts";
    }

    public static class MapPosts {
        /**
         * Endpoint to get map posts of a city.
         */
        public static final String MAP_POSTS = "https://us-central1-citywatchandroid.cloudfunctions.net/getMapPosts";
    }
}
