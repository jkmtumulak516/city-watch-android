package com.sofeng.citywatch.net;

/**
 * Created by JKMT on 09/11/2017.
 */

/**
 * A listener interface for managing an upload.
 */
public interface UploadListener {

    /**
     * Called when upload has succeeded.
     */
    void onSuccess(String postId);

    /**
     * Called periodically as it uploads as long as it continues to upload.
     * @param currentProgress Progress of the upload.
     */
    void onProgress(double currentProgress);

    /**
     * Called when upload pauses.
     * @param currentProgress Progress of the upload when it was paused.
     */
    void onPaused(double currentProgress);

    /**
     * Called when the upload fails.
     * @param exception
     */
    void onFailure(Exception exception);
}
