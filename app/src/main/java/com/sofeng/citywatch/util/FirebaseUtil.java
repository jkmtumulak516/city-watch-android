package com.sofeng.citywatch.util;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.sofeng.citywatch.model.PostModel;

import java.util.List;

/**
 * Created by JKMT on 09/19/2017.
 */

public class FirebaseUtil {

    private static DatabaseReference root() {
        return FirebaseDatabase.getInstance().getReference();
    }

    public static void disposeListener(DatabaseReference databaseReference, ValueEventListener valueEventListener) {

        if (databaseReference != null && valueEventListener != null)
            databaseReference.removeEventListener(valueEventListener);
    }

    /**
     * Synchronizes or desynchronizes the user's that own a post in the list of given posts.
     * @param posts List of posts.
     * @param value Value which determines synchronization or desynchronization.
     */
    public static void syncUsers(List<PostModel> posts, boolean value) {
        // sets the base reference of all users
        DatabaseReference userReference = root().child("users");

        // iterates over all posts, get's the userId of a post, and then
        // syncs or desyncs based on the param value
        if(posts != null) {
            for (PostModel post : posts)
                userReference.child(post.userId).keepSynced(value);
        }
    }
}
