package com.sofeng.citywatch.util;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.TimeZone;

/**
 * Created by JKMT on 09/22/2017.
 */

public class Time {

    private static Time instance = null;

    private final NumberFormat numberFormat;

    private Time() {

        numberFormat = new DecimalFormat("00");
    }

    public static Time getInstance() {

        if (instance == null)
            instance = new Time();

        return instance;
    }

    public String getDate(double timestamp) {
        return getDate((long)timestamp);
    }

    public String getDate(long timestamp) {

        Calendar calendar = new GregorianCalendar(TimeZone.getDefault());
        calendar.setTimeInMillis(timestamp);
        calendar.setLenient(false);

        int amPm = calendar.get(Calendar.AM_PM);
        String amPmString = amPm == Calendar.AM ? "am" : "pm";

        String output = getMonth(calendar.get(Calendar.MONTH)) + " "
                + calendar.get(Calendar.DATE) + ", "
                + calendar.get(Calendar.YEAR) + " at "
                + calendar.get(Calendar.HOUR)+ ":"
                + numberFormat.format(calendar.get(Calendar.MINUTE))
                + amPmString;

        return output;
    }

    private String getMonth(int month) {
        switch (month) {
            case Calendar.JANUARY:
                return "Jan";
            case Calendar.FEBRUARY:
                return "Feb";
            case Calendar.MARCH:
                return "Mar";
            case Calendar.APRIL:
                return "Apr";
            case Calendar.MAY:
                return "May";
            case Calendar.JUNE:
                return "Jun";
            case Calendar.JULY:
                return "Jul";
            case Calendar.AUGUST:
                return "Aug";
            case Calendar.SEPTEMBER:
                return "Sep";
            case Calendar.OCTOBER:
                return "Oct";
            case Calendar.NOVEMBER:
                return "Nov";
            case Calendar.DECEMBER:
                return "Dec";
            default:
                return null;
        }
    }
}
