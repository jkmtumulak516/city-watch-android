package com.sofeng.citywatch.util;

import android.content.Context;
import android.util.Log;
import android.widget.ImageView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

/**
 * This singleton that acts as a wrapper/helper class to Picasso Image Loading Library
 * Author: Jhon Christian Ambrad
 * Date: 8/9/2017
 */

public class ImageLoader {
    private static ImageLoader imageLoaderInstance;
    private ImageLoader(){}

    /**
     * Creates an instance if first time, otherwise, return an instance
     * @return an instance of ImageLoader
     */
    public static ImageLoader getInstance(){
        if(imageLoaderInstance == null){
            imageLoaderInstance = new ImageLoader();
        }
        return imageLoaderInstance;
    }

    /**
     * Uses Picasso to load image to an imageview from a url
     * @param context Context of the Activity using the function
     * @param imageUrl Url of the image
     * @param imageView Destination of the image
     */
    public void loadImageFromUrl(final Context context, final String imageUrl, final ImageView imageView){
        Picasso.with(context)
                .load(imageUrl)
                .fit()
                .networkPolicy(NetworkPolicy.OFFLINE)
                .into(imageView, new Callback() {
                    @Override
                    public void onSuccess() {
                        Log.d("debug","Successfully fetched image from disk");
                    }

                    @Override
                    public void onError() {
                        //Try again online if cache failed
                        Picasso.with(context)
                                .load(imageUrl)
                                .into(imageView, new Callback() {
                                    @Override
                                    public void onSuccess() {
                                        Log.d("Debug","[PICASSSO] Successfully fetched image from network");
                                    }

                                    @Override
                                    public void onError() {
                                        Log.d("Debug","[PICASSSO] Could not fetch image");
                                    }
                                });
                    }
                });
    }

}
