package com.sofeng.citywatch.util;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;

/**
 * Author: King
 * Date: 10/2/2017
 */

public class TimeService {

    private static TimeService instance;
    private static final long intervalInMillis = 300000; // Five(5) minutes

    private final DatabaseReference timestampReference;
    private Thread currentTimer;

    private TimeService() {

        timestampReference = FirebaseDatabase.getInstance().getReference().child("timestamp");
        timestampReference.keepSynced(true);
        currentTimer = null;
    }

    public static void startTime() {

        if (instance == null)
            instance = new TimeService();

        instance.currentTimer = new Thread() {
            @Override
            public void run() {
                while (instance.currentTimer == this) {

                    instance.timestampReference.setValue(ServerValue.TIMESTAMP);

                    try {
                        Thread.sleep(intervalInMillis);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        };

        instance.currentTimer.start();
    }

    public static void stopTime() {

        instance.timestampReference.keepSynced(false);
        instance.currentTimer = null;
    }
}
