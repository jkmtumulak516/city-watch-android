package com.sofeng.citywatch.util;

/**
 * Created by JKMT on 09/01/2017.
 */

/**
 * Interface used to transfer tasks to different objects.
 */
public interface AsyncTask {

    /**
     * Execute task.
     */
    void execute();
}
