package com.sofeng.citywatch.login;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.sofeng.citywatch.model.RankProgressModel;
import com.sofeng.citywatch.model.UserModel;
import com.sofeng.citywatch.net.ResponseListener;

/**
 * Created by JKMT on 07/19/2017.
 */

public class LoginData implements ILoginData {

    private DatabaseReference rootReference;

    public LoginData() {

        rootReference = FirebaseDatabase.getInstance().getReference();
    }

    public void createUser(String userId) {

        UserModel emptyUser = new UserModel();
        RankProgressModel startingRankProgress = new RankProgressModel();

        startingRankProgress.progress = 0;
        startingRankProgress.floor = 0;
        startingRankProgress.ceil = 50;

        // set user default values here
        emptyUser.displayName = "xxxProReaps1337xx";
        emptyUser.id = userId;
        emptyUser.rank = 0;
        emptyUser.rankProgress = startingRankProgress;
        emptyUser.avatar = 1;

        rootReference.child("users").child(userId).setValue(emptyUser);
    }

    public void checkUserExists(String userId, ResponseListener<UserModel> responseListener) {

        rootReference.child("users").child(userId)
                .addListenerForSingleValueEvent(new UserExistsListener(responseListener));
    }

    private class UserExistsListener implements ValueEventListener {

        private ResponseListener<UserModel> responseListener;

        public UserExistsListener(ResponseListener<UserModel> responseListener) {

            this.responseListener = responseListener;
        }

        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {

            UserModel user = dataSnapshot.getValue(UserModel.class);

            responseListener.onSuccess(user);
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {

            responseListener.onCancel();
        }
    }
}
