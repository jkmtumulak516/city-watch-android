package com.sofeng.citywatch.login;

import com.google.firebase.database.ValueEventListener;
import com.sofeng.citywatch.model.UserModel;
import com.sofeng.citywatch.net.ResponseListener;

/**
 * Created by JKMT on 07/19/2017.
 */

public interface ILoginData {

    /**
     * Create new user entry in database.
     * @param userId New user's id.
     */
    void createUser(String userId);

    /**
     * Check if a user entry has been created in the database.
     * Address: [root]/users/{userId}
     * @param userId User id being checked.
     * @param responseListener Listener event upon checking user.
     */
    void checkUserExists(String userId, ResponseListener<UserModel> responseListener);
}
