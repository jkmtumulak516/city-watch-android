package com.sofeng.citywatch.login;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.sofeng.citywatch.dashboard.DashboardActivity;
import com.sofeng.citywatch.R;
import com.sofeng.citywatch.setup.SetupActivity;

public class LoginActivity
        extends AppCompatActivity
        implements ILoginView {

    private ILoginPresenter loginPresenter;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Signing in...");

        loginPresenter = new LoginPresenter(this);

        //Setup GoogleSignIn Intent
        loginPresenter.setupGoogleSignIn(getString(R.string.default_web_client_id));

        //Setup GoogleAPI
        loginPresenter.setupGoogleApiClient();

        //Setup Firebase Auth
        loginPresenter.setupFirebaseAuth();

    }

    @Override
    public void gotoAccountSetup() {
        Intent i = new Intent(this, SetupActivity.class);
        startActivity(i);
        finish();
    }

    @Override
    public void gotoMain() {
        Intent i = new Intent(this, DashboardActivity.class);
        startActivity(i);
        finish();
    }

    @Override
    public Context getViewContext() {
        return getBaseContext();
    }

    @Override
    public FragmentActivity getViewInstance() {
        return this;
    }

    @Override
    public void showProgressDialog() {
        progressDialog.show();
    }

    @Override
    public void hideProgressDialog() {
        progressDialog.hide();
    }

    @Override
    public void showSignInErrorMessage() {
        Toast.makeText(this, "Authentication failed.", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showConnectionError() {
        Toast.makeText(this, "Connection Error", Toast.LENGTH_SHORT).show();
    }

    public void handleLogin(View view) {
        loginPresenter.login();

    }

    @Override
    protected void onStart() {
        super.onStart();
        loginPresenter.attachFirebaseAuthListener();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == this.RC_SIGN_IN) {
            loginPresenter.handleGoogleSignIn(data);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        loginPresenter.detachFirebaseAuthListener();

    }
}
