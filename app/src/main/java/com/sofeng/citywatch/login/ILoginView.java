package com.sofeng.citywatch.login;

import android.content.Context;
import android.support.v4.app.FragmentActivity;

/**
 * Created by JKMT on 07/19/2017.
 */

public interface ILoginView {
    /**
     * Request code for the Google Intent
     */
    int RC_SIGN_IN = 1;

    /**
     * Goes to AccountSetup Activity
     */
    void gotoAccountSetup();

    /**
     * Goes to Main Activity
     */
    void gotoMain();

    /**
     * Gets the context of the activity
     * @return Context of the activity
     */
    Context getViewContext();

    /**
     * Gets the instance of the activity
     * @return Instance of the activity
     */
    FragmentActivity getViewInstance();

    /**
     * Shows the progress dialog
     */
    void showProgressDialog();

    /**
     * Hides the progress dialog
     */
    void hideProgressDialog();

    /**
     * Shows the sign-in error dialog
     */
    void showSignInErrorMessage();

    /**
     * Shows the connection error dialog
     */
    void showConnectionError();
}
