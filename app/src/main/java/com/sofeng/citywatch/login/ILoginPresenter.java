package com.sofeng.citywatch.login;

import android.content.Intent;

/**
 * Created by JKMT on 07/19/2017.
 */

public interface ILoginPresenter {

    /**
     * Sets up Firebase Authentication
     */
    void setupFirebaseAuth();

    /**
     * Sets up Google API Client
     */
    void setupGoogleApiClient();

    /**
     * Sets up Google Sign-in
     */
    void setupGoogleSignIn(String defaultWebClientId);

    /**
     * Attaches AuthListener to Firebase Auth
     */
    void attachFirebaseAuthListener();

    /**
     * Detaches AuthListener from Firebase Auth
     */
    void detachFirebaseAuthListener();

    /**
     * Handles the returned Intent (data) from the GoogleSignIn call
     * @param data
     */
    void handleGoogleSignIn(Intent data);

    /**
     * Logs in the user;
     */
    void login();
}
