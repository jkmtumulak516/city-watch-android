package com.sofeng.citywatch.login;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.sofeng.citywatch.model.UserModel;
import com.sofeng.citywatch.net.ResponseListener;

/**
 * Created by JKMT on 07/19/2017.
 */

public class LoginPresenter implements ILoginPresenter {

    private ILoginView loginView;
    private ILoginData loginData;

    private GoogleSignInOptions gso;

    private GoogleApiClient mGoogleApiClient;

    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;


    public LoginPresenter(ILoginView loginView) {

        this.loginView = loginView;

        loginData = new LoginData();
    }

    @Override
    public void setupFirebaseAuth() {

        mAuth = FirebaseAuth.getInstance();


        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                final FirebaseUser user = firebaseAuth.getCurrentUser();

                if (user != null) {

                    loginData.checkUserExists(user.getUid(), new CheckUserListener(user.getUid(), loginView, loginData));
                }
            }
        };
    }

    @Override
    public void setupGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(loginView.getViewContext())
                .enableAutoManage(loginView.getViewInstance(), new GoogleApiClient.OnConnectionFailedListener() {

                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
                        loginView.showConnectionError();
                    }
                })
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
    }

    @Override
    public void setupGoogleSignIn(String defaultWebClientId) {
        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(defaultWebClientId)
                .requestEmail()
                .build();
    }

    @Override
    public void attachFirebaseAuthListener() {
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    public void detachFirebaseAuthListener() {
        mAuth.removeAuthStateListener(mAuthListener);
    }

    @Override
    public void handleGoogleSignIn(Intent data) {
        GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
        Log.d("tag", "Signing in");
        if (result.isSuccess()) {
            loginView.showProgressDialog();
            GoogleSignInAccount account = result.getSignInAccount();
            firebaseAuthWithGoogle(account);
        }else{
            loginView.showSignInErrorMessage();
        }
    }

    @Override
    public void login() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        loginView.getViewInstance().startActivityForResult(signInIntent, loginView.RC_SIGN_IN);

    }

    /**
     * Signs in a Google Account to Firebase Auth
     *
     * @param acct
     */
    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(loginView.getViewInstance(), new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        loginView.hideProgressDialog();
                        if (!task.isSuccessful()) {
                            loginView.showSignInErrorMessage();
                        }
                    }
                });
    }

    /**
     * ResponseListener used to check user when listening on address [root]/user/{userId}
     */
    private class CheckUserListener implements ResponseListener<UserModel> {

        private String userId;

        private ILoginView loginView;
        private ILoginData loginData;

        public CheckUserListener(String userId, ILoginView loginView, ILoginData loginData) {

            this.userId = userId;

            this.loginView = loginView;
            this.loginData = loginData;
        }

        @Override
        public void onSuccess(UserModel object) {

            if (object != null) { // user exists, do something

                loginView.gotoMain();

            } else { // user does not exist, do something

                loginData.createUser(userId);
                loginView.gotoAccountSetup();
            }
        }

        @Override
        public void onCancel() {

        }

        @Override
        public void onError() {

        }
    }
}
