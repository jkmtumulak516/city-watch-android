package com.sofeng.citywatch.splash;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.sofeng.citywatch.R;

public class SplashActivity
        extends AppCompatActivity
        implements ISplashView{

    private ISplashPresenter splashPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        splashPresenter = new SplashPresenter(this);
    }
}
