package com.sofeng.citywatch.splash;

/**
 * Created by JKMT on 07/19/2017.
 */

public class SplashPresenter implements ISplashPresenter {

    private ISplashView splashView;
    private ISplashData splashData;

    public SplashPresenter(ISplashView splashView) {

        this.splashView = splashView;

        splashData = new SplashData();
    }
}
