package com.sofeng.citywatch.dashboard.profile.tabs.allpost;


import android.util.Log;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.sofeng.citywatch.model.CityModel;
import com.sofeng.citywatch.model.PostModel;
import com.sofeng.citywatch.net.ResponseListener;
import com.sofeng.citywatch.util.FirebaseUtil;

import java.util.List;
import java.util.Map;

/**
 * Created by JKMT on 08/05/2017.
 */

public class ProfileAllPostPresenter implements IProfileAllPostPresenter {

    private IProfileAllPostView profileAchievementView;
    private IProfileAllPostData profileAchievementData;

    public ProfileAllPostPresenter(IProfileAllPostView profileAchievementView) {

        this.profileAchievementView = profileAchievementView;

        profileAchievementData = new ProfileAllPostData(profileAchievementView.getContext());
    }

    @Override
    public void updateFeed(String userId) {
        profileAchievementData.getPosts(userId, new FeedListener());

    }

    private class FeedListener implements ResponseListener<List<PostModel>> {

        @Override
        public void onSuccess(List<PostModel> object) {
            FirebaseUtil.syncUsers(profileAchievementView.getPostList(), false);
            FirebaseUtil.syncUsers(object, true);
            profileAchievementView.updateRecyclerView(object);
            profileAchievementView.hideLoadingLayout();
        }

        @Override
        public void onCancel() {
            Log.d("Debug", "[PROFILE-ALL] On cancel");
        }

        @Override
        public void onError() {
            Log.d("Debug", "[PROFILE-ALL] On error");
        }
    }
}
