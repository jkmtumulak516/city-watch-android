package com.sofeng.citywatch.dashboard.map;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.ValueEventListener;
import com.google.maps.android.heatmaps.WeightedLatLng;
import com.sofeng.citywatch.model.InsightModel;
import com.sofeng.citywatch.model.MapPostModel;
import com.sofeng.citywatch.model.TagEnum;
import com.sofeng.citywatch.model.filter.PostFilter;
import com.sofeng.citywatch.net.Endpoints;
import com.sofeng.citywatch.net.MultipleObjectsRequest;
import com.sofeng.citywatch.net.ResponseListener;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * Created by JKMT on 08/05/2017.
 */

public class MapData implements IMapData{

    private static final String requestTag = "MAP";
    private RequestQueue requestQueue;
    private DatabaseReference rootReference;

    public MapData(Context context) {

        rootReference = FirebaseDatabase.getInstance().getReference();
        requestQueue = Volley.newRequestQueue(context);
    }

    @Override
    public void getMapPosts(String cityId, PostFilter filter, final ResponseListener<List<MapPostModel>> listener) {

        String baseUrl = Endpoints.MapPosts.MAP_POSTS;
        String url = baseUrl + "?cityId=" + cityId;

        MultipleObjectsRequest<PostFilter> mapPostsRequest = new MultipleObjectsRequest<>(Request.Method.POST, url, filter, new Response.Listener<Map>() {
            @Override
            public void onResponse(Map response) {
                // converts the map response into a list of PostModels
                List<MapPostModel> mapPosts = MapPostModel.convertMultiple((List<Map>)response.get("mapPosts"));
                // passes the list of posts into the listener
                listener.onSuccess(mapPosts);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                listener.onError();
            }
        });

        requestQueue.add(mapPostsRequest);
    }

    @Override
    public void getInsights(String cityId, TagEnum tag, final ResponseListener<List<WeightedLatLng>> listener) {
        // sets the address location to listen for insights
        DatabaseReference insightReference = rootReference.child("posts")
                .child(cityId).child("statistics").child("tags").child(tag.string);

        // listens to the location once
        insightReference.addListenerForSingleValueEvent(new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // define the type indicator to convert the datasnapshot later
                GenericTypeIndicator<Map<String, InsightModel>> type =
                        new GenericTypeIndicator<Map<String, InsightModel>>() {};

                // converts the datasnapshot into the corresponding map
                final Map<String, InsightModel> insights = dataSnapshot.getValue(type);
                // listens to the current timestamp once
                if (insights != null) {
                    FirebaseDatabase.getInstance().getReference().child("timestamp")
                            .addListenerForSingleValueEvent(new GetTimeListener(insights.values()));
                } else{
                    listener.onSuccess(null);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

                listener.onCancel();
            }

            class GetTimeListener implements ValueEventListener {

                private final Collection<InsightModel> insights;

                private static final double MIN_INTENSITY = 0.3;
                private static final double REMAINDER = 1 - MIN_INTENSITY;

                GetTimeListener(Collection<InsightModel> insights) {

                    this.insights = insights;
                }

                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    // converts the datasnapshot to a long value
                    // and initializes the smallest value AS the largest value
                    long currentTime = dataSnapshot.getValue(Long.class);
                    long smallest = Long.MAX_VALUE;

                    // initializes a list of WeightedLatLng
                    List<WeightedLatLng> wll = new ArrayList<>();

                    // finds the smallest timestamp
                    for (InsightModel insight : insights) {
                        if (smallest > insight.timestamp)
                            smallest = (long)insight.timestamp;
                    }

                    // gets the range defined as:
                    // Range = Current_Timestamp - Smallest_Timestamp
                    long timeRange = currentTime - smallest;

                    // iterates over the insights and converts them into WeightedLatLng
                    for (InsightModel insight : insights) {
                        // the converted intensity of an insight is based on factors:
                        // 1.) the minimum intensity
                        // 2.) the range intensity
                        // because minimum intensity and the maximum possible value of range intensity
                        // add up to One(1), then the range of an insight's intensity is between
                        // minimum intensity and One(1)
                        double rangeIntensity = (REMAINDER * ((currentTime - insight.timestamp) / timeRange));
                        double intensity = MIN_INTENSITY + rangeIntensity;

                        // adds a WeightedLatLng to the list
                        wll.add(new WeightedLatLng(new LatLng(insight.lat, insight.lng), intensity));
                    }

                    // calls the listener and passes the list of WeightedLatLng
                    listener.onSuccess(wll);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                    listener.onCancel();
                }
            }
        });
    }

    @Override
    public void getInsights(String cityId, int tag, final ResponseListener<List<WeightedLatLng>> listener) {

        getInsights(cityId, TagEnum.getTag(tag), listener);
    }
}
