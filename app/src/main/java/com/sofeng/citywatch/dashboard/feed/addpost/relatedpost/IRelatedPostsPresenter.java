package com.sofeng.citywatch.dashboard.feed.addpost.relatedpost;

import com.sofeng.citywatch.model.PostModel;

/**
 * Created by JKMT on 09/22/2017.
 */

public interface IRelatedPostsPresenter {

    /**
     * Set the post model related to current post
     * @param postModel
     */
    void setRelated(String postId, PostModel postModel);
}
