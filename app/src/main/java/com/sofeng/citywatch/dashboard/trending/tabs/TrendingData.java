package com.sofeng.citywatch.dashboard.trending.tabs;

import android.content.Context;
import android.support.annotation.NonNull;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.sofeng.citywatch.model.PostModel;
import com.sofeng.citywatch.model.TagEnum;
import com.sofeng.citywatch.model.filter.PostFilter;
import com.sofeng.citywatch.net.Endpoints;
import com.sofeng.citywatch.net.MultipleObjectsRequest;
import com.sofeng.citywatch.net.ResponseListener;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;
import java.util.Map;

/**
 * Created by JKMT on 09/26/2017.
 */

public class TrendingData implements ITrendingData {

    private static final String requestTag = "TRENDING_FEED";
    private RequestQueue requestQueue;

    public TrendingData(Context context) {

        requestQueue = Volley.newRequestQueue(context);
    }

    @Override
    public void getPosts(String cityId, TagEnum tag, @NonNull final ResponseListener<List<PostModel>> listener) {
        getPosts(cityId, (tag != null ? tag.string : null), listener);
    }

    @Override
    public void getPosts(String cityId, String tag, @NonNull final ResponseListener<List<PostModel>> listener) {

        try {
            // sets the endpoint of the url
            String baseUrl = Endpoints.Posts.TRENDING_POSTS;
            String url = baseUrl + "?cityId=" + cityId;

            if (tag != null) {
                url = url + "&tag=" + URLEncoder.encode(tag, "UTF-8");
            }

            // volley request towards the specified endpoint
            MultipleObjectsRequest<PostFilter> postsRequest = new MultipleObjectsRequest<>(Request.Method.POST, url, null, new Response.Listener<Map>() {
                @Override
                public void onResponse(Map response) {
                    // converts the map response into a list of PostModels
                    List<PostModel> posts = PostModel.convertMultiple((List<Map>)response.get("trendingPosts"));
                    // passes the list of posts into the listener
                    listener.onSuccess(posts);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    listener.onError();
                }
            });

            postsRequest.setTag(requestTag);

            requestQueue.add(postsRequest);
        } catch (UnsupportedEncodingException e) {
            listener.onError();
        }
    }
}
