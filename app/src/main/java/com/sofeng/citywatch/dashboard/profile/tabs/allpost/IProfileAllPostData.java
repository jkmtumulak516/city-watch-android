package com.sofeng.citywatch.dashboard.profile.tabs.allpost;

import com.sofeng.citywatch.model.PostModel;
import com.sofeng.citywatch.net.ResponseListener;

import java.util.List;

/**
 * Created by JKMT on 08/05/2017.
 */

public interface IProfileAllPostData {

    /**
     * Gets all posts based on the User ID passed and executes the listener
     * or error listener on response.
     * @param userId The User's ID. Does not have to be the current User's ID
     * @param listener Listener on response.
     */
    void getPosts(String userId, ResponseListener<List<PostModel>> listener);

}
