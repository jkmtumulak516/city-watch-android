package com.sofeng.citywatch.dashboard.feed.addpost;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.util.ArraySet;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.github.clans.fab.FloatingActionMenu;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.firebase.auth.FirebaseAuth;
import com.iceteck.silicompressorr.SiliCompressor;
import com.sofeng.citywatch.R;
import com.sofeng.citywatch.dashboard.feed.addpost.relatedpost.RelatedPostsActivity;
import com.sofeng.citywatch.model.PostModel;
import com.sofeng.citywatch.model.TagEnum;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Set;

public class AddPostActivity extends AppCompatActivity implements View.OnClickListener, IAddPostView {

    // Activity request codes
    private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
    private static final int CAMERA_CAPTURE_VIDEO_REQUEST_CODE = 200;
    private static final int PICK_GALLERY_REQUEST = 300;
    public static final int MEDIA_TYPE_IMAGE = 1;
    public static final int MEDIA_TYPE_VIDEO = 2;
    private static final int REQUEST_CODE_AUTOCOMPLETE = 9;


    // directory name to store captured images and videos
    private static final String IMAGE_DIRECTORY_NAME = "Hello Camera";

    private Uri fileUri; // file url to store image/video
    private Uri compressUri;// url for compress image/video
    // Google Places Stuff

    // Holds the bounds of the Philippines
    private LatLngBounds BOUNDS_PH = new LatLngBounds(
            new LatLng(4.6145711, 119.6272661), new LatLng(19.966096, 124.173694));
    //Holds the current city that is selected
    private Place place = null;
    private RelativeLayout postSelectedLayout;
    private TextView postLocation;
    private ImageView locationDeselect;

    private ImageView imgPreview;
    private VideoView videoPreview;

    private Boolean isFabOpen = false;
    private com.github.clans.fab.FloatingActionButton fab_Gallery, fab_VideoCam, fab_Camera;
    private FloatingActionMenu fab_Upload;
    private ImageView btnClosePreview;
    private FrameLayout previewContainer;

    private Set<TagEnum> tagEnumSet;
    private String mood;
    private EditText title;
    private EditText description;
    private TextView titleCtr,descriptionCtr;
    private TextView editPlace;
    private double mediaType;
    private String cityId;
    private String userId;

    private ImageView iv_Happy_Active, iv_Sad_Active, iv_Excited_Active, iv_Frustrated_Active, iv_Tired_Active, iv_Amazed_Active;
    private TextView tvSadMood,tvHappyMood,tvExcitedMood,tvTiredMood,tvFrustratedMood,tvAmazedMood;

    private ConstraintLayout postProgressLayout;
    private TextView postProgress;

    private IAddPostPresenter addPostPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_post);
        getSupportActionBar().setTitle("Add Post");

        Bundle b = getIntent().getExtras();
        cityId = b.getString("cityId");
        mediaType = b.getDouble("mediaType");
        if(b.getString("fileUri") != null)
            fileUri = Uri.parse(b.getString("fileUri"));


        tagEnumSet = new ArraySet<TagEnum>();
        mood = "Happy";
        userId = FirebaseAuth.getInstance().getCurrentUser().getUid();

        addPostPresenter = new AddPostPresenter(this);

        initializeView();

        requestRuntimePermission();

        setOnClickListeners();

        isDeviceSupportCamera();


        if (mediaType == PostModel.IMAGE) {
            compressImage();
            previewPictureFromGallery();
        } else if (mediaType == PostModel.VIDEO) {
            previewRecordVideo();
        }
    }

    /**
     * Initialization for every item necessary to be use from the add post xml
     */
    private void initializeView() {
        postProgressLayout = (ConstraintLayout) findViewById(R.id.postProgressLayout);
        postProgress = (TextView) findViewById(R.id.txtPostProgress);
        locationDeselect = (ImageView) findViewById(R.id.btnDeselectPostLocation);
        postSelectedLayout = (RelativeLayout) findViewById(R.id.post_location_selected);
        postLocation = (TextView) findViewById(R.id.txtPostLocation);
        locationDeselect = (ImageView) findViewById(R.id.btnDeselectPostLocation);
        title = (EditText) findViewById(R.id.tv_Title);
        description = (EditText) findViewById(R.id.tv_Description);
        titleCtr=(TextView)findViewById(R.id.tv_TitleCtr);
        descriptionCtr=(TextView)findViewById(R.id.tv_DescriptionCtr);
        editPlace = (TextView) findViewById(R.id.tv_Location);
        imgPreview = (ImageView) findViewById(R.id.imgPreview);
        videoPreview = (VideoView) findViewById(R.id.videoPreview);
        btnClosePreview = (ImageView) findViewById(R.id.btnClosePreview);
        previewContainer = (FrameLayout) findViewById(R.id.PreviewContainer);
        fab_Gallery = (com.github.clans.fab.FloatingActionButton) findViewById(R.id.fab_Gallery);
        fab_VideoCam = (com.github.clans.fab.FloatingActionButton) findViewById(R.id.fab_VideoCam);
        fab_Camera = (com.github.clans.fab.FloatingActionButton) findViewById(R.id.fab_Camera);
        fab_Upload = (com.github.clans.fab.FloatingActionMenu) findViewById(R.id.fab_Upload);
        initMoodSelection();

        title.addTextChangedListener(mTitleTextWatcher);
        description.addTextChangedListener(mDescriptionTextWatcher);
    }

    public void scrollUP(){
        ScrollView scroll = (ScrollView) findViewById(R.id.addpost_scroll);
        scroll.smoothScrollTo(0, 0);
    }

    private  void showTagErrorDialog(){
        AlertDialog alertDialog = new AlertDialog.Builder(AddPostActivity.this).create();
        alertDialog.setTitle("Missing Input");
        alertDialog.setMessage("Select atleast one CityWatch Tag");
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }
    public boolean isTitleFieldEmpty(){
        boolean flag=false;
        if(TextUtils.isEmpty(title.getText().toString())) {
            title.setError("This item cannot be empty");
            flag = true;
            scrollUP();
        }
        return flag;
    }
    public boolean isDescriptionFieldEmpty(){
        boolean flag=false;
        if(TextUtils.isEmpty(description.getText().toString())) {
            description.setError("This item cannot be empty");
            flag=true;
            scrollUP();
        }
        return flag;
    }
    public boolean isLocationEmpty(){
        boolean flag=false;
        if((postLocation.getText().toString()).isEmpty() || postLocation.getText().toString().equals("test")) {
            editPlace.setError("This item cannot be empty");
            flag=true;
            scrollUP();
        }
        return flag;
    }
    public boolean isTagEmpty(){
        boolean flag=false;
        if(tagEnumSet.size()==0){
            showTagErrorDialog();
            flag=true;
        }
        return flag;
    }
    public boolean checkEmptyFields(){
        boolean flag=false;
        if(isTitleFieldEmpty()) {
            flag = true;
        }
        if(isDescriptionFieldEmpty()) {
            flag = true;
        }
        if(isLocationEmpty()) {
            flag = true;
        }
        if (isTagEmpty()) {
            flag = true;
        }

        return flag;
    }

    private TextWatcher mTitleTextWatcher=new TextWatcher() {

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {}

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count,int after) {}

        @Override
        public void afterTextChanged(Editable s) {
            if(s.length()>0){
                titleCtr.setVisibility(View.VISIBLE);
            }else{
                titleCtr.setVisibility(View.GONE);
            }
            titleCtr.setText(String.valueOf(s.length()+"/32"));
        }
    };

    private TextWatcher mDescriptionTextWatcher=new TextWatcher() {

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {}

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count,int after) {}

        @Override
        public void afterTextChanged(Editable s) {
            if(s.length()>0){
                descriptionCtr.setVisibility(View.VISIBLE);
            }else{
                descriptionCtr.setVisibility(View.GONE);
            }
            descriptionCtr.setText(String.valueOf(s.length()+"/250"));
        }
    };

    /**
     * Initialize Moods Selection Area
     */
    private void initMoodSelection() {

        iv_Happy_Active = (ImageView) findViewById(R.id.iv_Happy_Active);
        iv_Sad_Active = (ImageView) findViewById(R.id.iv_Sad_Active);
        iv_Excited_Active = (ImageView) findViewById(R.id.iv_Excited_Active);
        iv_Amazed_Active = (ImageView) findViewById(R.id.iv_Amazed_Active);
        iv_Tired_Active = (ImageView) findViewById(R.id.iv_Tired_Active);
        iv_Frustrated_Active = (ImageView) findViewById(R.id.iv_Frustrated_Active);

        tvHappyMood=(TextView)findViewById(R.id.tv_Happy_Mood);
        tvSadMood=(TextView)findViewById(R.id.tv_Sad_Mood);
        tvAmazedMood=(TextView)findViewById(R.id.tv_Amazed_Mood);
        tvExcitedMood=(TextView)findViewById(R.id.tv_Excited_Mood);
        tvTiredMood=(TextView)findViewById(R.id.tv_Tired_Mood);
        tvFrustratedMood=(TextView)findViewById(R.id.tv_Frustrated_Mood);

        initialMood();
    }

    /**
     * Setup on clicklisteners for buttons
     */
    private void setOnClickListeners() {
        locationDeselect.setOnClickListener(this);
        fab_Gallery.setOnClickListener(this);
        fab_VideoCam.setOnClickListener(this);
        fab_Camera.setOnClickListener(this);
        btnClosePreview.setOnClickListener(this);
        previewContainer.setOnClickListener(this);
        editPlace.setOnClickListener(this);

    }

    /**
     * Check Permission for Phone Storage
     */
    private void requestRuntimePermission() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this,
                        new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
            }
            if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this,
                        new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
            }
        }
    }

    private void displayOriginalSize(){
        File f = new File(fileUri.getPath());
        long size = f.length();
        Log.d("Add Post Original Size:", ""+formatFileSize(size));
    }

    private void displayCompressSize(){
        File f = new File(compressUri.getPath());
        long size = f.length();
        Log.d("Add Post Original Size:", ""+formatFileSize(size));
    }

    private  void compressImage(){
        // External sdcard location
        displayOriginalSize();
        File mediaStorageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                "COMPESS IMAGES");

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d("COMPRESS IMAGES", "Oops! Failed create "
                        + "COMPRESS IMAGES"+ " directory");

            }
        }

        String filePath = SiliCompressor.with(this).compress(fileUri.toString(), mediaStorageDir);
        compressUri=Uri.fromFile(new File(filePath));
        fileUri=compressUri;
        displayCompressSize();

    }



    public static String formatFileSize(final long pBytes){
        String BYTES = "Bytes";
        String MEGABYTES = "MB";
        String KILOBYTES = "kB";
        String GIGABYTES = "GB";
        long KILO = 1024;
        long MEGA = KILO * 1024;
        long GIGA = MEGA * 1024;

        if(pBytes < KILO){
            return pBytes + BYTES;
        }else if(pBytes < MEGA){
            return (int)(0.5 + (pBytes / (double)KILO)) + KILOBYTES;
        }else if(pBytes < GIGA){
            return (int)(0.5 + (pBytes / (double)MEGA)) + MEGABYTES;
        }else {
            return (int)(0.5 + (pBytes / (double)GIGA)) + GIGABYTES;
        }
    }
    /**
     * Handles the click event of the FAB and clear
     *
     * @param v
     */
    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.fab_Gallery:
                getPictureVideoFromGallery();
                break;
            case R.id.fab_VideoCam:
                recordVideo();
                break;
            case R.id.fab_Camera:
                captureImage();
                break;
            case R.id.btnClosePreview:
                fab_Upload.showMenu(true);
                fileUri = null;
                mediaType = PostModel.NONE;
                closePreview();
                break;
            case R.id.tv_Location:
                openPlacesAutocompleteActivity();
                break;
            case R.id.btnDeselectPostLocation:
                clearSelectedLocation();


        }
    }

    /**
     * Click listener for the tag header in tag selection
     * Expands and Retracts containers
     *
     * @param v
     */
    public void tagHeaderClick(View v) {
        //gets the linearlayout that holds all the headers and containers
        ViewGroup parent = (ViewGroup) v.getParent();
        // gets the container next to the header
        ViewGroup container = (ViewGroup) parent.getChildAt(parent.indexOfChild(v) + 1);
        triggerExpandRetract(container);
        triggerHeaderImage(v);
    }

    /**
     * Helper function that sets the visibility of the container
     *
     * @param container
     */
    private void triggerExpandRetract(ViewGroup container) {
        if (container.getVisibility() == View.GONE) {
            container.setVisibility(View.VISIBLE);
        } else {
            container.setVisibility(View.GONE);
        }
    }

    /**
     * Switches the arrow image beside the tag header
     *
     * @param v
     */
    private void triggerHeaderImage(View v) {
        // gets the frame layout that holds the two image
        ViewGroup imgContainer = (ViewGroup) ((ViewGroup) v).getChildAt(1);
        ImageView retract = (ImageView) imgContainer.getChildAt(0);
        ImageView expand = (ImageView) imgContainer.getChildAt(1);

        if (retract.getVisibility() == View.GONE && expand.getVisibility() == View.VISIBLE) {
            retract.setVisibility(View.VISIBLE);
            expand.setVisibility(View.GONE);
        } else {
            retract.setVisibility(View.GONE);
            expand.setVisibility(View.VISIBLE);
        }

    }

    /**
     * Handles the check event of tag selection
     *
     * @param v
     */
    public void onCheck(View v) {
        CheckBox cb = (CheckBox) v;
        int tagId = Integer.parseInt(v.getTag().toString());
        TagEnum tag = TagEnum.getTag(tagId);
        if (cb.isChecked()) {
            tagEnumSet.add(tag);
            Log.d("Debug", "Selected tag: " + tag.string);
        } else {
            tagEnumSet.remove(tag);
            Log.d("Debug", "Removed tag: " + tag.string);
        }

        Log.d("Debug", "Current Tags: " + tagEnumSet.toString());

    }

    private void initialMood(){
        iv_Happy_Active.setVisibility(View.VISIBLE);
        tvHappyMood.setTextColor(getResources().getColor(R.color.colorAccentSecondary));
        mood="Happy";
    }
    /**
     * Unselect Any Mood in Mood Selection Area
     */
    private void disableMoodsSelection() {
        iv_Happy_Active.setVisibility(View.INVISIBLE);
        iv_Sad_Active.setVisibility(View.INVISIBLE);
        iv_Excited_Active.setVisibility(View.INVISIBLE);
        iv_Tired_Active.setVisibility(View.INVISIBLE);
        iv_Amazed_Active.setVisibility(View.INVISIBLE);
        iv_Frustrated_Active.setVisibility(View.INVISIBLE);

        tvHappyMood.setTextColor(getResources().getColor(R.color.textColorPrimary));
        tvSadMood.setTextColor(getResources().getColor(R.color.textColorPrimary));
        tvExcitedMood.setTextColor(getResources().getColor(R.color.textColorPrimary));
        tvTiredMood.setTextColor(getResources().getColor(R.color.textColorPrimary));
        tvAmazedMood.setTextColor(getResources().getColor(R.color.textColorPrimary));
        tvFrustratedMood.setTextColor(getResources().getColor(R.color.textColorPrimary));


    }
    /**
     * Handles the click event of mood radio buttons
     *
     * @param v
     */
    public void onClickMoods(View v){
        ViewGroup vg = (ViewGroup) v;
        ViewGroup container=(ViewGroup) vg.getChildAt(0);
        View emojiActive=container.getChildAt(1);
        TextView emojiText=(TextView) vg.getChildAt(1);

        if (emojiActive.getVisibility() == View.INVISIBLE) {
            disableMoodsSelection();
            emojiActive.setVisibility(View.VISIBLE);
            emojiText.setTextColor(getResources().getColor(R.color.colorAccentSecondary));
            mood=emojiText.getText().toString();
            Log.d("Debug", "[MOOD]: " + mood);
        }
    }

    /**
     * Hides the preview when close button is click
     */
    private void closePreview() {
        previewContainer.setVisibility(View.GONE);
    }

    /**
     * Capturing Camera Image will lauch camera app requrest image capture
     */
    private void captureImage() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);

        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

        // start the image capture Intent
        startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
    }

    /**
     * Choose Video or Photo from gallery
     */
    private void getPictureVideoFromGallery() {
        Intent pickIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        pickIntent.setType("image/* video/*");
        startActivityForResult(pickIntent, PICK_GALLERY_REQUEST);
    }

    /**
     * Click listener for post button. Passes data to the presenter for upload
     *
     * @param v
     */
    public void uploadPost(View v) {
        if(!checkEmptyFields()) {
            addPostPresenter.uploadPost(userId, cityId, title.getText().toString(),
                    description.getText().toString(), mood, tagEnumSet, place.getName().toString(),
                    place.getLatLng().latitude, place.getLatLng().longitude, mediaType, fileUri);
        }
    }

    /**
     * Here we store the file url as it will be null after returning from camera
     * app
     */
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        // save file url in bundle as it will be null on scren orientation
        // changes
        outState.putParcelable("file_uri", fileUri);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        // get the file url
        fileUri = savedInstanceState.getParcelable("file_uri");
    }

    /**
     * Recording video
     */
    private void recordVideo() {
        Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);

        fileUri = getOutputMediaFileUri(MEDIA_TYPE_VIDEO);

        // set video quality
        intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1);

        // set video duration limit
        intent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, 10);

        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri); // set the image file
        // name

        // start the video capture Intent
        startActivityForResult(intent, CAMERA_CAPTURE_VIDEO_REQUEST_CODE);
    }

    /**
     * Receiving activity result method will be called after closing the camera
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // if the result is capturing Image
        if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                // successfully captured the image
                compressImage();
                previewPictureFromGallery();

            } else if (resultCode == RESULT_CANCELED) {
                // user cancelled Image capture
                Toast.makeText(getApplicationContext(),
                        "User cancelled image capture", Toast.LENGTH_SHORT)
                        .show();
            } else {
                // failed to capture image
                Toast.makeText(getApplicationContext(),
                        "Sorry! Failed to capture image", Toast.LENGTH_SHORT)
                        .show();
            }
        } else if (requestCode == CAMERA_CAPTURE_VIDEO_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                // video successfully recorded
                // preview the recorded video

                previewRecordVideo();
            } else if (resultCode == RESULT_CANCELED) {
                // user cancelled recording
                Toast.makeText(getApplicationContext(),
                        "User cancelled video recording", Toast.LENGTH_SHORT)
                        .show();
            } else {
                // failed to record video
                Toast.makeText(getApplicationContext(),
                        "Sorry! Failed to record video", Toast.LENGTH_SHORT)
                        .show();
            }
        } else if (requestCode == PICK_GALLERY_REQUEST) {
            if (resultCode == RESULT_OK) {
                fileUri = data.getData();
                String path = fileUri.getPath();
                if (path.contains("/video/")) {
                    previewVideoFromGallery();
                } else if (path.contains("/images/")) {
                    compressImage();
                    previewPictureFromGallery();
                }

            }

        } else if (requestCode == REQUEST_CODE_AUTOCOMPLETE) {
            if (resultCode == RESULT_OK) {
                // Get the user's selected place from the Intent.
                Place place = PlaceAutocomplete.getPlace(this, data);
                setLocation(place);
                Log.d("Debug", "Place Selected: " + place.getAddress());
            }
        }
    }

    private void previewPictureFromGallery() {
        //handle image
        videoPreview.setVisibility(View.GONE);
        imgPreview.setVisibility(View.VISIBLE);
        previewContainer.setVisibility(View.VISIBLE);

        try {
            Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), fileUri);
            // Log.d(TAG, String.valueOf(bitmap));
            imgPreview.setImageBitmap(bitmap);
            mediaType = PostModel.IMAGE;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void previewVideoFromGallery() {
        //handle video
        imgPreview.setVisibility(View.GONE);
        videoPreview.setVisibility(View.VISIBLE);
        previewContainer.setVisibility(View.VISIBLE);
        try {
            mediaType = PostModel.VIDEO;
            MediaController mc = new MediaController(this);
            videoPreview.setMediaController(mc);

            videoPreview.requestFocus();
            videoPreview.setVideoURI(fileUri);
            videoPreview.start();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Display image from a path to ImageView
     */
    private void previewCapturedImage() {
        fab_Upload.hideMenu(true);
        videoPreview.setVisibility(View.GONE);
        imgPreview.setVisibility(View.VISIBLE);
        previewContainer.setVisibility(View.VISIBLE);
        try {
            // hide video preview


            // bimatp factory
            BitmapFactory.Options options = new BitmapFactory.Options();

            // downsizing image as it throws OutOfMemory Exception for larger
            // images
            options.inSampleSize = 8;

            final Bitmap bitmap = BitmapFactory.decodeFile(fileUri.getPath(),
                    options);
            mediaType = PostModel.IMAGE;
            imgPreview.setImageBitmap(bitmap);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    /**
     * Previewing recorded video
     */
    private void previewRecordVideo() {
        fab_Upload.hideMenu(true);
        previewContainer.setVisibility(View.VISIBLE);
        videoPreview.setVisibility(View.VISIBLE);
        imgPreview.setVisibility(View.GONE);
        try {
            // hide image preview
            imgPreview.setVisibility(View.GONE);
            MediaController mc = new MediaController(this);
            videoPreview.setMediaController(mc);

            videoPreview.requestFocus();
            videoPreview.setVideoPath(fileUri.getPath());
            // start playing
            mediaType = PostModel.VIDEO;
            videoPreview.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Opens the places autocomplete as a new activity
     */
    private void openPlacesAutocompleteActivity() {
        try {

            // The autocomplete activity requires Google Play Services to be available. The intent
            // builder checks this and throws an exception if it is not the case.
            Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                    .setFilter(new AutocompleteFilter.Builder().setTypeFilter(AutocompleteFilter.TYPE_FILTER_NONE).build())
                    .setBoundsBias(BOUNDS_PH)
                    .build(this);
            startActivityForResult(intent, REQUEST_CODE_AUTOCOMPLETE);
        } catch (GooglePlayServicesRepairableException e) {
            // Indicates that Google Play Services is either not installed or not up to date. Prompt
            // the user to correct the issue.
            GoogleApiAvailability.getInstance().getErrorDialog(this, e.getConnectionStatusCode(),
                    0 /* requestCode */).show();
        } catch (GooglePlayServicesNotAvailableException e) {
            // Indicates that Google Play Services is not available and the problem is not easily
            // resolvable.
            String message = "Google Play Services is not available: " +
                    GoogleApiAvailability.getInstance().getErrorString(e.errorCode);

        }
    }

    private void showPlaceSelectedLayout(String address) {
        postSelectedLayout.setVisibility(View.VISIBLE);
        editPlace.setVisibility(View.GONE);
        postLocation.setText(address);
    }

    private void hidePlaceSelectedLayout() {
        postSelectedLayout.setVisibility(View.GONE);
        editPlace.setVisibility(View.VISIBLE);
    }

    private void clearSelectedLocation() {
        this.place = null;
        hidePlaceSelectedLayout();
    }

    private void setLocation(Place place) {
        this.place = place;
        showPlaceSelectedLayout(place.getName().toString());
    }

    /**
     * ------------ Helper Methods ----------------------
     */

    /**
     * Creating file uri to store image/video
     */
    public Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }

    /**
     * returning image / video
     */
    private static File getOutputMediaFile(int type) {

        // External sdcard location
        File mediaStorageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                IMAGE_DIRECTORY_NAME);

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(IMAGE_DIRECTORY_NAME, "Oops! Failed create "
                        + IMAGE_DIRECTORY_NAME + " directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");
        } else if (type == MEDIA_TYPE_VIDEO) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "VID_" + timeStamp + ".mp4");
        } else {
            return null;
        }

        return mediaFile;
    }

    public boolean isDeviceSupportCamera() {
        if (getApplicationContext().getPackageManager().hasSystemFeature(
                PackageManager.FEATURE_CAMERA)) {
            // this device has a camera
            return true;
        } else {
            // no camera on this device
            return false;
        }
    }

    @Override
    public void displayPostProgress() {
        Log.d("Debug", "[LOADING] Displayed loading layout");
        postProgressLayout.setVisibility(View.VISIBLE);
        getSupportActionBar().hide();
        fab_Upload.hideMenu(true);
    }

    @Override
    public void setPostProgress(int progress) {
        postProgress.setText("Uploading... " + progress + "%");
    }

    @Override
    public void hidePostProgress() {
        Log.d("Debug", "[LOADING] Displayed loading layout");
        postProgressLayout.setVisibility(View.GONE);
        getSupportActionBar().show();

    }

    @Override
    public void backToDashboard() {
        finish();
    }

    @Override
    public void gotoRelatedPosts(String postId, List<PostModel> relatedPosts) {
        Intent i = new Intent(this, RelatedPostsActivity.class);
        Bundle b = new Bundle();
        b.putSerializable("potentialPosts", (Serializable) relatedPosts);
        b.putString("postId", postId);
        i.putExtras(b);
        startActivity(i);
        finish();
    }

    @Override
    public Context getContext() {
        return getBaseContext();
    }
}
