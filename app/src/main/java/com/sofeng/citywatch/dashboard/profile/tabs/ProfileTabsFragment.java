package com.sofeng.citywatch.dashboard.profile.tabs;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.auth.FirebaseAuth;
import com.sofeng.citywatch.R;
import com.sofeng.citywatch.dashboard.PagerAdapter;
import com.sofeng.citywatch.dashboard.profile.tabs.allpost.ProfileAllPostFragment;
import com.sofeng.citywatch.dashboard.profile.tabs.citypost.ProfileCityPostFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileTabsFragment extends Fragment {

    private TabLayout tabLayout;

    private ViewPager viewPager;
    private String userId;
    private String cityId;

    public ProfileTabsFragment() {
        // Required empty public constructor
    }

    public static ProfileTabsFragment newInstance() {

        Bundle args = new Bundle();

        ProfileTabsFragment fragment = new ProfileTabsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public static ProfileTabsFragment newInstance(String userId, String cityId) {

        Bundle args = new Bundle();
        args.putString("userId", userId);
        args.putString("cityId", cityId);
        ProfileTabsFragment fragment = new ProfileTabsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private void addTabs(ViewPager viewPager) {

        PagerAdapter adapter = new PagerAdapter(getActivity().getSupportFragmentManager());
        if(userId != null && cityId != null){
            adapter.addFrag(ProfileCityPostFragment.newInstance(userId, cityId), "City Posts");
            adapter.addFrag(ProfileAllPostFragment.newInstance(userId), "All Posts");
        }else{
            adapter.addFrag(ProfileCityPostFragment.newInstance(), "City Posts");
            adapter.addFrag(ProfileAllPostFragment.newInstance(), "All Posts");
        }

        viewPager.setAdapter(adapter);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle b = this.getArguments();
        if (b.getString("userId") != null && b.getString("cityId") != null) {
            userId = b.getString("userId");
            cityId = b.getString("cityId");
        } else {
            userId = FirebaseAuth.getInstance().getCurrentUser().getUid();
        }

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_profile_tabs, container, false);
        viewPager = (ViewPager) view.findViewById(R.id.vp_profile);
        addTabs(viewPager);

        tabLayout = (TabLayout) view.findViewById(R.id.tabs_profile);
        tabLayout.setupWithViewPager(viewPager);

        return view;
    }

}
