package com.sofeng.citywatch.dashboard;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.sofeng.citywatch.model.AvatarEnum;
import com.sofeng.citywatch.model.CityModel;
import com.sofeng.citywatch.model.UserModel;
import com.sofeng.citywatch.net.ResponseListener;

/**
 * Created by JKMT on 09/02/2017.
 */

public class DashboardPresenter implements IDashboardPresenter {

    private IDashboardView dashboardView;
    private IDashboardData dashboardData;
    private FirebaseUser user;

    public DashboardPresenter(IDashboardView dashboardView) {

        this.dashboardView = dashboardView;

        dashboardData = new DashboardData();

        user = FirebaseAuth.getInstance().getCurrentUser();
    }

    @Override
    public void updateViewsToUser() {
        dashboardData.getCurrentUser(user.getUid(), new UserListener());
    }

    @Override
    public void signOut(){
        FirebaseAuth.getInstance().signOut();
        dashboardView.gotoLogin();
    }

    @Override
    public void onDestroy() {
        dashboardData.onDestroy();
    }

    /**
     * A wrapper class for listening for the value of city of the user
     */
    class UserListener implements ResponseListener<UserModel>{

        @Override
        public void onSuccess(UserModel userModel) {
            if(userModel != null) {
                dashboardView.updateActionBarTitle(userModel.currentCity.name);
                dashboardView.updateNavigationDrawerHeader(userModel.displayName,
                        userModel.currentCity.name, userModel.currentCity.id, AvatarEnum.getAvatar(userModel.avatar));
            }
        }

        @Override
        public void onCancel() {

        }

        @Override
        public void onError() {

        }
    }
}
