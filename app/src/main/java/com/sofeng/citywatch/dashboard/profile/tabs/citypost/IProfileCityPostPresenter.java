package com.sofeng.citywatch.dashboard.profile.tabs.citypost;

/**
 * Created by JKMT on 08/05/2017.
 */

public interface IProfileCityPostPresenter {

    /**
     * Requests the server to update feed
     * @param cityId Id of the current city
     * @param userId Id of the user
     */
    void updateFeed(String cityId,String userId);
}
