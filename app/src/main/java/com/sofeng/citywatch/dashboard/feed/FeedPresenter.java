package com.sofeng.citywatch.dashboard.feed;

import com.sofeng.citywatch.model.PostModel;
import com.sofeng.citywatch.model.TagEnum;
import com.sofeng.citywatch.model.filter.PostFilter;
import com.sofeng.citywatch.net.ResponseListener;
import com.sofeng.citywatch.util.FirebaseUtil;

import java.util.List;
import java.util.Set;

/**
 * Created by JKMT on 08/05/2017.
 */

public class  FeedPresenter implements IFeedPresenter {

    private IFeedView feedView;
    private IFeedData feedData;

    public FeedPresenter(IFeedView feedView) {

        this.feedView = feedView;
        // @TODO set necessary properties
        feedData = new FeedData(feedView.getContext());
    }

    @Override
    public void updateFeed(String cityId, String userId, boolean followingFlag, Set<TagEnum> tags, int minRank, int maxRank) {
        PostFilter postFilter = new PostFilter(cityId)
                .setUserId(userId)
                .setFollowingFlag(followingFlag)
                .addTags(tags)
                .setMinRank(minRank)
                .setMaxRank(maxRank);
        feedData.getPosts(postFilter, new FeedListener());
    }

    private class FeedListener implements ResponseListener<List<PostModel>> {

        @Override
        public void onSuccess(List<PostModel> object) {
            FirebaseUtil.syncUsers(feedView.getPostList(), false);
            FirebaseUtil.syncUsers(object, true);
            feedView.updateRecyclerView(object);
            feedView.hideLoadingLayout();
        }

        @Override
        public void onCancel() {

        }

        @Override
        public void onError() {

        }
    }
}
