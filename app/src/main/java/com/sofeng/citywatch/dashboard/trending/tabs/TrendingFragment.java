package com.sofeng.citywatch.dashboard.trending.tabs;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.auth.FirebaseAuth;
import com.sofeng.citywatch.R;
import com.sofeng.citywatch.dashboard.IDashboardView;
import com.sofeng.citywatch.dashboard.feed.BaseFeedView;
import com.sofeng.citywatch.model.TagEnum;

/**
 * A simple {@link Fragment} subclass.
 */
public class TrendingFragment
        extends BaseFeedView
        implements ITrendingView {

    private ITrendingPresenter trendingPresenter;
    private static String TAG_ARG = "TAG";
    private TagEnum tagEnum;
    private String cityId;

    public TrendingFragment() {
        // Required empty public constructor
    }

    public static TrendingFragment newInstance(@Nullable TagEnum tag) {
        TrendingFragment fragment = new TrendingFragment();
        Bundle b = new Bundle();
        b.putSerializable(TAG_ARG, tag);
        fragment.setArguments(b);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        trendingPresenter = new TrendingPresenter(this);
        if (this.getArguments().getSerializable(TAG_ARG) != null)
            tagEnum = (TagEnum) this.getArguments().getSerializable(TAG_ARG);
        else
            tagEnum = null;
        super.onCreate(savedInstanceState);
    }


    @Override
    protected void init() {
        IDashboardView parentActivity = (IDashboardView) getActivity();
        cityId = parentActivity.getCityId();
        refreshFeed();
    }

    @Override
    protected void refreshFeed() {
        IDashboardView parentActivity = (IDashboardView) getActivity();
        cityId = parentActivity.getCityId();
        trendingPresenter.updateFeed(cityId, tagEnum);
    }
}
