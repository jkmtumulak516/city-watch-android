package com.sofeng.citywatch.dashboard.feed.addpost;

import android.net.Uri;
import android.support.annotation.NonNull;

import com.sofeng.citywatch.model.PostModel;
import com.sofeng.citywatch.model.TagEnum;
import com.sofeng.citywatch.net.ResponseListener;
import com.sofeng.citywatch.net.UploadListener;

import java.util.List;
import java.util.Set;

/**
 * Created by JKMT on 08/30/2017.
 */

public interface IAddPostData {

    /**
     * Uploads the user's posts.
     * @param post The post. Contains meta info like the City and User it belongs to.
     * @param tags The post's tags.
     * @param mediaFile The associated media file to the post.
     * @param listener The upload listener.
     */
    void createPost(final PostModel post, final Set<TagEnum> tags, final Uri mediaFile, @NonNull final UploadListener listener);

    /**
     *
     * @param cityId
     * @param postId
     * @param listener
     */
    void getPotentiallyRelatedPosts(String cityId, String postId, final ResponseListener<List<PostModel>> listener);
}
