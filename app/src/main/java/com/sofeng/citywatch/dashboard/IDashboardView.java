package com.sofeng.citywatch.dashboard;

import com.sofeng.citywatch.model.AvatarEnum;

/**
 * Created by JKMT on 09/02/2017.
 */

public interface IDashboardView {

    /**
     * Updates the title in the action bar
     * @param city Text to be set in the action bar
     */
    void updateActionBarTitle(String city);

    /**
     * Updates the header of the navigation drawer
     * @param displayName Display to be set in the header
     * @param city City to be set in the header
     * @param avatar Avatar to be set in the header
     */
    void updateNavigationDrawerHeader(String displayName, String city, String cityId, AvatarEnum avatar);

    /**
     * Goes to Login activity
     */
    void gotoLogin();

    /**
     * Returns CityId
     * @return Id of the city
     */
    String getCityId();
}
