package com.sofeng.citywatch.dashboard.profile;


import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sofeng.citywatch.R;
import com.sofeng.citywatch.dashboard.profile.header.ProfileHeaderFragment;
import com.sofeng.citywatch.dashboard.profile.tabs.ProfileTabsFragment;


/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends Fragment {
    ProfileHeaderFragment profileHeaderFragment;
    ProfileTabsFragment profileTabsFragment;
    CollapsingToolbarLayout collapsingToolbarLayout;
    String userId;
    String cityId;
    Toolbar toolbar;

    public static ProfileFragment newInstance() {
        ProfileFragment fragment = new ProfileFragment();
        Bundle b = new Bundle();
        fragment.setArguments(b);
        return fragment;
    }

    public static ProfileFragment newInstance(String userId, String cityId) {
        ProfileFragment fragment = new ProfileFragment();
        Bundle b = new Bundle();
        b.putString("userId", userId);
        b.putString("cityId", cityId);
        fragment.setArguments(b);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle b = this.getArguments();
        if (b != null && b.getString("userId") != null && b.getString("cityId") != null) {
            userId = b.getString("userId");
            cityId = b.getString("cityId");
            profileHeaderFragment = ProfileHeaderFragment.newInstance(userId);
            profileTabsFragment = ProfileTabsFragment.newInstance(userId,cityId);
        } else {
            profileHeaderFragment = ProfileHeaderFragment.newInstance();
            profileTabsFragment = ProfileTabsFragment.newInstance();
        }
    }

    public void initProfile() {
        //Sets the new toolbar (Collapsing)
        AppCompatActivity activity = (AppCompatActivity) getActivity();
        activity.findViewById(R.id.my_toolbar).setVisibility(View.GONE);
        activity.setSupportActionBar(toolbar);
        activity.supportInvalidateOptionsMenu();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();

        //Navigation Drawer stuff
        DrawerLayout drawer = (DrawerLayout) activity.findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                activity, drawer, toolbar, R.string.nav_bar_open, R.string.nav_bar_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) activity.findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener((NavigationView.OnNavigationItemSelectedListener) activity);
        navigationView.getMenu().getItem(0).setChecked(true);

        //To change the actionbar title (subtitle also)
        collapsingToolbarLayout.setTitle("City Watch");
        collapsingToolbarLayout.setExpandedTitleColor(getResources().getColor(android.R.color.transparent));

        if (profileHeaderFragment == null) {
            profileHeaderFragment = new ProfileHeaderFragment();
        }
        if (profileTabsFragment == null) {
            profileTabsFragment = profileTabsFragment.newInstance();
        }
        transaction.replace(R.id.profile_header, profileHeaderFragment,"header");
        transaction.replace(R.id.fl_posts_achievements, profileTabsFragment);
        transaction.commitAllowingStateLoss();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_profile, container, false);
        collapsingToolbarLayout = (CollapsingToolbarLayout) rootView.findViewById(R.id.profile_collapsing_toolbar);
        toolbar = (Toolbar) rootView.findViewById(R.id.profile_toolbar);
        initProfile();
        return rootView;
    }

    public void updateCollapsingToolbarTitle(String title) {
        collapsingToolbarLayout.setTitle(title);
    }

    @Override
    public void onResume() {
        super.onResume();
        getChildFragmentManager().beginTransaction().add(R.id.profile_header, profileHeaderFragment).commit();
    }

    public void onDestroyView() {
        FragmentManager fm = getFragmentManager();
        ProfileHeaderFragment header = (ProfileHeaderFragment) fm.findFragmentByTag("header");
        FragmentTransaction ft = fm.beginTransaction();
        ft.remove(header);
        ft.commitAllowingStateLoss();
        super.onDestroyView();
    }
}
