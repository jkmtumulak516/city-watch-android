package com.sofeng.citywatch.dashboard.profile.header;

import com.sofeng.citywatch.model.AvatarEnum;
import com.sofeng.citywatch.model.TagEnum;

/**
 * Created by JKMT on 08/05/2017.
 */

public interface IProfileHeaderPresenter {

    /**
     * Loads the profile information of the user
     * to the profile_header
     * @param uid FirebaseUser id of the user
     */
    void loadProfileData(String uid);

    /**
     * Updates the avatar of the user in server
     * @param uid Id of the User
     * @param avatarEnum Selected avatar
     */
    void updateAvatar(String uid, AvatarEnum avatarEnum);

    /**
     * Updates the display name of the user in server
     * @param uid Id of the User
     * @param displayName New display name
     */
    void updateDisplayName(String uid, String displayName);

    /**
     * Updates the profile information of the user
     * @param uid FirebaseUser id of the user
     * @param name Updated name of the user
     * @param avatar Selected avatar of the user
     */
    void updateProfileData(String uid, String name, AvatarEnum avatar);

    /**
     * Handles the follow event of the user
     * @param followingId The User Id of the user who is following the other user.
     * @param followedById The User Id of the user who is being followed by the other user.
     */
    void followUser(String followingId, String followedById);

    /**
     * Handles the unfollow event of the user
     * @param followingId The User Id of the user who is following the other user.
     * @param followedById The User Id of the user who is being followed by the other user.
     */
    void unfollowUser(String followingId, String followedById);

    /**
     * Asks if user is following the viewed user the updates view
     * @param followingId The User Id of the user who is following the other user.
     * @param followedById The User Id of the user who is being followed by the other user.
     */
    void initFollowing(String followingId, String followedById);


}
