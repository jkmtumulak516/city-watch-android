package com.sofeng.citywatch.dashboard.feed.addpost.relatedpost;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.sofeng.citywatch.R;
import com.sofeng.citywatch.model.PostModel;
import com.sofeng.citywatch.post.card.PostCardViewHolder;

import java.util.List;

/**
 * Author: King
 * Date: 9/24/2017
 */

public class RelatedPostsDeckAdapter extends BaseAdapter {

    private List<PostModel> data;
    private Context context;

    public RelatedPostsDeckAdapter(List<PostModel> data, Context context) {
        this.data = data;
        this.context = context;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        View v = convertView;
        if(v == null){
            // normally use a viewholder
            v =  LayoutInflater.from(parent.getContext()).inflate(R.layout.post_card,parent,false);
        }
        PostCardViewHolder viewHolder = new PostCardViewHolder(v);
        viewHolder.setView(data.get(position));
        viewHolder.setCardClickable(false);

        return v;
    }
}
