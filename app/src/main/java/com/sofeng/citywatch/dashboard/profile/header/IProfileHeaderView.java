package com.sofeng.citywatch.dashboard.profile.header;

import com.sofeng.citywatch.model.AvatarEnum;
import com.sofeng.citywatch.model.RankEnum;
import com.sofeng.citywatch.model.RankProgressModel;

/**
 * Created by JKMT on 08/05/2017.
 */

public interface IProfileHeaderView {

    /**
     * Shows the loading overlay for the profile_header
     * and hides the profile_header_content
     */
    void showLoadingOverlay();

    /**
     * Hides the loading overlay for the profile_header
     * and shows the profile_header_content
     */
    void hideLoadingOverlay();

    /**
     * Updates the information displayed in profile header
     * @param displayName Display name of user
     * @param currentLocation Current city of the user
     * @param rank Rank of the user
     * @param avatar Selected avatar of the user
     */
    void updateProfileHeaderContent(String displayName, String currentLocation, RankEnum rank,
                                    RankProgressModel progress, AvatarEnum avatar);

    void updateAchievements(Integer first, Integer second, Integer third);

    /**
     * Initialized the view with the corresponding follow status
     * @param isFollowing If current user is following the viewed user
     */
    void initFollow(boolean isFollowing);

}
