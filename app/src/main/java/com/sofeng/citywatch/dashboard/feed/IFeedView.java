package com.sofeng.citywatch.dashboard.feed;

import android.content.Context;

import com.sofeng.citywatch.model.PostModel;

import java.util.List;

/**
 * Created by JKMT on 08/05/2017.
 */

public interface IFeedView {


    /**
     * Updates the recycler explore in the feed with new posts.
     * @param posts New posts from the server.
     */
    void updateRecyclerView(List<PostModel> posts);

    /**
     * Gets the view's context.
     * @return A context.
     */
    Context getContext();

    /**
     * Hides the loading layout of feed.
     */
    void hideLoadingLayout();

    /**
     * Gets the old post list for desync.
     * @return List of posts.
     */
    List<PostModel> getPostList();
}
