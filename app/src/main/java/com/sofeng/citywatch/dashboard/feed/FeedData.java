package com.sofeng.citywatch.dashboard.feed;

import android.content.Context;
import android.support.annotation.NonNull;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.sofeng.citywatch.model.PostModel;
import com.sofeng.citywatch.model.filter.PostFilter;
import com.sofeng.citywatch.net.Endpoints;
import com.sofeng.citywatch.net.MultipleObjectsRequest;
import com.sofeng.citywatch.net.ResponseListener;

import java.util.List;
import java.util.Map;

/**
 * Created by JKMT on 08/05/2017.
 */

public class FeedData implements IFeedData {

    private static final String requestTag = "WATCH_FEED";
    private RequestQueue requestQueue;

    public FeedData(Context context) {

        requestQueue = Volley.newRequestQueue(context);
    }

    @Override
    public void getPosts(PostFilter postFilter, @NonNull final ResponseListener<List<PostModel>> listener) {
        // sets the endpoint of the url
        String url = Endpoints.Posts.FEED_POSTS;

        // volley request towards the specified endpoint
        MultipleObjectsRequest<PostFilter> postsRequest = new MultipleObjectsRequest<>(Request.Method.POST, url, postFilter, new Response.Listener<Map>() {
            @Override
            public void onResponse(Map response) {
                // converts the map response into a list of PostModels
                List<PostModel> posts = PostModel.convertMultiple((List<Map>)response.get("feedPosts"));
                // passes the list of posts into the listener
                listener.onSuccess(posts);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                listener.onError();
            }
        });

        postsRequest.setTag(requestTag);

        requestQueue.add(postsRequest);
    }
}
