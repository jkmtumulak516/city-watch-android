package com.sofeng.citywatch.dashboard.feed.addpost.relatedpost;

import com.sofeng.citywatch.net.ResponseListener;

import java.util.List;

/**
 * Created by JKMT on 09/22/2017.
 */

public interface IRelatedPostsData {

    /**
     * Sets the relation of the new post with the other given post.
     * @param cityId The city's id that all the posts belong to.
     * @param postId The new post's id.
     * @param relatedPostId The other post that is related to the new post.
     * @param listener The listener on response.
     */
    void setRelatedPosts(String cityId, String postId, String relatedPostId, ResponseListener<Integer> listener);
}
