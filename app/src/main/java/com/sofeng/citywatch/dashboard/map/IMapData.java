package com.sofeng.citywatch.dashboard.map;

import com.google.maps.android.heatmaps.WeightedLatLng;
import com.sofeng.citywatch.model.MapPostModel;
import com.sofeng.citywatch.model.TagEnum;
import com.sofeng.citywatch.model.filter.PostFilter;
import com.sofeng.citywatch.net.ResponseListener;

import java.util.List;
import java.util.Set;

/**
 * Created by JKMT on 08/05/2017.
 */

public interface IMapData {

    /**
     * Gets the posts of a city, converts it into a list of MapPostModel and sends it to the listener.
     * @param cityId City's Id.
     * @param filter Request filter.
     * @param listener Listener to be executed.
     */
    void getMapPosts(String cityId, PostFilter filter, final ResponseListener<List<MapPostModel>> listener);

    /**
     * Gets the insight of a particular tag in a city, converts into a WeightedLatLng and sends it to the listener.
     * @param cityId City's Id.
     * @param tag The tag.
     * @param listener Listener to be executed.
     */
    void getInsights(String cityId, TagEnum tag, final ResponseListener<List<WeightedLatLng>> listener);

    /**
     * Gets the insight of a particular tag in a city, converts into a list of WeightedLatLng and sends it to the listener.
     * @param cityId City's Id.
     * @param tag The tag's Id.
     * @param listener Listener to be executed.
     */
    void getInsights(String cityId, int tag, final ResponseListener<List<WeightedLatLng>> listener);
}
