package com.sofeng.citywatch.dashboard.map;

import com.sofeng.citywatch.model.TagEnum;

import java.util.Set;

/**
 * Created by JKMT on 08/05/2017.
 */

public interface IMapPresenter {

    /**
     * Requests server to load map posts of a city filtered by the given tags.
     * @param cityId Id of the current City
     * @param userId Id of the current User
     * @param followingFlag Flag to show only following or not
     * @param tags Tags to filter the posts
     * @param minRank Minimum rank filter
     * @param maxRank Maximum rank filter
     */
    void getMapPosts(String cityId, String userId, boolean followingFlag, Set<TagEnum> tags, int minRank, int maxRank);

    /**
     * Requests server to load insights for a city and for a given tag
     * @param cityId Id of the Current City
     * @param tagEnum The tag
     */
    void getInsights(String cityId, TagEnum tagEnum);
}
