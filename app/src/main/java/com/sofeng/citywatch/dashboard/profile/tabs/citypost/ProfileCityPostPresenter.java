package com.sofeng.citywatch.dashboard.profile.tabs.citypost;

import android.util.Log;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.sofeng.citywatch.model.PostModel;
import com.sofeng.citywatch.net.ResponseListener;
import com.sofeng.citywatch.util.FirebaseUtil;

import java.util.List;
import java.util.Map;

/**
 * Created by JKMT on 08/05/2017.
 */

public class ProfileCityPostPresenter implements IProfileCityPostPresenter {

    private IProfileCityPostView profilePostView;
    private IProfileCityPostData profilePostData;

    public ProfileCityPostPresenter(IProfileCityPostView profilePostView) {

        this.profilePostView = profilePostView;

        profilePostData = new ProfileCityPostData(profilePostView.getContext());
    }

    @Override
    public void updateFeed(String cityId, String userId) {
        profilePostData.getPosts(userId, cityId, new FeedListener());
    }


    private class FeedListener implements ResponseListener<List<PostModel>> {

        @Override
        public void onSuccess(List<PostModel> object) {
            FirebaseUtil.syncUsers(profilePostView.getPostList(), false);
            FirebaseUtil.syncUsers(object, true);
            profilePostView.updateRecyclerView(object);
            profilePostView.hideLoadingLayout();
        }

        @Override
        public void onCancel() {

        }

        @Override
        public void onError() {

        }
    }
}
