package com.sofeng.citywatch.dashboard.feed.addpost.relatedpost;

import android.util.Log;

import com.sofeng.citywatch.model.PostModel;
import com.sofeng.citywatch.net.ResponseListener;

/**
 * Created by JKMT on 09/22/2017.
 */

public class RelatedPostsPresenter implements IRelatedPostsPresenter {

    private IRelatedPostsView relatedPostsView;
    private IRelatedPostsData relatedPostsData;

    public RelatedPostsPresenter(IRelatedPostsView relatedPostsView) {

        this.relatedPostsView = relatedPostsView;

        relatedPostsData = new RelatedPostsData();
    }

    @Override
    public void setRelated(String postId, PostModel postModel) {
        relatedPostsData.setRelatedPosts(postModel.cityId, postId, postModel.id, new RelatedPostListener());
    }

    class RelatedPostListener implements ResponseListener<Integer>{

        @Override
        public void onSuccess(Integer object) {
            Log.d("Debug", "[RELATED-POST] Success");
        }

        @Override
        public void onCancel() {
            Log.d("Debug", "[RELATED-POST] Cancel");
        }

        @Override
        public void onError() {
            Log.d("Debug", "[RELATED-POST] Error");
        }
    }

}

