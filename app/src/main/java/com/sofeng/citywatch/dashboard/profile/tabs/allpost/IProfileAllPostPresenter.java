package com.sofeng.citywatch.dashboard.profile.tabs.allpost;

/**
 * Created by JKMT on 08/05/2017.
 */

public interface IProfileAllPostPresenter {

    /**
     * Requests the server to update feed
     * @param userId Id of the user
     */
    void updateFeed(String userId);
}
