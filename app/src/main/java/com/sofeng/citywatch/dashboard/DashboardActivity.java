package com.sofeng.citywatch.dashboard;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.nfc.Tag;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.PersistableBundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.internal.BottomNavigationMenuView;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.clans.fab.FloatingActionMenu;
import com.sofeng.citywatch.R;
import com.sofeng.citywatch.city.CityMenuActivity;
import com.sofeng.citywatch.dashboard.feed.FeedFragment;
import com.sofeng.citywatch.dashboard.feed.addpost.AddPostActivity;
import com.sofeng.citywatch.dashboard.map.MapFragment;
import com.sofeng.citywatch.dashboard.profile.ProfileFragment;
import com.sofeng.citywatch.dashboard.trending.MasterFragment;
import com.sofeng.citywatch.filter.FilterActivity;
import com.sofeng.citywatch.login.LoginActivity;
import com.sofeng.citywatch.model.AvatarEnum;
import com.sofeng.citywatch.model.PostModel;
import com.sofeng.citywatch.model.TagEnum;
import com.sofeng.citywatch.model.filter.PostFilter;
import com.sofeng.citywatch.util.CityFormatter;
import com.sofeng.citywatch.util.ImageLoader;

import java.io.File;
import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Set;

import at.favre.lib.dali.Dali;
import at.favre.lib.dali.builder.nav.DaliBlurDrawerToggle;
import at.favre.lib.dali.builder.nav.NavigationDrawerListener;

public class DashboardActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, IDashboardView {
    public static String cityId;

    private BottomNavigationView navigation;

    private FeedFragment feedFragment;

    private MapFragment mapFragment;

    private MasterFragment masterFragment;

    private ProfileFragment profileFragment;

    private IDashboardPresenter dashboardPresenter;

    private FrameLayout leftDrawer;

    private DrawerLayout drawer;

    private DaliBlurDrawerToggle drawerToggle;

    private NavigationView navigationView;

    private TextView signOut;

    private AlertDialog insightDialog;

    private boolean isInitialized = false;

    private FloatingActionMenu myFab;

    Fragment selectedFragment;

    // Activity request codes
    private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
    private static final int CAMERA_CAPTURE_VIDEO_REQUEST_CODE = 200;
    private static final int PICK_GALLERY_REQUEST = 300;
    public static final int MEDIA_TYPE_IMAGE = 1;
    public static final int MEDIA_TYPE_VIDEO = 2;
    private static final int REQUEST_CODE_AUTOCOMPLETE = 9;
    private static final int REQUEST_CODE_FILTER = 10;

    private com.github.clans.fab.FloatingActionButton fab_Gallery, fab_VideoCam, fab_Camera, fab_Text;

    // directory name to store captured images and videos
    private static final String IMAGE_DIRECTORY_NAME = "Hello Camera";

    private Uri fileUri;

    private PostFilter postFilter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        dashboardPresenter = new DashboardPresenter(this);
        initMain();

    }

    @Override
    public void onPostCreate(@Nullable Bundle savedInstanceState, @Nullable PersistableBundle persistentState) {
        super.onPostCreate(savedInstanceState, persistentState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        drawerToggle.syncState();
    }

    @Override
    protected void onResume() {
        super.onResume();
        dashboardPresenter.updateViewsToUser();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        dashboardPresenter.onDestroy();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        boolean drawerOpen = drawer.isDrawerOpen(leftDrawer);

        // Removes filter menu item depending on the selected fragment
        if (selectedFragment instanceof MasterFragment || selectedFragment instanceof ProfileFragment) {
            menu.getItem(1).setVisible(false);
        } else {
            menu.getItem(1).setVisible(true);
        }

        return super.onPrepareOptionsMenu(menu);
    }

    /**
     * Initialize the dependencies of the activity
     */
    private void initMain() {

        setupDrawer();
        setupNavigationDrawer();
        leftDrawer = (FrameLayout) findViewById(R.id.left_drawer);
        navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        setupFragments();
        dashboardPresenter.updateViewsToUser();
        disableShiftMode(navigation);
        initFab();
        initInsightDialog();
        postFilter = new PostFilter(cityId);
    }

    private void initInsightDialog(){
        insightDialog = new AlertDialog.Builder(this)
                .setSingleChoiceItems(TagEnum.getAllNames(), 0, null)
                .setPositiveButton("Select", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                        int selectedPosition = ((AlertDialog)dialog).getListView().getCheckedItemPosition();
                        if(mapFragment != null){
                            TagEnum tag = TagEnum.values()[selectedPosition];
                            mapFragment.updateInsightFilter(tag);
                        }
                    }
                }).create();
    }

    private void initFab() {
        myFab = (FloatingActionMenu) findViewById(R.id.floatingActionButton);
        fab_Gallery = (com.github.clans.fab.FloatingActionButton) findViewById(R.id.fab_Gallery);
        fab_VideoCam = (com.github.clans.fab.FloatingActionButton) findViewById(R.id.fab_VideoCam);
        fab_Camera = (com.github.clans.fab.FloatingActionButton) findViewById(R.id.fab_Camera);
        fab_Text = (com.github.clans.fab.FloatingActionButton) findViewById(R.id.fab_Text);

        fab_Gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestRuntimePermission();
                getPictureVideoFromGallery();
            }
        });

        fab_VideoCam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestRuntimePermission();
                recordVideo();
            }
        });

        fab_Camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestRuntimePermission();
                captureImage();
            }
        });

        fab_Text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoAddPost(PostModel.NONE, null);
            }
        });
    }


    /**
     * Check Permission for Phone Storage
     */
    private void requestRuntimePermission() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this,
                        new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
            }
            if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this,
                        new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
            }
        }
    }

    /**
     * Sets up the drawer to shown by the NavigationDrawer
     */
    private void setupDrawer() {
        // Setups Toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("CityWatch");

        // Setups drawer in Navigation Drawer
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawerToggle = Dali.create(this).constructNavToggle(this, drawer,
                toolbar, R.string.drawer_open, R.string.drawer_close, new NavigationDrawerListener() {
                    @Override
                    public void onDrawerClosed(View view) {
                        invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
                    }

                    /** Called when a drawer has settled in a completely open state. */
                    public void onDrawerOpened(View drawerView) {
                        invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
                    }
                });
        drawerToggle.setDrawerIndicatorEnabled(true);
        // Set the drawer toggle as the DrawerListener
        drawer.addDrawerListener(drawerToggle);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        drawerToggle.syncState();
    }

    /**
     * Sets up the NavigationDrawer
     */
    private void setupNavigationDrawer() {
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.getMenu().getItem(0).setChecked(true);
        signOut = (TextView) navigationView.findViewById(R.id.nav_signout);
        // Sets up the sign out button in navigation drawer
        signOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dashboardPresenter.signOut();
            }
        });
    }

    /**
     * Sets up the fragments needed in dashboard
     */
    private void setupFragments() {

        feedFragment = FeedFragment.newInstance();
        mapFragment = MapFragment.newInstance();
        masterFragment = MasterFragment.newInstance();
        profileFragment = ProfileFragment.newInstance();
    }

    /**
     * Sets up the first fragment to be shown
     */
    private void setupStartFragment() {

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        transaction.replace(R.id.content, feedFragment);
        transaction.commit();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.search_appbar:
                //TODO Implement search
                break;
            case R.id.filter:
                if (selectedFragment instanceof MapFragment) {
                    if( ((MapFragment) selectedFragment).isPostMode()){
                        gotoFilter();
                    }else {
                        insightDialog.show();
                    }
                }else{
                    gotoFilter();
                }
                break;
            default:
        }

        return true;
    }

    /**
     * Listener for the bottom navigation tabs
     */
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {

            selectedFragment = null;
            invalidateOptionsMenu();
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    selectedFragment = feedFragment;
                    if(myFab.isMenuButtonHidden()){
                        myFab.showMenuButton(true);
                    }
                    findViewById(R.id.my_toolbar).setVisibility(View.VISIBLE);
                    break;
                case R.id.navigation_map:
                    selectedFragment = mapFragment;
                    findViewById(R.id.my_toolbar).setVisibility(View.VISIBLE);
                    if(!myFab.isMenuButtonHidden()){
                        myFab.hideMenuButton(true);
                    }
                    break;
                case R.id.navigation_trending:
                    selectedFragment = masterFragment;
                    findViewById(R.id.my_toolbar).setVisibility(View.VISIBLE);
                    if(myFab.isMenuButtonHidden()){
                        myFab.showMenuButton(true);
                    }
                    break;
                case R.id.navigation_profile:
                    selectedFragment = profileFragment;
                    if(!myFab.isMenuButtonHidden()){
                        myFab.hideMenuButton(true);
                    }
                    break;
            }

            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

            transaction.replace(R.id.content, selectedFragment);
            transaction.commit();
            return true;
        }

    };

    /**
     * Disables Shift mode of Bottom Navigation
     *
     * @param view
     */
    public void disableShiftMode(BottomNavigationView view) {

        BottomNavigationMenuView menuView = (BottomNavigationMenuView) view.getChildAt(0);

        try {

            Field shiftingMode = menuView.getClass().getDeclaredField("mShiftingMode");
            shiftingMode.setAccessible(true);
            shiftingMode.setBoolean(menuView, false);
            shiftingMode.setAccessible(false);

        } catch (NoSuchFieldException e) {

            Log.e("BNVHelper", "Unable to get shift mode field", e);

        } catch (IllegalAccessException e) {

            Log.e("BNVHelper", "Unable to change value of shift mode", e);
        }
    }

    /**
     * Listener for the navigation item
     *
     * @param item
     * @return
     */
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_dashboard:
                item.setChecked(true);
                //TODO implement start activity dashboard
                break;
            case R.id.nav_city:
                item.setChecked(true);
                //implement start activity cities
                Intent i = new Intent(getBaseContext(), CityMenuActivity.class);
                i.putExtra("cityId", cityId);
                startActivity(i);
                break;
            case R.id.nav_leaderboard:
                item.setChecked(true);
                //TODO implement start activity leaderboard
                break;
            default:
        }
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_app, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void updateActionBarTitle(String city) {
        getSupportActionBar().setTitle(CityFormatter.getMainAddress(city));
    }

    @Override
    public void updateNavigationDrawerHeader(String displayName, String city, String cityId, AvatarEnum avatar) {
        if (((TextView) navigationView.findViewById(R.id.nav_displayName)) != null &&
                ((TextView) navigationView.findViewById(R.id.nav_address)) != null) {
            ((TextView) navigationView.findViewById(R.id.nav_displayName)).setText(displayName);
            ((TextView) navigationView.findViewById(R.id.nav_address)).setText(city);
            this.cityId = cityId;
            ImageLoader.getInstance().loadImageFromUrl(this, avatar.url, (ImageView) navigationView.findViewById(R.id.nav_avatar));

            if (!isInitialized) {
                setupStartFragment();
                isInitialized = true;
            }
        }
    }

    public void gotoFilter() {
        Intent i = new Intent(this, FilterActivity.class);
        i.putExtra("postFilter", postFilter);
        startActivityForResult(i, REQUEST_CODE_FILTER);
        overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
    }

    @Override
    public void gotoLogin() {
        Intent i = new Intent(this, LoginActivity.class);
        startActivity(i);
        finish();
    }

    @Override
    public String getCityId() {
        return cityId;
    }

    /**
     * Capturing Camera Image will lauch camera app requrest image capture
     */
    private void captureImage() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);

        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

        // start the image capture Intent
        startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
    }

    /**
     * Choose Video or Photo from gallery
     */
    private void getPictureVideoFromGallery() {
        Intent pickIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        pickIntent.setType("image/* video/*");
        startActivityForResult(pickIntent, PICK_GALLERY_REQUEST);
    }

    /**
     * Recording video
     */
    private void recordVideo() {
        Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);

        fileUri = getOutputMediaFileUri(MEDIA_TYPE_VIDEO);

        // set video quality
        intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1);

        // set video duration limit
        intent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, 10);

        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri); // set the image file
        // name

        // start the video capture Intent
        startActivityForResult(intent, CAMERA_CAPTURE_VIDEO_REQUEST_CODE);
    }

    private void gotoAddPost(double mediaType, Uri fileUri) {
        Log.d("Debug", "CityId : " + this.getCityId());

        Intent addPostActivity = new Intent(this, AddPostActivity.class);
        addPostActivity.putExtra("cityId", this.getCityId());
        addPostActivity.putExtra("mediaType", mediaType);
        //getActivity().getContentResolver().notifyChange(fileUri,null);
        if (null != fileUri)
            addPostActivity.putExtra("fileUri", fileUri.toString());
        startActivity(addPostActivity);
    }

    /**
     * Here we store the file url as it will be null after returning from camera
     * app
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {

        // save file url in bundle as it will be null on scren orientation
        // changes
        outState.putParcelable("file_uri", fileUri);
        super.onSaveInstanceState(outState);

    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        if (savedInstanceState != null)
            fileUri = savedInstanceState.getParcelable("file_uri");
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        // if the result is capturing Image
        if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                gotoAddPost(PostModel.IMAGE, fileUri);

            } else if (resultCode == RESULT_CANCELED) {

            } else {

            }
        } else if (requestCode == CAMERA_CAPTURE_VIDEO_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                gotoAddPost(PostModel.VIDEO, fileUri);
            } else if (resultCode == RESULT_CANCELED) {

            } else {

            }
        } else if (requestCode == PICK_GALLERY_REQUEST) {
            if (resultCode == RESULT_OK) {
                fileUri = data.getData();
                String path = fileUri.getPath();
                if (path.contains("/video/")) {
                    gotoAddPost(PostModel.VIDEO, fileUri);
                } else if (path.contains("/images/")) {
                    gotoAddPost(PostModel.IMAGE, fileUri);
                }

            }

        } else if (requestCode == REQUEST_CODE_FILTER) {
            if (resultCode == RESULT_OK) {
                postFilter = (PostFilter) data.getExtras().getSerializable("postFilter");
                updateFilters(postFilter.isFollowingFlag(), postFilter.getTags(),
                        postFilter.getMinRank(), postFilter.getMaxRank());
            }
        }
    }

    private void updateFilters(boolean followingFlag, Set<TagEnum> tags, int minRank, int maxRank) {
        feedFragment.updateFilters(followingFlag, tags, minRank, maxRank);
        mapFragment.updateFilters(followingFlag, tags, minRank, maxRank);
    }

    /**
     * ------------ Helper Methods ----------------------
     */

    /**
     * Creating file uri to store image/video
     */
    public Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }

    /**
     * returning image / video
     */
    private static File getOutputMediaFile(int type) {

        // External sdcard location
        File mediaStorageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                IMAGE_DIRECTORY_NAME);

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(IMAGE_DIRECTORY_NAME, "Oops! Failed create "
                        + IMAGE_DIRECTORY_NAME + " directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");
        } else if (type == MEDIA_TYPE_VIDEO) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "VID_" + timeStamp + ".mp4");
        } else {
            return null;
        }

        return mediaFile;
    }
}

