package com.sofeng.citywatch.dashboard.trending.tabs;

import com.sofeng.citywatch.model.TagEnum;

/**
 * Created by JKMT on 09/26/2017.
 */

public interface ITrendingPresenter {

    /**
     * Requests to update trending feed
     * @param cityId Id of the current City
     * @param tag Category of the feed
     */
    void updateFeed(String cityId, TagEnum tag);
}
