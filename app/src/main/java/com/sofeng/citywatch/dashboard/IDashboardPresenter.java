package com.sofeng.citywatch.dashboard;

/**
 * Created by JKMT on 09/02/2017.
 */

public interface IDashboardPresenter {

    /**
     * Gets the default location from the user
     * and sets it as the title in the action bar
     */
    void updateViewsToUser();

    /**
     * Sign out the user in firebaseAuth
     */
    void signOut();

    void onDestroy();
}
