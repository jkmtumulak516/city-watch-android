package com.sofeng.citywatch.dashboard.profile;

import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.sofeng.citywatch.R;
import com.sofeng.citywatch.dashboard.profile.header.ProfileHeaderFragment;
import com.sofeng.citywatch.dashboard.profile.tabs.ProfileTabsFragment;

public class ProfilePreviewActivity extends AppCompatActivity {

    ProfileHeaderFragment profileHeaderFragment;
    ProfileTabsFragment profileTabsFragment;
    CollapsingToolbarLayout collapsingToolbarLayout;
    Toolbar toolbar;
    String userId;
    String cityId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_preview);
        userId = getIntent().getExtras().getString("userId");
        cityId = getIntent().getExtras().getString("cityId");
        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.profile_collapsing_toolbar);
        toolbar = (Toolbar) findViewById(R.id.profile_toolbar);
        initProfile();
    }

    public void initProfile() {
        setSupportActionBar(toolbar);

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        collapsingToolbarLayout.setTitle("City Watch");
        collapsingToolbarLayout.setExpandedTitleColor(getResources().getColor(android.R.color.transparent));

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (profileHeaderFragment == null) {
            profileHeaderFragment = profileHeaderFragment.newInstance(userId);
        }
        if (profileTabsFragment == null) {
            profileTabsFragment = profileTabsFragment.newInstance(userId, cityId);
        }
        transaction.replace(R.id.profile_header_preview, profileHeaderFragment,"header");
        transaction.replace(R.id.fl_posts_achievements_preview, profileTabsFragment);
        transaction.commitAllowingStateLoss();
    }

    public void updateCollapsingToolbarTitle(String title) {
        collapsingToolbarLayout.setTitle(title);
    }

    public void onResume() {
        super.onResume();
        //getSupportFragmentManager().beginTransaction().add(R.id.profile_header_preview, profileHeaderFragment).commit();
    }

}
