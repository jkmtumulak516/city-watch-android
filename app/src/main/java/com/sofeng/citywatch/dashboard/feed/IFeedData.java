package com.sofeng.citywatch.dashboard.feed;

import com.sofeng.citywatch.model.PostModel;
import com.sofeng.citywatch.model.filter.PostFilter;
import com.sofeng.citywatch.net.ResponseListener;

import java.util.List;

/**
 * Created by JKMT on 08/05/2017.
 */

public interface IFeedData {

    /**
     * Gets all the posts in a city sorted from newest posts to oldest posts.
     * @param postFilter The filter for posts.
     * @param listener The listener on response.
     */
    void getPosts(PostFilter postFilter, ResponseListener<List<PostModel>> listener);
}
