package com.sofeng.citywatch.dashboard.feed.addpost;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v8.renderscript.ScriptIntrinsicBLAS;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.Transaction;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnPausedListener;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.sofeng.citywatch.model.PostModel;
import com.sofeng.citywatch.model.TagEnum;
import com.sofeng.citywatch.model.filter.PostFilter;
import com.sofeng.citywatch.net.Endpoints;
import com.sofeng.citywatch.net.MultipleObjectsRequest;
import com.sofeng.citywatch.net.ResponseListener;
import com.sofeng.citywatch.net.UploadListener;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by JKMT on 08/30/2017.
 */

public class AddPostData implements IAddPostData {

    private static final String requestTag = "ADD_POST";

    private RequestQueue requestQueue;
    private DatabaseReference databaseReference;
    private StorageReference storageReference;

    public AddPostData(Context context) {

        databaseReference = FirebaseDatabase.getInstance().getReference();

        storageReference = FirebaseStorage.getInstance().getReference();

        requestQueue = Volley.newRequestQueue(context);
    }

    @Override
    public void createPost(final PostModel post, final Set<TagEnum> tags,
                           @Nullable final Uri mediaFile, @Nullable final UploadListener listener) {
        // builds the database address of the post
        final DatabaseReference postReference = databaseReference
                .child("posts").child(post.cityId).child("content").push();

        // set the post's id, tags, and votes
        post.id = postReference.getKey();
        post.tags = getTags(tags);
        post.voteCount = 0;

        // generates the parent tags based on the tags given to be uploaded later
        final Map<String, Boolean> parentTags = getParentTags(tags);

        // if the media type has been set to image or video
        // calls method to upload media and then put post
        // else it'll just put the post through a transaction
        if (post.mediaType != PostModel.NONE) {
            uploadMediaAndPost(post, parentTags, mediaFile, postReference, listener);
        } else {
            postReference.runTransaction(new PutPostTransaction(post, parentTags, listener));
        }
    }

    @Override
    public void getPotentiallyRelatedPosts(String cityId, String postId, final ResponseListener<List<PostModel>> listener) {

        String baseUrl = Endpoints.Posts.POTENTIALLY_RELATED_POSTS;
        String url = baseUrl + "?cityId=" + cityId + "&postId=" + postId;

        MultipleObjectsRequest<PostFilter> postsRequest = new MultipleObjectsRequest<>(Request.Method.GET, url, null, new Response.Listener<Map>() {
            @Override
            public void onResponse(Map response) {
                // converts the map response into a list of PostModels
                List<PostModel> posts = PostModel.convertMultiple((List<Map>)response.get("relatedPosts"));
                // passes the list of posts into the listener
                listener.onSuccess(posts);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                listener.onError();
            }
        });

        postsRequest.setTag(requestTag);

        requestQueue.add(postsRequest);
    }

    /**
     * Uploads a posts associated media file into storage, and then, on success,
     * uploads the post data into the database.
     * @param post The post to added.
     * @param parentTags The parent tags of the post's tags.
     * @param mediaFile The post's associated media file.
     * @param postReference The database address of the post.
     * @param listener The upload listener for managing the upload.
     */
    @SuppressWarnings("VisibleForTests")
    private void uploadMediaAndPost(final PostModel post, final Map<String, Boolean> parentTags,
                                    final Uri mediaFile, final DatabaseReference postReference,
                                    final UploadListener listener) {
        // builds the storage address of the media file
        StorageReference mediaReference = storageReference.child("posts").child(post.cityId);

        // begins the media file's upload to the storage
        mediaReference.putFile(mediaFile).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                // sets the posts media url then runs a transaction
                // to set the main post data and its parent tags
                post.mediaUrl = taskSnapshot.getDownloadUrl().toString();
                postReference.runTransaction(new PutPostTransaction(post, parentTags, listener));
            }
        }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                // gets the current progress of the upload:
                // progress = uploadedBytes / totalBytes
                double progress = taskSnapshot.getBytesTransferred() / (double)taskSnapshot.getTotalByteCount();

                // passes the progress into the listener
                listener.onProgress(progress);
            }
        }).addOnPausedListener(new OnPausedListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onPaused(UploadTask.TaskSnapshot taskSnapshot) {
                // gets the current progress of the upload:
                // progress = uploadedBytes / totalBytes
                double progress = taskSnapshot.getBytesTransferred() / (double)taskSnapshot.getTotalByteCount();

                // passes the progress into the listener
                listener.onPaused(progress);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                // passes the exception into the listener
                listener.onFailure(e);
            }
        });
    }

    /**
     * Gets a set of parent tags of the given set.
     * @param tags A set of TagEnums.
     * @return A set of TagEnums that are parents to the given tags.
     */
    private Map<String, Boolean> getParentTags(Set<TagEnum> tags) {
        // instantiate map to return
        Map<String, Boolean> parentTags = new HashMap<>();

        // iterates over all tags and turns them into <String, Boolean> pairs
        // where String is the tag's name and Boolean is true
        for (TagEnum tag : tags) {
            // will only add the tag to the map if:
            // - it has a parent
            // - is not contained in the original tags
            // - has not already been added to the parent tags
            if (tag.parent != null && !tags.contains(tag.parent)
                    && !parentTags.containsKey(tag.parent.string)) {
                parentTags.put(tag.parent.string, true);
            }
        }

        return parentTags;
    }

    /**
     * Converts the set of tags int a Map where the value inside the set are turned into keys and all the values are true.
     * @param tags A set of TagEnums.
     * @return A converted map of the given set.
     */
    private Map<String, Boolean> getTags(Set<TagEnum> tags) {
        // instantiate map to return
        Map<String, Boolean> tagMap = new HashMap<>();

        // iterates over all tags and turns them into <String, Boolean> pairs
        // where String is the tag's name and Boolean is true
        for (TagEnum tag : tags) {
            tagMap.put(tag.string, true);
        }

        return tagMap;
    }

    /**
     * Transaction handler for putting a post and its parent tags.
     */
    private class PutPostTransaction implements Transaction.Handler {

        private final PostModel post;
        private final Map<String, Boolean> parentTags;
        private final UploadListener listener;

        public PutPostTransaction(PostModel post, Map<String, Boolean> parentTags, UploadListener listener) {

            this.post = post;
            this.parentTags = parentTags;
            this.listener = listener;
        }

        @Override
        public Transaction.Result doTransaction(MutableData mutableData) {
            // puts the post into the database
            mutableData.setValue(post);
            // puts the parent tags int the database
            mutableData.child("parentTags").setValue(parentTags);
            // puts the post's timestamp
            mutableData.child("timestamp").setValue(ServerValue.TIMESTAMP);

            // finishes the transaction
            return Transaction.success(mutableData);
        }

        @Override
        public void onComplete(DatabaseError databaseError, boolean b, DataSnapshot dataSnapshot) {

            if (b) {
                listener.onSuccess(post.id);
            } else {
                listener.onFailure(databaseError.toException());
            }
        }
    }
}
