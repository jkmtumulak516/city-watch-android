package com.sofeng.citywatch.dashboard.profile.header;


import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.lzyzsd.circleprogress.DonutProgress;
import com.google.firebase.auth.FirebaseAuth;
import com.sofeng.citywatch.R;
import com.sofeng.citywatch.dashboard.profile.ProfileFragment;
import com.sofeng.citywatch.dashboard.profile.ProfilePreviewActivity;
import com.sofeng.citywatch.model.AvatarEnum;
import com.sofeng.citywatch.model.RankEnum;
import com.sofeng.citywatch.model.RankProgressModel;
import com.sofeng.citywatch.util.ImageLoader;

import at.favre.lib.dali.Dali;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileHeaderFragment
        extends Fragment
        implements IProfileHeaderView {

    private IProfileHeaderPresenter profileHeaderPresenter;

    private FrameLayout profileHeaderLoadingOverlay;

    private FrameLayout profileHeaderContent;

    private TextView profileHeaderDisplayName;

    private TextView profileHeaderCurrentLocation;

    private TextView profileHeaderRank;

    private ImageView profileAvatar;

    private ImageView headerBackground;

    private FrameLayout headerFollowLayout;

    private ImageView headerFollow;

    private ImageView headerUnfollow;

    private DonutProgress profileHeaderRankProgress;

    private AvatarEnum selectedAvatar;

    private Dialog avatarSelect;

    private ImageView first, second, third;


    private ImageLoader imageLoader;


    /**
     * Array of the avatar imageViews' id
     */
    private final int selectAvatarIds[] = {R.id.avatar1, R.id.avatar2, R.id.avatar3, R.id.avatar4,
            R.id.avatar5, R.id.avatar6, R.id.avatar7, R.id.avatar8};

    private String userId;


    public static ProfileHeaderFragment newInstance() {
        ProfileHeaderFragment fragment = new ProfileHeaderFragment();
        Bundle b = new Bundle();
        b.putString("userId", FirebaseAuth.getInstance().getCurrentUser().getUid());
        fragment.setArguments(b);
        return fragment;
    }


    public static ProfileHeaderFragment newInstance(String uid) {
        ProfileHeaderFragment fragment = new ProfileHeaderFragment();
        Bundle b = new Bundle();
        b.putString("userId", uid);
        fragment.setArguments(b);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        Bundle b = this.getArguments();
        if (b.getString("userId") != null) {
            userId = b.getString("userId");
            profileHeaderPresenter = new ProfileHeaderPresenter(this);
        }

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_profile_header, container, false);

        //initialize ui references
        initUiBinding(rootView);

        //Can only edit if viewing own profile
        if (!userId.equals(FirebaseAuth.getInstance().getCurrentUser().getUid())) {
            profileHeaderPresenter.initFollowing(FirebaseAuth.getInstance().getCurrentUser().getUid(), userId);
        }else{
            initEditAvatar();
            initEditDisplayName();
        }

        profileHeaderPresenter.loadProfileData(userId);
        return rootView;
    }


    /**
     * Finds the views by id
     *
     * @param view The inflated layout
     */
    private void initUiBinding(View view) {
        profileHeaderLoadingOverlay = (FrameLayout) view.findViewById(R.id.profile_header_loading);
        profileHeaderContent = (FrameLayout) view.findViewById(R.id.profile_header_content);
        profileHeaderDisplayName = (TextView) view.findViewById(R.id.tv_profile_name);
        profileHeaderCurrentLocation = (TextView) view.findViewById(R.id.tv_profile_location);
        profileHeaderRank = (TextView) view.findViewById(R.id.tv_profile_rank);
        profileAvatar = (ImageView) view.findViewById(R.id.img_profile_avatar);
        profileHeaderRankProgress = (DonutProgress) view.findViewById(R.id.profile_rank_progress);
        headerBackground = (ImageView) view.findViewById(R.id.header_bg);
        headerFollowLayout = (FrameLayout) view.findViewById(R.id.header_follow_layout);
        headerFollow = (ImageView) view.findViewById(R.id.header_follow);
        headerUnfollow = (ImageView) view.findViewById(R.id.header_unfollow);
        first = (ImageView) view.findViewById(R.id.header_first);
        second = (ImageView) view.findViewById(R.id.header_second);
        third = (ImageView) view.findViewById(R.id.header_third);

        profileAvatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAvatarSelectDialog();
            }
        });

        Dali.create(view.getContext()).load(R.drawable.profile_header_background2)
                .blurRadius(25).downScale(2)
                .concurrent()
                .reScale()
                .into(headerBackground);
    }

    public void initEditAvatar() {
        selectedAvatar = AvatarEnum.Avatar1;

        imageLoader = ImageLoader.getInstance();
        loadSelectedImage();
        initializeDialog();

    }

    public void initEditDisplayName() {
        profileHeaderDisplayName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // get prompts.xml view
                LayoutInflater li = LayoutInflater.from(getContext());
                View promptsView = li.inflate(R.layout.prompt_edit_profile_name, null);

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        getContext());

                // set prompts.xml to alertdialog builder
                alertDialogBuilder.setView(promptsView);

                final EditText userInput = (EditText) promptsView
                        .findViewById(R.id.editTextDialogUserInput);

                userInput.setText(profileHeaderDisplayName.getText().toString());
                // set dialog message
                alertDialogBuilder
                        .setPositiveButton("Save",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {

                                    }
                                })
                        .setNegativeButton("Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {

                                    }
                                });

                // create alert dialog
                final AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();

                //Overriding the handler immediately after show is probably a better approach than OnShowListener as described below
                alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Boolean wantToCloseDialog = false;
                        if (userInput.getText().toString().isEmpty()) {
                            userInput.setError("Item must not be empty");
                        } else {
                            profileHeaderDisplayName.setText(userInput.getText().toString());
                            profileHeaderPresenter.updateDisplayName(userId, userInput.getText().toString());
                            wantToCloseDialog = true;
                        }
                        //Do stuff, possibly set wantToCloseDialog to true then...
                        if (wantToCloseDialog)
                            alertDialog.dismiss();
                        //else dialog stays open. Make sure you have an obvious way to close the dialog especially if you set cancellable to false.
                    }
                });

                alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Boolean wantToCloseDialog = true;
                        //Do stuff, possibly set wantToCloseDialog to true then...
                        if (wantToCloseDialog)
                            alertDialog.dismiss();
                        //else dialog stays open. Make sure you have an obvious way to close the dialog especially if you set cancellable to false.
                    }
                });

            }
        });
    }

    /**
     * Initializes the selectAvatar dialog
     */
    private void initializeDialog() {
        avatarSelect = new Dialog(getContext(), android.R.style.Theme_Material_Dialog);
        avatarSelect.setContentView(R.layout.dialog_avatar_select);
        avatarSelect.setCanceledOnTouchOutside(true);
        avatarSelect.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        for (int i = 0; i < selectAvatarIds.length; i++) {
            imageLoader.loadImageFromUrl(getContext(), AvatarEnum.getAvatar(i + 1).url, (ImageView) avatarSelect.findViewById(selectAvatarIds[i]));
            avatarSelect.findViewById(selectAvatarIds[i]).setOnClickListener(new AvatarSelectListener());
        }

    }

    /**
     * Class that handles the click event of the avatars in selectAvatar dialog
     */
    class AvatarSelectListener implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            AvatarEnum avatarModel;
            Log.d("debug", "Clicked " + view.getId());
            switch (view.getId()) {
                case R.id.avatar1:
                    avatarModel = AvatarEnum.Avatar1;
                    break;
                case R.id.avatar2:
                    avatarModel = AvatarEnum.Avatar2;
                    break;
                case R.id.avatar3:
                    avatarModel = AvatarEnum.Avatar3;
                    break;
                case R.id.avatar4:
                    avatarModel = AvatarEnum.Avatar4;
                    break;
                case R.id.avatar5:
                    avatarModel = AvatarEnum.Avatar5;
                    break;
                case R.id.avatar6:
                    avatarModel = AvatarEnum.Avatar6;
                    break;
                case R.id.avatar7:
                    avatarModel = AvatarEnum.Avatar7;
                    break;
                case R.id.avatar8:
                    avatarModel = AvatarEnum.Avatar8;
                    break;
                default:
                    avatarModel = AvatarEnum.Avatar1;
            }

            changeSelectedAvatar(avatarModel);
        }
    }

    /**
     * Change the selected avatar in setup
     *
     * @param avatar
     */
    private void changeSelectedAvatar(AvatarEnum avatar) {
        selectedAvatar = avatar;
        loadAvatar(selectedAvatar);
        profileHeaderPresenter.updateAvatar(userId, avatar);
        hideAvatarSelectDialog();

    }

    /**
     * Shows the avatar select dialog
     */
    private void showAvatarSelectDialog() {
        if(avatarSelect != null)
            avatarSelect.show();
    }

    /**
     * Closes the avatar select dialog
     */
    public void hideAvatarSelectDialog() {
        avatarSelect.hide();
    }

    /**
     * Loads the selected image in the dialog
     */
    private void loadSelectedImage() {
        imageLoader.loadImageFromUrl(getContext(), selectedAvatar.url, profileAvatar);
    }


    private void loadAvatar(AvatarEnum avatar) {
        ImageLoader loader = ImageLoader.getInstance();
        loader.loadImageFromUrl(this.getContext(), avatar.url, profileAvatar);
    }

    @Override
    public void showLoadingOverlay() {
        Log.d("Debug", "[PROFILE_HEADER] Showing Loading Overlay");
        profileHeaderContent.setVisibility(View.INVISIBLE);
        profileHeaderLoadingOverlay.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoadingOverlay() {
        Log.d("Debug", "[PROFILE_HEADER] Hiding Loading Overlay");
        profileHeaderContent.setVisibility(View.VISIBLE);
        profileHeaderLoadingOverlay.setVisibility(View.INVISIBLE);
    }

    @Override
    public void updateProfileHeaderContent(String displayName, String currentLocation, RankEnum rank,
                                           RankProgressModel progress, AvatarEnum avatar) {
        profileHeaderDisplayName.setText(displayName);
        profileHeaderCurrentLocation.setText(currentLocation);
        Log.d("Debug", "[PROFILE_HEADER] Current location: " + currentLocation);
        profileHeaderRank.setText(rank.name);
        updateRankProgess(progress);
        loadAvatar(avatar);
        selectedAvatar = avatar;
        updateToolbarTitle(displayName);

    }

    @Override
    public void updateAchievements(Integer first, Integer second, Integer third) {
        if(first!= null){
            this.first.setImageResource(first);
        }
        if(second != null){
            this.second.setImageResource(second);
        }
        if (third != null) {
            this.third.setImageResource(third);
        }
    }

    @Override
    public void initFollow(boolean isFollowing) {
        headerFollowLayout.setVisibility(View.VISIBLE);
        final String currentUser = FirebaseAuth.getInstance().getCurrentUser().getUid();
        headerFollow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                profileHeaderPresenter.followUser(currentUser, userId);
                headerFollow.setVisibility(View.GONE);
                headerUnfollow.setVisibility(View.VISIBLE);
            }
        });
        headerUnfollow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                profileHeaderPresenter.unfollowUser(currentUser, userId);
                headerFollow.setVisibility(View.VISIBLE);
                headerUnfollow.setVisibility(View.GONE);
            }
        });

        //view toggle stuff
        if(isFollowing){
            headerUnfollow.setVisibility(View.VISIBLE);
        }else {
            headerFollow.setVisibility(View.VISIBLE);
        }
    }

    /***
     * Updates the threshold and the progress of the donut progressbar
     * @param progressModel Current progress of the user
     */
    private void updateRankProgess(RankProgressModel progressModel) {
        profileHeaderRankProgress.setMax((int) (progressModel.ceil - progressModel.floor));
        profileHeaderRankProgress.setProgress((float) (progressModel.progress - progressModel.floor));
    }

    public void updateToolbarTitle(String title) {

        ProfileFragment parentFrag = ((ProfileFragment) this.getParentFragment());
        if (parentFrag != null)
            parentFrag.updateCollapsingToolbarTitle(title);
        else {
            ((ProfilePreviewActivity) getActivity()).updateCollapsingToolbarTitle(title);
        }

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }


}
