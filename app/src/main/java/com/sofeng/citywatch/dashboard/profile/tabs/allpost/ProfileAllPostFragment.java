package com.sofeng.citywatch.dashboard.profile.tabs.allpost;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.auth.FirebaseAuth;
import com.sofeng.citywatch.R;
import com.sofeng.citywatch.dashboard.feed.BaseFeedView;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileAllPostFragment
        extends BaseFeedView
        implements IProfileAllPostView {

    private IProfileAllPostPresenter profileAchievementPresenter;
    private String userId;

    public ProfileAllPostFragment() {
        // Required empty public constructor
    }

    public static ProfileAllPostFragment newInstance() {
        ProfileAllPostFragment fragment = new ProfileAllPostFragment();
        Bundle b = new Bundle();
        fragment.setArguments(b);
        return fragment;
    }

    public static ProfileAllPostFragment newInstance(String userId) {
        ProfileAllPostFragment fragment = new ProfileAllPostFragment();
        Bundle b = new Bundle();
        b.putString("userId", userId);
        fragment.setArguments(b);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        Bundle b = this.getArguments();
        if (b.getString("userId") != null) {
            userId = b.getString("userId");
        } else {
            userId = FirebaseAuth.getInstance().getCurrentUser().getUid();
        }
        profileAchievementPresenter = new ProfileAllPostPresenter(this);
    }


    @Override
    protected void init() {
        refreshFeed();
    }

    @Override
    protected void refreshFeed() {
        profileAchievementPresenter.updateFeed(userId);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            refreshFeed();
        }
    }
}
