package com.sofeng.citywatch.dashboard.map;

import android.content.Context;

import com.google.maps.android.heatmaps.WeightedLatLng;
import com.sofeng.citywatch.model.MapPostModel;

import java.util.List;

/**
 * Created by JKMT on 08/05/2017.
 */

public interface IMapView {

    Context getContext();

    /**
     * Adds heatmap overlay in map
     * @param points Points of interests in map
     */
    void addHeatMap(List<WeightedLatLng> points);

    /**
     * Adds the posts to the cluster manager
     * @param mapPostModelList List of posts
     */
    void addPostToMap(List<MapPostModel> mapPostModelList);
}
