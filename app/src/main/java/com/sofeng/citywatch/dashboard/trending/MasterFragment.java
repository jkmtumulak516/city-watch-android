package com.sofeng.citywatch.dashboard.trending;


import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sofeng.citywatch.R;
import com.sofeng.citywatch.dashboard.PagerAdapter;
import com.sofeng.citywatch.dashboard.trending.tabs.TrendingFragment;
import com.sofeng.citywatch.model.TagEnum;

import java.util.Arrays;


/**
 * A simple {@link Fragment} subclass.
 */
public class MasterFragment extends Fragment {


    private TabLayout tabLayout;

    private ViewPager viewPager;

    public static MasterFragment newInstance() {
        MasterFragment fragment = new MasterFragment();
        return fragment;
    }

    private void addTabs(ViewPager viewPager) {

        PagerAdapter adapter = new PagerAdapter(getActivity().getSupportFragmentManager());

        adapter.addFrag(TrendingFragment.newInstance(null), "All");
        // adds remaining tabs for the all the parent tags
        for (TagEnum tag : Arrays.copyOfRange(TagEnum.values(), 0, 6)) {
            adapter.addFrag(TrendingFragment.newInstance(tag), tag.string);
        }

        viewPager.setAdapter(adapter);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        // create our manager instance after the content explore is set
        View view = inflater.inflate(R.layout.fragment_trending, container, false);
        viewPager = (ViewPager) view.findViewById(R.id.vp_trending);
        addTabs(viewPager);

        tabLayout = (TabLayout) view.findViewById(R.id.tabs_trending);
        tabLayout.setupWithViewPager(viewPager);

        return view;
    }

}

