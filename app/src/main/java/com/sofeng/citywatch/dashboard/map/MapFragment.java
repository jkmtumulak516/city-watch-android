package com.sofeng.citywatch.dashboard.map;


import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.util.ArraySet;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.TileOverlay;
import com.google.android.gms.maps.model.TileOverlayOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.heatmaps.Gradient;
import com.google.maps.android.heatmaps.HeatmapTileProvider;
import com.google.maps.android.heatmaps.WeightedLatLng;
import com.sofeng.citywatch.R;
import com.sofeng.citywatch.dashboard.IDashboardView;
import com.sofeng.citywatch.model.MapPostModel;
import com.sofeng.citywatch.model.TagEnum;
import com.sofeng.citywatch.post.explore.ExploreActivity;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

import lib.kingja.switchbutton.SwitchMultiButton;


/**
 * A simple {@link Fragment} subclass.
 */
public class MapFragment
        extends Fragment
        implements IMapView, OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    private boolean followingFlag;
    private Set<TagEnum> tags;
    private TagEnum selectedInsightTag;
    private int minRank;
    private int maxRank;
    private boolean initiliazedFilters = false;

    private SwitchMultiButton mSwitchMultiButton;
    private ClusterManager<MapPostModel> mClusterManager;
    private GoogleMap mMap;
    SupportMapFragment mMapView;
    private GoogleApiClient googleApiClient;

    private static View view;
    private String cityId;
    private String userId;
    private HeatmapTileProvider mProvider;
    private IMapPresenter mapPresenter;
    private TileOverlay mOverlay;

    private CardView insightSelectedCard;
    private TextView insightSelectedText;

    private boolean isPostMode = true;

    public static MapFragment newInstance() {
        MapFragment fragment = new MapFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        mapPresenter = new MapPresenter(this);
        if (!initiliazedFilters) {
            followingFlag = false;
            tags = new ArraySet<>();
            minRank = -1;
            maxRank = 6;
            initiliazedFilters = true;
        }
        selectedInsightTag = TagEnum.values()[0];

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        IDashboardView parentActivity = (IDashboardView) getActivity();
        cityId = parentActivity.getCityId();
        userId = FirebaseAuth.getInstance().getCurrentUser().getUid();

        // Inflate the layout for this fragment
        try {
            view = inflater.inflate(R.layout.fragment_map, container, false);
            mSwitchMultiButton = (SwitchMultiButton) view.findViewById(R.id.switchMultiButton);
            mSwitchMultiButton.setText("Posts", "Insights").setOnSwitchListener(new SwitchMultiButton.OnSwitchListener() {
                @Override
                public void onSwitch(int position, String tabText) {
                    clearMap();
                    switch (position) {
                        case 0:
                            // display post map function
                            hideSelectedInsightTag();
                            startMap();
                            refreshMapPost();
                            isPostMode = true;
                            break;
                        case 1:
                            // display insight map function
                            showSelectedInsightTag();
                            setSelectedInsightTag(selectedInsightTag);
                            mClusterManager.clearItems();
                            mapPresenter.getInsights(cityId, selectedInsightTag);
                            isPostMode = false;
                            break;
                    }
                }
            });
            setUpMap(savedInstanceState);
            googleApiClient = new GoogleApiClient.Builder(getContext())
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();

            insightSelectedCard = (CardView) view.findViewById(R.id.insight_selected_card);
            insightSelectedText = (TextView) view.findViewById(R.id.insight_selected_tag);
        } catch (InflateException e) {

        }

        return view;
    }

    private void setSelectedInsightTag(TagEnum tag) {
        insightSelectedText.setText(tag.string);
    }

    private void hideSelectedInsightTag() {
        insightSelectedCard.setVisibility(View.INVISIBLE);
    }

    private void showSelectedInsightTag() {
        insightSelectedCard.setVisibility(View.VISIBLE);
    }

    public void clearMap() {
        getMap().clear();
        if (mOverlay != null) {
            mOverlay.clearTileCache();
        }
    }

    public boolean isPostMode() {
        return isPostMode;
    }

    public void updateFilters(boolean followingFlag, Set<TagEnum> tags, int minRank, int maxRank) {
        initiliazedFilters = true;
        this.followingFlag = followingFlag;
        this.tags = tags;
        this.minRank = minRank;
        this.maxRank = maxRank;
    }

    public void updateInsightFilter(TagEnum tagEnum) {
        initiliazedFilters = true;
        selectedInsightTag = tagEnum;
        setSelectedInsightTag(selectedInsightTag);
        mapPresenter.getInsights(cityId, selectedInsightTag);
    }

    public void refreshMapPost() {
        mapPresenter.getMapPosts(cityId, userId, followingFlag, tags, minRank, maxRank);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        if (mMap != null) {
            Log.d("Debug", "[MAP] onMapReady map not null");
        }
        mMap = googleMap;
        styleMap();
        startMap();
        refreshMapPost();
    }

    /**
     * Styles the map using a raw file
     */
    private void styleMap() {
        try {
            // Customise the styling of the base map using a JSON object defined
            // in a raw resource file.
            boolean success = mMap.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                            getContext(), R.raw.style_map));

            if (!success) {
                Log.d("Debug", "[MAP] onMapReady Style parsing failed.");
            } else {
                Log.d("Debug", "[MAP] onMapReady Style parsing success.");
            }
        } catch (Resources.NotFoundException e) {
            Log.d("Debug", "[MAP] onMapReady Can't find style. Error: ", e);
        }
    }


    public void moveMapCamera(LatLng latlng) {
        getMap().moveCamera(CameraUpdateFactory.newLatLng(latlng));
        getMap().animateCamera(CameraUpdateFactory.zoomTo(15));
    }

    private GoogleMap getMap() {
        return mMap;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    private void setUpMap(Bundle savedInstanceState) {
        mMapView = ((SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map));
        mMapView.onCreate(savedInstanceState);
        mMapView.getMapAsync(this);
    }

    private void startMap() {
        mClusterManager = new ClusterManager<>(getContext(), getMap());
        mClusterManager.setRenderer(new MapPostRenderer(getContext(), getMap(), mClusterManager));
        mClusterManager.setOnClusterItemInfoWindowClickListener(new ClusterManager.OnClusterItemInfoWindowClickListener<MapPostModel>() {
            @Override
            public void onClusterItemInfoWindowClick(MapPostModel mapPostModel) {
                Intent i = new Intent(MapFragment.this.getContext(), ExploreActivity.class);
                i.putExtra("cityId", cityId);
                i.putExtra("postId", mapPostModel.postId);
                MapFragment.this.getContext().startActivity(i);
            }
        });

        getMap().setOnCameraIdleListener(mClusterManager);
        getMap().setOnInfoWindowClickListener(mClusterManager);
        moveCameraCurrentCityLocation();
    }

    private void moveCameraCurrentCityLocation() {
        LatLng cityLoc = new LatLng(10.31111, 123.89167);
        moveMapCamera(cityLoc);

    }

    @Override
    public void onStart() {
        googleApiClient.connect();
        super.onStart();
    }

    @Override
    public void onStop() {
        googleApiClient.disconnect();
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void addHeatMap(List<WeightedLatLng> points) {
        // Create a heat map tile provider, passing it the latlngs of the posts.
        if (mProvider != null) {
            if (points != null) {
                mProvider.setWeightedData(points);
                mOverlay.clearTileCache();
            } else {
                mOverlay.remove();
                mProvider = null;
            }

        } else {
            if(points != null) {
                mProvider = new HeatmapTileProvider.Builder()
                        .weightedData(points)
                        .radius(50)
                        .build();
                // Add a tile overlay to the map, using the heat map tile provider.
                mOverlay = mMap.addTileOverlay(new TileOverlayOptions().tileProvider(mProvider));
            }
        }
    }

    @Override
    public void addPostToMap(List<MapPostModel> mapPostModelList) {
        if (mClusterManager != null) {
            mClusterManager.clearItems();
            mClusterManager.addItems(mapPostModelList);
        }
    }
}
