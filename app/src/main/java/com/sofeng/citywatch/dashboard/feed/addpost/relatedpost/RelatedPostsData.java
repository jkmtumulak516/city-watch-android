package com.sofeng.citywatch.dashboard.feed.addpost.relatedpost;

import android.support.annotation.NonNull;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Transaction;
import com.sofeng.citywatch.net.ResponseListener;

import java.util.List;

/**
 * Created by JKMT on 09/22/2017.
 */

public class RelatedPostsData implements IRelatedPostsData {

    private DatabaseReference rootReference;

    public RelatedPostsData() {

        rootReference = FirebaseDatabase.getInstance().getReference();
    }

    @Override
    public void setRelatedPosts(String cityId, @NonNull String postId1, @NonNull String postId2, ResponseListener<Integer> listener) {

        DatabaseReference relatedReference = rootReference.child("posts").child(cityId).child("statistics").child("related");

        relatedReference.runTransaction(new SetRelatedPosts(postId1, postId2, listener));
    }

    private class SetRelatedPosts implements Transaction.Handler {

        private String postId1;
        private String postId2;
        private ResponseListener<Integer> listener;

        public SetRelatedPosts(String postId1, String postId2, ResponseListener<Integer> listener) {

            this.postId1 = postId1;
            this.postId2 = postId2;
            this.listener = listener;
        }

        @Override
        public Transaction.Result doTransaction(MutableData mutableData) {

            mutableData.child(postId1).child(postId2).setValue(true);
            mutableData.child(postId2).child(postId1).setValue(true);

            return Transaction.success(mutableData);
        }

        @Override
        public void onComplete(DatabaseError databaseError, boolean b, DataSnapshot dataSnapshot) {

            if (b) {
                listener.onSuccess(0);
            } else {
                listener.onError();
            }
        }
    }
}
