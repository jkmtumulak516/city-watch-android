package com.sofeng.citywatch.dashboard.feed;

import com.sofeng.citywatch.model.TagEnum;
import com.sofeng.citywatch.model.filter.PostFilter;

import java.util.Set;

/**
 * Created by JKMT on 08/05/2017.
 */

public interface IFeedPresenter {

    /**
     * Requests to update feed
     * @param cityId Id of the current City
     * @param userId Id of the current User
     * @param followingFlag Flag to show only following or not
     * @param tags Tags to filter the posts
     * @param minRank Minimum rank filter
     * @param maxRank Maximum rank filter
     */
    void updateFeed(String cityId, String userId, boolean followingFlag, Set<TagEnum> tags, int minRank, int maxRank);
}
