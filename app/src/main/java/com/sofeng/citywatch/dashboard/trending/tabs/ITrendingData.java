package com.sofeng.citywatch.dashboard.trending.tabs;

import android.support.annotation.NonNull;

import com.sofeng.citywatch.model.PostModel;
import com.sofeng.citywatch.model.TagEnum;
import com.sofeng.citywatch.net.ResponseListener;

import java.util.List;

/**
 * Created by JKMT on 09/26/2017.
 */

public interface ITrendingData {

    void getPosts(String cityId, TagEnum tag, @NonNull final ResponseListener<List<PostModel>> listener);

    void getPosts(String cityId, String tag, @NonNull final ResponseListener<List<PostModel>> listener);
}
