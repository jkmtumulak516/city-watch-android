package com.sofeng.citywatch.dashboard.profile.tabs.citypost;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.sofeng.citywatch.model.PostModel;
import com.sofeng.citywatch.model.filter.UserPostFilter;
import com.sofeng.citywatch.net.Endpoints;
import com.sofeng.citywatch.net.MultipleObjectsRequest;
import com.sofeng.citywatch.net.ResponseListener;

import java.util.List;
import java.util.Map;

/**
 * Created by JKMT on 08/05/2017.
 */

public class ProfileCityPostData implements IProfileCityPostData {

    private static final String requestTag = "ALL_USER_POSTS_BY_CITY";

    private RequestQueue requestQueue;

    public ProfileCityPostData(Context context) {

        requestQueue = Volley.newRequestQueue(context);
    }

    @Override
    public void getPosts(String userId, String cityId, final ResponseListener<List<PostModel>> listener) {

        String baseUrl = Endpoints.UserPosts.ALL_USER_POSTS_BY_CITY;
        String url = baseUrl + "?userId=" + userId + "&cityId=" + cityId; // combines the base url with form parameters to create the url

        UserPostFilter userPostFilter = new UserPostFilter(userId)
                .setByCity(true)
                .setCityId(cityId);

        MultipleObjectsRequest<UserPostFilter> postsRequest = new MultipleObjectsRequest<>(Request.Method.GET, url, userPostFilter, new Response.Listener<Map>() {
            @Override
            public void onResponse(Map response) {

                List<PostModel> posts = PostModel.convertMultiple((List<Map>) response.get("userPosts"));

                listener.onSuccess(posts);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                listener.onError();
            }
        });

        postsRequest.setTag(requestTag); // sets the requests tag to this objects tag

        requestQueue.add(postsRequest); // queues the request
    }

}
