package com.sofeng.citywatch.dashboard.feed;


import android.nfc.Tag;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.util.ArraySet;

import com.google.firebase.auth.FirebaseAuth;
import com.sofeng.citywatch.dashboard.IDashboardView;
import com.sofeng.citywatch.model.TagEnum;

import java.util.Set;


/**
 * A simple {@link Fragment} subclass.
 */
public class FeedFragment
        extends BaseFeedView
        implements IFeedView {

    private IFeedPresenter feedPresenter;

    private String cityId;
    private String userId;
    private boolean followingFlag;
    private Set<TagEnum> tags;
    private int minRank;
    private int maxRank;
    private boolean initiliazedFilters = false;

    public static FeedFragment newInstance() {
        FeedFragment fragment = new FeedFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        feedPresenter = new FeedPresenter(this);
        if(!initiliazedFilters) {
            followingFlag = false;
            tags = new ArraySet<>();
            minRank = -1;
            maxRank = 6;
            initiliazedFilters = true;
        }
    }

    @Override
    protected void init() {
        IDashboardView parentActivity = (IDashboardView) getActivity();
        cityId = parentActivity.getCityId();
        userId = FirebaseAuth.getInstance().getCurrentUser().getUid();

        refreshFeed();
    }

    @Override
    protected void refreshFeed(){
        IDashboardView parentActivity = (IDashboardView) getActivity();
        cityId = parentActivity.getCityId();
        feedPresenter.updateFeed(cityId, userId, followingFlag, tags, minRank, maxRank);
    }

//    @Override
//    public void setUserVisibleHint(boolean isVisibleToUser){
//        super.setUserVisibleHint(isVisibleToUser);
//        if(isVisibleToUser){
//            refreshFeed();
//        }
//    }

    public void updateFilters(boolean followingFlag, Set<TagEnum> tags, int minRank, int maxRank){
        this.followingFlag = followingFlag;
        this.tags = tags;
        this.minRank = minRank;
        this.maxRank = maxRank;
        refreshFeed();
    }
}
