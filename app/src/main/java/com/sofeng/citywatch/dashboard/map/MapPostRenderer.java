package com.sofeng.citywatch.dashboard.map;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Marker;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;
import com.sofeng.citywatch.R;
import com.sofeng.citywatch.model.MapPostModel;


/**
 * Created by JKMT on 03/06/2017.
 */

public class MapPostRenderer extends DefaultClusterRenderer<MapPostModel> {
    Context context;
    static Bitmap b = null;
    public MapPostRenderer(Context context, GoogleMap map, ClusterManager<MapPostModel> clusterManager) {
        super(context, map, clusterManager);
        this.context = context;
        if(b == null){
            b = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(context.getResources(), R.drawable.blip), 100, 100, false);
        }

    }

    @Override
    protected void onClusterItemRendered(MapPostModel clusterItem, Marker marker) {
        super.onClusterItemRendered(clusterItem, marker);
        try {
            marker.setIcon(BitmapDescriptorFactory.fromBitmap(b));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setOnClusterItemInfoWindowClickListener(ClusterManager.OnClusterItemInfoWindowClickListener<MapPostModel> listener) {
        super.setOnClusterItemInfoWindowClickListener(listener);
    }
}
