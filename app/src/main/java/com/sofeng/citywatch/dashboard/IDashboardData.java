package com.sofeng.citywatch.dashboard;

import com.sofeng.citywatch.model.CityModel;
import com.sofeng.citywatch.model.UserModel;
import com.sofeng.citywatch.net.ResponseListener;

/**
 * Created by JKMT on 09/02/2017.
 */

public interface IDashboardData {

    /**
     * Gets the user's current city and city name and executes the listener's onSuccess method.
     * @param userId The User's Id.
     * @param listener The Listener performed on success, cancel, or error.
     */
    void getCurrentUser(String userId, final ResponseListener<UserModel> listener);

    /**
     * Sets the user's current city.
     * @param userId The User's Id.
     * @param cityId The City's Id.
     */
    void setCurrentCity(String userId, String cityId);

    void onDestroy();
}
