package com.sofeng.citywatch.dashboard.feed.addpost.relatedpost;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.daprlabs.cardstack.SwipeDeck;
import com.sofeng.citywatch.R;
import com.sofeng.citywatch.dashboard.DashboardActivity;
import com.sofeng.citywatch.model.PostModel;

import java.util.List;

public class RelatedPostsActivity
        extends AppCompatActivity
        implements IRelatedPostsView {

    private IRelatedPostsPresenter relatedPostsPresenter;
    private String currentPostId;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_related_posts);

        relatedPostsPresenter = new RelatedPostsPresenter(this);
        List<PostModel> potentialPosts = (List<PostModel>) getIntent()
                .getExtras().getSerializable("potentialPosts");
        currentPostId = getIntent().getExtras().getString("postId");

        setupDeck(potentialPosts);
        showGuideDialog();
    }

    private void setupDeck(final List<PostModel> postModels){
        RelatedPostsDeckAdapter adapter = new RelatedPostsDeckAdapter(postModels, this);
        SwipeDeck cardStack = (SwipeDeck) findViewById(R.id.swipe_deck);
        cardStack.setAdapter(adapter);

        cardStack.setEventCallback(new SwipeDeck.SwipeEventCallback() {
            @Override
            public void cardSwipedLeft(int position) {
                Log.i("MainActivity", "card was swiped left, position in adapter: " + position);
            }

            @Override
            public void cardSwipedRight(int position) {
                relatedPostsPresenter.setRelated(currentPostId, postModels.get(position));
            }

            @Override
            public void cardsDepleted() {
                Log.i("MainActivity", "no more cards");
                showEndDialog();
            }

            @Override
            public void cardActionDown() {

            }

            @Override
            public void cardActionUp() {

            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.related_posts_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.menu_skip){
            backToDashboard();
        }
        return super.onOptionsItemSelected(item);
    }

    public void backToDashboard(){
        Intent i = new Intent(this, DashboardActivity.class);
        startActivity(i);
        finish();
    }

    public void showGuideDialog(){
        AlertDialog alertDialog = new AlertDialog.Builder(RelatedPostsActivity.this).create();
        alertDialog.setTitle("Related Posts");
        alertDialog.setMessage("SWIPE RIGHT if the post are related to yours, otherwise SWIPE LEFT.");
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    public void showEndDialog(){
        AlertDialog alertDialog = new AlertDialog.Builder(RelatedPostsActivity.this).create();
        alertDialog.setTitle("No More Related Posts");
        alertDialog.setMessage("There are no more potentially related posts");
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Go back to Dashboard",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        backToDashboard();
                    }
                });
        alertDialog.show();
    }
}
