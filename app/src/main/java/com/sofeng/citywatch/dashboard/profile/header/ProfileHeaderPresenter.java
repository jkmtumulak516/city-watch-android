package com.sofeng.citywatch.dashboard.profile.header;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.sofeng.citywatch.model.AchievementEnum;
import com.sofeng.citywatch.model.AvatarEnum;
import com.sofeng.citywatch.model.RankEnum;
import com.sofeng.citywatch.model.UserModel;
import com.sofeng.citywatch.net.ResponseListener;

/**
 * Created by JKMT on 08/05/2017.
 */

public class ProfileHeaderPresenter implements IProfileHeaderPresenter {

    private IProfileHeaderView profileHeaderView;
    private IProfileHeaderData profileHeaderData;

    public ProfileHeaderPresenter(IProfileHeaderView profileHeaderView) {

        this.profileHeaderView = profileHeaderView;

        profileHeaderData = new ProfileHeaderData();

    }

    @Override
    public void loadProfileData(String uid) {
        profileHeaderData.getUser(uid, new SetUserInformation());
    }

    @Override
    public void updateAvatar(String uid, AvatarEnum avatarEnum) {
        profileHeaderData.editAvatar(uid, avatarEnum.id);
    }

    @Override
    public void updateDisplayName(String uid, String displayName) {
        profileHeaderData.editDisplayName(uid, displayName);
    }

    @Override
    public void updateProfileData(String uid, String name, AvatarEnum avatar) {
        // TODO presenter impl update profile
    }

    @Override
    public void followUser(String followingId, String followedById) {
        profileHeaderData.follow(followingId, followedById);
    }

    @Override
    public void unfollowUser(String followingId, String followedById) {
        profileHeaderData.unfollow(followingId, followedById);
    }

    @Override
    public void initFollowing(String followingId, String followedById) {
        profileHeaderData.isFollowing(followingId, followedById, new IsFollowingListener());
    }

    private void updateProfileHeaderContent(UserModel user) {
        profileHeaderView.showLoadingOverlay();
        profileHeaderView.updateProfileHeaderContent(user.displayName, user.currentCity.name,
                RankEnum.getRank(user.rank), user.rankProgress, AvatarEnum.getAvatar(user.avatar));

        if (user.achievements != null) {
            long firstVal = user.achievements.containsKey("first") ? user.achievements.get("first") : 0;
            long secondVal = user.achievements.containsKey("second") ? user.achievements.get("second") : 0;
            long thirdVal = user.achievements.containsKey("third") ? user.achievements.get("third") : 0;
            profileHeaderView.updateAchievements(
                    AchievementEnum.getAchievement("first").getIcon((int) firstVal),
                    AchievementEnum.getAchievement("second").getIcon((int) secondVal),
                    AchievementEnum.getAchievement("third").getIcon((int) thirdVal));
        }

        profileHeaderView.hideLoadingOverlay();
    }

    private class SetUserInformation implements ResponseListener<UserModel> {

        @Override
        public void onSuccess(UserModel user) {

            if (user != null) {
                updateProfileHeaderContent(user);
            }
        }

        @Override
        public void onCancel() {

        }

        @Override
        public void onError() {

        }
    }

    private class IsFollowingListener implements ResponseListener<Boolean> {
        @Override
        public void onSuccess(Boolean object) {
            profileHeaderView.initFollow(object);
        }

        @Override
        public void onCancel() {

        }

        @Override
        public void onError() {

        }
    }
}
