package com.sofeng.citywatch.dashboard.feed.addpost;

import android.net.Uri;
import com.sofeng.citywatch.model.TagEnum;

import java.util.Set;

/**
 * Created by JKMT on 08/30/2017.
 */

public interface IAddPostPresenter {


    /**
     * Gets data from explore and encapsulates it to PostModel
     * then passes it to data for uploading
     * @param userId Id of the current user
     * @param cityId Id of the user's current city
     * @param title Title of the post
     * @param description Description of the post
     * @param mood Mood of the post
     * @param tags Tags of the post
     * @param locationName Address of the post
     * @param lat Latitude of the address of the post
     * @param lng Longitude of the address of the post
     * @param mediaType Media type of the extra data
     * @param mediaFile Uri of the media
     */
    void uploadPost(String userId, String cityId, String title, String description,
                    String mood, final Set<TagEnum> tags, String locationName, double lat,
                    double lng, double mediaType, Uri mediaFile);
}
