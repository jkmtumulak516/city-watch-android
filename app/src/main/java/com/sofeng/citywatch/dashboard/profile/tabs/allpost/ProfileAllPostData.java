package com.sofeng.citywatch.dashboard.profile.tabs.allpost;


import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.sofeng.citywatch.model.PostModel;
import com.sofeng.citywatch.model.filter.UserPostFilter;
import com.sofeng.citywatch.net.Endpoints;
import com.sofeng.citywatch.net.MultipleObjectsRequest;
import com.sofeng.citywatch.net.ResponseListener;

import java.util.List;
import java.util.Map;

/**
 * Created by JKMT on 08/05/2017.
 */

public class ProfileAllPostData implements IProfileAllPostData {

    private static final String requestTag = "ALL_USER_POSTS";

    private RequestQueue requestQueue;

    public ProfileAllPostData(Context context) {

        requestQueue = Volley.newRequestQueue(context);
    }

    @Override
    public void getPosts(String userId, final ResponseListener<List<PostModel>> listener) {

        String baseUrl = Endpoints.UserPosts.ALL_USER_POSTS;
        String url = baseUrl + "?userId=" + userId; // combines the base url with form parameters to create the url

        UserPostFilter userPostFilter = new UserPostFilter(userId)
                .setByCity(false);

        MultipleObjectsRequest<UserPostFilter> postsRequest = new MultipleObjectsRequest<>(Request.Method.POST, url, userPostFilter,new Response.Listener<Map>() {
            @Override
            public void onResponse(Map response) {

                List<PostModel> posts = PostModel.convertMultiple((List<Map>) response.get("userPosts"));

                listener.onSuccess(posts);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                listener.onError();
                Log.d("Debug","[PROFILE-ALL-DATA] "+error.getMessage());
            }
        });

        postsRequest.setTag(requestTag); // sets the requests tag to this objects tag

        requestQueue.add(postsRequest); // queues the request
    }

}
