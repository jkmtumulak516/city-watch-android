package com.sofeng.citywatch.dashboard.profile.header;

import android.support.annotation.NonNull;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.sofeng.citywatch.model.UserModel;
import com.sofeng.citywatch.net.ResponseListener;

/**
 * Created by JKMT on 08/05/2017.
 */

public class ProfileHeaderData implements IProfileHeaderData {

    private DatabaseReference rootReference;

    public ProfileHeaderData() {

        rootReference = FirebaseDatabase.getInstance().getReference();
    }

    @Override
    public void getUser(@NonNull String userId, final ResponseListener<UserModel> listener) {

        DatabaseReference userReference = rootReference.child("users").child(userId);

        userReference.addListenerForSingleValueEvent(new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                UserModel user = dataSnapshot.getValue(UserModel.class);

                listener.onSuccess(user);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

                listener.onCancel();
            }
        });
    }

    @Override
    public void getFollowerCount(@NonNull String userId, final ResponseListener<Double> listener) {

        DatabaseReference followedByReference = rootReference.child("followedByCount");

        followedByReference.child(userId).addListenerForSingleValueEvent(new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                double followerCount = dataSnapshot.getValue(Double.class);

                listener.onSuccess(followerCount);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

                listener.onCancel();
            }
        });
    }

    @Override
    public void editDisplayName(@NonNull String userId, @NonNull String displayName) {

        DatabaseReference userReference = rootReference.child("users").child(userId);

        userReference.child("displayName").setValue(displayName);
    }

    @Override
    public void editAvatar(@NonNull String userId, @NonNull int avatar) {

        DatabaseReference userReference = rootReference.child("users").child(userId);

        userReference.child("avatar").setValue(avatar);
    }

    @Override
    public void isFollowing(@NonNull String followingId, @NonNull String followedById, final ResponseListener<Boolean> listener) {

        DatabaseReference followingReference = rootReference.child("following").child(followingId).child(followedById);

        followingReference.addListenerForSingleValueEvent(new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                Boolean isFollowing = dataSnapshot.getValue(Boolean.class);

                isFollowing = isFollowing == null ? false : isFollowing;

                listener.onSuccess(isFollowing);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

                listener.onCancel();
            }
        });
    }

    @Override
    public void follow(@NonNull String followingId, @NonNull String followedById) {

        DatabaseReference followingReference = rootReference.child("following");
        DatabaseReference followedByReference = rootReference.child("followedBy");

        followingReference.child(followingId).child(followedById).setValue(true);
        followedByReference.child(followedById).child(followingId).setValue(true);
    }

    @Override
    public void unfollow(@NonNull String followingId, @NonNull String followedById) {

        DatabaseReference followingReference = rootReference.child("following");
        DatabaseReference followedByReference = rootReference.child("followedBy");

        followingReference.child(followingId).child(followedById).setValue(null);
        followedByReference.child(followedById).child(followingId).setValue(null);
    }
}
