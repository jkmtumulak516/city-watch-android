package com.sofeng.citywatch.dashboard.feed.addpost;

import android.content.Context;

import com.sofeng.citywatch.model.PostModel;

import java.util.List;

/**
 * Created by JKMT on 08/30/2017.
 */

public interface IAddPostView {

    /**
     * Displays the loading layout
     */
    public void displayPostProgress();

    /**
     * Sets the progress of loading layout
     */
    public void setPostProgress(int progress);

    /**
     * Hides the loading layout
     */
    public void hidePostProgress();

    /**
     * Finishes activity and goes back to dashboard
     */
    public void backToDashboard();

    /**
     * Finishes activity and goes to related posts.
     */
    public void gotoRelatedPosts(String postId, List<PostModel> relatedPosts);

    Context getContext();
}
