package com.sofeng.citywatch.dashboard.trending.tabs;

import com.sofeng.citywatch.model.PostModel;
import com.sofeng.citywatch.model.TagEnum;
import com.sofeng.citywatch.net.ResponseListener;
import com.sofeng.citywatch.util.FirebaseUtil;

import java.util.List;

/**
 * Created by JKMT on 09/26/2017.
 */

public class TrendingPresenter implements ITrendingPresenter {

    private ITrendingView trendingView;
    private ITrendingData trendingData;

    public TrendingPresenter(ITrendingView trendingView) {

        this.trendingView = trendingView;

        trendingData = new TrendingData(trendingView.getContext());
    }

    @Override
    public void updateFeed(String cityId, TagEnum tag) {
        trendingData.getPosts(cityId, tag, new TrendingListener());
    }

    private class TrendingListener implements ResponseListener<List<PostModel>> {

        @Override
        public void onSuccess(List<PostModel> object) {
            FirebaseUtil.syncUsers(trendingView.getPostList(), false);
            FirebaseUtil.syncUsers(object, true);
            trendingView.updateRecyclerView(object);
            trendingView.hideLoadingLayout();
        }

        @Override
        public void onCancel() {

        }

        @Override
        public void onError() {

        }
    }
}
