package com.sofeng.citywatch.dashboard.feed.addpost;

import android.net.Uri;
import android.util.Log;

import com.sofeng.citywatch.model.PostModel;
import com.sofeng.citywatch.model.TagEnum;
import com.sofeng.citywatch.net.ResponseListener;
import com.sofeng.citywatch.net.UploadListener;

import java.util.List;
import java.util.Set;

/**
 * Created by JKMT on 08/30/2017.
 */

public class AddPostPresenter implements IAddPostPresenter {

    private IAddPostView addPostView;
    private IAddPostData addPostData;
    private PostModel postModel;
    private String currentPostId;

    public AddPostPresenter(IAddPostView addPostView) {

        this.addPostView = addPostView;

        addPostData = new AddPostData(addPostView.getContext());
    }

    @Override
    public void uploadPost(String userId, String cityId, String title, String description, String mood, Set<TagEnum> tags, String locationName, double lat, double lng, double mediaType, Uri mediaFile) {
        postModel = new PostModel();
        postModel.userId = userId;
        postModel.cityId = cityId;
        postModel.title = title;
        postModel.description = description;
        postModel.mood = mood;
        postModel.locationName = locationName;
        postModel.lat = lat;
        postModel.lng = lng;
        postModel.mediaType = mediaType;

        addPostData.createPost(postModel, tags, mediaFile, new PostListener());
        // If there is a media to upload display loading screen
        addPostView.displayPostProgress();

    }


    class PostListener implements UploadListener {

        @Override
        public void onSuccess(String postId) {
            Log.d("Debug", "[POST-UPLOAD]: Upload success");
            currentPostId = postId;
            addPostData.getPotentiallyRelatedPosts(postModel.cityId, postId, new PotentialRelatedPostListener());

        }

        @Override
        public void onProgress(double currentProgress) {
            Log.d("Debug", "[POST-UPLOAD]: Upload progress = " + currentProgress);
            addPostView.setPostProgress(((int) (currentProgress * 100)));
        }

        @Override
        public void onPaused(double currentProgress) {
            Log.d("Debug", "[POST-UPLOAD]: Upload paused = " + currentProgress);
        }

        @Override
        public void onFailure(Exception exception) {
            Log.d("Debug", "[POST-UPLOAD]: Upload fail = " + exception.getMessage());
        }
    }

    class PotentialRelatedPostListener implements ResponseListener<List<PostModel>> {

        @Override
        public void onSuccess(List<PostModel> object) {
            Log.d("Debug", "[POST-Related]: On success");
            addPostView.hidePostProgress();
            if (object != null) {
                if (!object.isEmpty()) {
                    Log.d("Debug", "[POST-Related]: has related");
                    addPostView.gotoRelatedPosts(currentPostId, object);
                } else {
                    Log.d("Debug", "[POST-Related]: has no related");
                    addPostView.backToDashboard();
                }
            } else {
                Log.d("Debug", "[POST-Related]: has no related");
                addPostView.backToDashboard();
            }
        }

        @Override
        public void onCancel() {
            Log.d("Debug", "[POST-Related]: On cancel");
        }

        @Override
        public void onError() {
            Log.d("Debug", "[POST-Related]: On error");
        }
    }
}
