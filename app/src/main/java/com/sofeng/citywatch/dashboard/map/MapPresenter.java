package com.sofeng.citywatch.dashboard.map;

import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.maps.android.heatmaps.WeightedLatLng;
import com.sofeng.citywatch.model.MapPostModel;
import com.sofeng.citywatch.model.TagEnum;
import com.sofeng.citywatch.model.filter.PostFilter;
import com.sofeng.citywatch.net.ResponseListener;

import java.util.List;
import java.util.Set;

/**
 * Created by JKMT on 08/05/2017.
 */

public class MapPresenter implements IMapPresenter {

    private IMapView mapView;
    private IMapData mapData;

    public MapPresenter(IMapView mapView) {

        this.mapView = mapView;

        mapData = new MapData(mapView.getContext());
    }

    public void getMapPosts(String cityId, String userId, boolean followingFlag, Set<TagEnum> tags, int minRank, int maxRank) {
        PostFilter postFilter = new PostFilter(cityId)
                .setUserId(userId)
                .setTags(tags)
                .setFollowingFlag(followingFlag)
                .setMinRank(minRank)
                .setMaxRank(maxRank);
        mapData.getMapPosts(cityId, postFilter, new SetMapPosts());
    }

    @Override
    public void getInsights(String cityId, TagEnum tag) {

        mapData.getInsights(cityId, tag, new SetInsights());
    }



    private class SetMapPosts implements ResponseListener<List<MapPostModel>> {

        @Override
        public void onSuccess(List<MapPostModel> mapPosts) {
            mapView.addPostToMap(mapPosts);
            Log.d("Debug", "[MAP] Success getting map posts ");
        }

        @Override
        public void onCancel() {
            Log.d("Debug", "[MAP] onCancel getting map posts ");
        }

        @Override
        public void onError() {
            Log.d("Debug", "[MAP] Error getting map posts ");

        }
    }

    private class SetInsights implements ResponseListener<List<WeightedLatLng>> {

        @Override
        public void onSuccess(List<WeightedLatLng> object) {
            mapView.addHeatMap(object);
            Log.d("Debug", "[MAP] Success getting map Insights ");
        }

        @Override
        public void onCancel() {
            Log.d("Debug", "[MAP] OnCancel getting map insights ");

        }

        @Override
        public void onError() {
            Log.d("Debug", "[MAP] Error getting map insights ");
        }
    }
}
