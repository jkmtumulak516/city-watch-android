package com.sofeng.citywatch.dashboard.profile.tabs.citypost;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.auth.FirebaseAuth;
import com.sofeng.citywatch.R;
import com.sofeng.citywatch.dashboard.IDashboardView;
import com.sofeng.citywatch.dashboard.feed.BaseFeedView;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileCityPostFragment
        extends BaseFeedView
        implements IProfileCityPostView {


    private IProfileCityPostPresenter profilePostPresenter;
    private String cityId;
    private String userId;

    public ProfileCityPostFragment() {
        // Required empty public constructor
    }

    public static ProfileCityPostFragment newInstance() {
        ProfileCityPostFragment fragment = new ProfileCityPostFragment();
        Bundle b = new Bundle();
        fragment.setArguments(b);
        return fragment;
    }

    public static ProfileCityPostFragment newInstance(String userId, String cityId) {
        ProfileCityPostFragment fragment = new ProfileCityPostFragment();
        Bundle b = new Bundle();
        b.putString("cityId", cityId);
        b.putString("userId", userId);
        fragment.setArguments(b);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        profilePostPresenter = new ProfileCityPostPresenter(this);
        Bundle b = this.getArguments();
        if (b.getString("userId") != null && b.getString("cityId") != null) {
            userId = b.getString("userId");
            cityId = b.getString("cityId");
        } else {
            userId = FirebaseAuth.getInstance().getCurrentUser().getUid();
            cityId = null;
        }
    }


    @Override
    protected void init() {
        if (cityId == null) {
            cityId = ((IDashboardView) getActivity()).getCityId();
        }
        refreshFeed();
    }

    @Override
    protected void refreshFeed() {
        if (getActivity() instanceof IDashboardView)
            cityId = ((IDashboardView) getActivity()).getCityId();
        profilePostPresenter.updateFeed(cityId, userId);
    }
}
