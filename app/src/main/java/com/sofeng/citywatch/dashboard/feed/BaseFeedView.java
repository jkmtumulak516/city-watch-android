package com.sofeng.citywatch.dashboard.feed;

import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.clans.fab.FloatingActionMenu;
import com.sofeng.citywatch.R;
import com.sofeng.citywatch.dashboard.IDashboardView;
import com.sofeng.citywatch.model.PostModel;
import com.sofeng.citywatch.post.PostAdapter;

import java.util.List;

/**
 * Created by JKMT on 09/24/2017.
 */

public abstract class BaseFeedView extends Fragment {

    protected RecyclerView.LayoutManager mLayoutManager;
    protected RecyclerView mRecyclerView;
    protected SwipeRefreshLayout swipeContainer;
    protected ConstraintLayout loadingLayout;
    protected ConstraintLayout emptyLayout;

    private List<PostModel> oldPosts;
    private List<PostModel> newPosts;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_watch_feed, container, false);
        loadingLayout = (ConstraintLayout) rootView.findViewById(R.id.feed_loading_layout);
        emptyLayout = (ConstraintLayout) rootView.findViewById(R.id.feed_empty);
        oldPosts = null;
        newPosts = null;
        setupRecyclerView(rootView);
        setupSwipeContainer(rootView);
        integrateFab();
        init();
        return rootView;
    }

    public void setupRecyclerView(View v) {

        mRecyclerView = (RecyclerView) v.findViewById(R.id.feed_recycler);

        if (mRecyclerView != null) {
            mRecyclerView.setHasFixedSize(true);
        }

        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
    }

    private void setupSwipeContainer(View v) {
        swipeContainer = (SwipeRefreshLayout) v.findViewById(R.id.feed_refresh);
        swipeContainer.setColorSchemeResources(R.color.colorPrimary);
        // Setup refresh listener which triggers new data loading
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            public void onRefresh() {
                Log.d("Debug", "[FEED] Refreshing feed...");
                refreshFeed();
            }

        });
    }

    public void integrateFab() {
        final FloatingActionMenu myFab = (FloatingActionMenu) getActivity().findViewById(R.id.floatingActionButton);
        if (myFab != null) {
            mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    if (dy > 0) {
                        // Scroll Down
                        if (!myFab.isMenuButtonHidden()) {
                            myFab.hideMenuButton(true);
                        }
                    } else if (dy < 0) {
                        // Scroll Up
                        if (myFab.isMenuButtonHidden()) {
                            myFab.showMenuButton(true);
                        }
                    }
                }

            });
        }

    }

    public void hideLoadingLayout() {
        loadingLayout.setVisibility(View.GONE);
    }

    public List<PostModel> getPostList() {
        return oldPosts;
    }

    public void updateRecyclerView(List<PostModel> posts) {
        oldPosts = newPosts;
        newPosts = posts;
        if(posts.size() <= 0) {
            showEmpty();
        }else{
            hideEmpty();
        }
        Log.d("Debug", "[FEED] Loading items...");
        PostAdapter postAdapter = new PostAdapter(posts);
        mRecyclerView.setAdapter(postAdapter);
        postAdapter.notifyDataSetChanged();
        swipeContainer.setRefreshing(false);
    }

    public void showEmpty(){
        emptyLayout.setVisibility(View.VISIBLE);
    }

    public void hideEmpty(){
        emptyLayout.setVisibility(View.GONE);
    }

    protected abstract void init();

    protected abstract void refreshFeed();

}
