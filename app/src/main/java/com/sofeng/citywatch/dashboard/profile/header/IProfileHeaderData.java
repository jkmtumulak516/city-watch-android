package com.sofeng.citywatch.dashboard.profile.header;

import android.support.annotation.NonNull;

import com.google.android.gms.common.api.ResolvingResultCallbacks;
import com.google.firebase.database.ValueEventListener;
import com.sofeng.citywatch.model.UserModel;
import com.sofeng.citywatch.net.ResponseListener;

/**
 * Created by JKMT on 08/05/2017.
 */

public interface IProfileHeaderData {

    /**
     * Gets the user and then executes listener.
     * @param userId Id of the user being retrieved.
     * @param listener Listener to execute once user has been retrieved.
     */
    void getUser(String userId, ResponseListener<UserModel> listener);

    /**
     * Gets the user's follower count.
     * @param userId User's Id.
     */
    void getFollowerCount(String userId, ResponseListener<Double> listener);

    /**
     * Changes the user's display name.
     * @param userId User'd Id.
     * @param displayName New display name.
     */
    void editDisplayName(String userId, String displayName);

    /**
     * Changes the user's avatar.
     * @param userId User's Id.
     * @param avatar New avatar.
     */
    void editAvatar( String userId, int avatar);

    /**
     * Checks if a user follows another user.
     * @param followingId The follower's Id.
     * @param followedById The one being followed Id.
     * @param listener Listener to execute.
     */
    void isFollowing(@NonNull String followingId, @NonNull String followedById, final ResponseListener<Boolean> listener);

    /**
     * Makes a user follow another user.
     * @param followingId The User Id of the user who is following the other user.
     * @param followedById The User Id of the user who is being followed by the other user.
     */
    void follow(String followingId, String followedById);

    /**
     * Makes a user unfollow another user.
     * @param followingId The User Id of the user who is following the other user.
     * @param followedById The User Id of the user who is being followed by the other user.
     */
    void unfollow(String followingId, String followedById);
}
