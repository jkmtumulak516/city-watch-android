package com.sofeng.citywatch.dashboard;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.sofeng.citywatch.model.CityModel;
import com.sofeng.citywatch.model.UserModel;
import com.sofeng.citywatch.net.ResponseListener;
import com.sofeng.citywatch.util.TimeService;

/**
 * Created by JKMT on 09/02/2017.
 */

public class DashboardData implements IDashboardData {

    private DatabaseReference rootReference;

    public DashboardData() {

        rootReference = FirebaseDatabase.getInstance().getReference();

        TimeService.startTime();
    }

    @Override
    public void getCurrentUser(String userId, final ResponseListener<UserModel> listener) {

        DatabaseReference userReference = rootReference.child("users").child(userId);

        userReference.addListenerForSingleValueEvent(new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                UserModel userModel = dataSnapshot.getValue(UserModel.class);

                listener.onSuccess(userModel);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

                listener.onCancel();
            }
        });
    }

    @Override
    public void setCurrentCity(String userId, String cityId) {

        DatabaseReference userReference = rootReference.child("users").child(userId);

        userReference.child("currentCity").setValue(cityId);
    }

    @Override
    public void onDestroy() {
        TimeService.stopTime();
    }
}
