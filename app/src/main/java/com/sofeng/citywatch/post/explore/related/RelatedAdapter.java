package com.sofeng.citywatch.post.explore.related;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sofeng.citywatch.R;
import com.sofeng.citywatch.model.CommentModel;
import com.sofeng.citywatch.model.PostModel;
import com.sofeng.citywatch.post.explore.comment.card.CommentCardViewHolder;
import com.sofeng.citywatch.post.explore.related.card.RelatedCardViewHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by JKMT on 09/19/2017.
 */

public class RelatedAdapter extends RecyclerView.Adapter<RelatedCardViewHolder> {

    private List<PostModel> postModels;

    public RelatedAdapter(List<PostModel> comments) {
        this.postModels = comments;
    }

    public void setPostModels(List<PostModel> postModels) {
        this.postModels = postModels;
    }

    @Override
    public RelatedCardViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View rootView = LayoutInflater.from(parent.getContext()).inflate(R.layout.related_card, parent, false);

        return new RelatedCardViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(RelatedCardViewHolder holder, int position) {

        PostModel postModel = postModels.get(position);

        holder.setView(postModel);
    }

    @Override
    public int getItemCount() {
        return postModels != null ? postModels.size() : 0;
    }


}
