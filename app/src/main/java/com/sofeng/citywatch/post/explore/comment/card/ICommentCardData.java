package com.sofeng.citywatch.post.explore.comment.card;

import com.sofeng.citywatch.model.UserModel;
import com.sofeng.citywatch.net.ResponseListener;

/**
 * Created by JKMT on 09/19/2017.
 */

public interface ICommentCardData {
    /**
     * Gets a user's information and then passes the information into the listener.
     * @param userId The user's id.
     * @param listener The listener on response.
     */
    void getUserInfo(final String userId, final ResponseListener<UserModel> listener);
}
