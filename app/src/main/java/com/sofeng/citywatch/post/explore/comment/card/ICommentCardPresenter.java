package com.sofeng.citywatch.post.explore.comment.card;

/**
 * Created by JKMT on 09/19/2017.
 */

public interface ICommentCardPresenter {

    /**
     * Gets the user information from the server
     * @param userId
     */
    void getUser(String userId);


}
