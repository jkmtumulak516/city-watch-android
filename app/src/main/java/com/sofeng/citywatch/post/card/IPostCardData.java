package com.sofeng.citywatch.post.card;

import com.sofeng.citywatch.model.UserModel;
import com.sofeng.citywatch.model.VoteEnum;
import com.sofeng.citywatch.net.ResponseListener;

/**
 * Created by JKMT on 09/19/2017.
 */

public interface IPostCardData {
    /**
     * Gets a user's information and then passes the information into the listener.
     * @param userId The user's id.
     * @param listener The listener on response.
     */
    void getUserInfo(final String userId, final ResponseListener<UserModel> listener);

    /**
     * Upvotes or downvotes a post as the given user.
     * @param userID The user's id.
     * @param postId The post's id.
     * @param cityID The city's id.
     * @param desiredState The desired vote state, either Up or Down.
     * @param currentState The current vote state of the user, either Up, Down, or None.
     */
    void vote(String userID, String postId, String cityID, VoteEnum desiredState, VoteEnum currentState);
}
