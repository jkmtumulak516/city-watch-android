package com.sofeng.citywatch.post.card;

import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.klinker.android.simple_videoview.SimpleVideoView;
import com.sofeng.citywatch.R;
import com.sofeng.citywatch.dashboard.profile.ProfilePreviewActivity;
import com.sofeng.citywatch.model.AvatarEnum;
import com.sofeng.citywatch.model.PostModel;
import com.sofeng.citywatch.model.RankEnum;
import com.sofeng.citywatch.model.UserModel;
import com.sofeng.citywatch.model.VoteEnum;
import com.sofeng.citywatch.post.explore.ExploreActivity;
import com.sofeng.citywatch.util.ImageLoader;
import com.sofeng.citywatch.util.Time;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import me.originqiu.library.EditTag;

/**
 * Created by JKMT on 09/19/2017.
 */

public class PostCardViewHolder
        extends RecyclerView.ViewHolder
        implements IPostCardView {

    private IPostCardPresenter postCardPresenter;
    private View view;
    ImageView userPicture;
    TextView userRank;
    TextView userName;
    FrameLayout btnDownvote;
    ImageView btnDownvoteActive;
    FrameLayout btnUpvote;
    ImageView btnUpvoteActive;
    EditTag postTags;
    TextView postVoteCount;
    CardView postCard;
    TextView postTime;

    String currentUser;
    VoteEnum currentVoteState;


    public PostCardViewHolder(View itemView) {
        super(itemView);
        this.view = itemView;
        postCardPresenter = new PostCardPresenter(this);
        currentUser = FirebaseAuth.getInstance().getCurrentUser().getUid();
    }

    public void setView(final PostModel post) {
        userPicture = (ImageView) view.findViewById(R.id.post_user_picture);
        userRank = (TextView) view.findViewById(R.id.post_rank);
        userName = (TextView) view.findViewById(R.id.post_user_name);
        TextView userMood = (TextView) view.findViewById(R.id.post_user_mood);
        TextView userLocation = (TextView) view.findViewById(R.id.post_location);
        LinearLayout mediaContainer = (LinearLayout) view.findViewById(R.id.post_media_layout);
        ImageView mediaImage = (ImageView) view.findViewById(R.id.post_media_image);
        final SimpleVideoView mediaVideo = (SimpleVideoView) view.findViewById(R.id.post_media_video);
        TextView postTitle = (TextView) view.findViewById(R.id.related_post_title);
        TextView postDescription = (TextView) view.findViewById(R.id.related_post_description);
        postTags = (EditTag) view.findViewById(R.id.related_post_tags);
        btnDownvote = (FrameLayout) view.findViewById(R.id.btn_downvote);
        btnDownvoteActive = (ImageView) view.findViewById(R.id.btn_downvote_active);
        btnUpvote = (FrameLayout) view.findViewById(R.id.btn_upvote);
        btnUpvoteActive = (ImageView) view.findViewById(R.id.btn_upvote_active);
        postVoteCount = (TextView) view.findViewById(R.id.tv_votecount);
        postCard = (CardView) view.findViewById(R.id.post_card);
        postTime = (TextView) view.findViewById(R.id.post_time);

        postCardPresenter.getUser(post.userId);
        //-----------------------------------------------------------------------------------------
        //Mood
        userMood.setText("is feeling " + post.mood);
        userLocation.setText(post.locationName);
        //Media stuff
        if (post.mediaType != PostModel.NONE) {
            if (post.mediaType == PostModel.IMAGE) {
                mediaImage.setVisibility(View.VISIBLE);
                mediaVideo.setVisibility(View.GONE);
                ImageLoader.getInstance().loadImageFromUrl(view.getContext(),
                        post.mediaUrl,
                        mediaImage);
            } else {
                mediaImage.setVisibility(View.GONE);
                mediaVideo.setVisibility(View.VISIBLE);
                mediaVideo.start(post.mediaUrl);
                mediaContainer.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (mediaVideo.isPlaying())
                            mediaVideo.pause();
                        else
                            mediaVideo.play();
                    }
                });

            }
        } else {
            mediaContainer.setVisibility(View.GONE);
        }
        //Title
        postTitle.setText(post.title);
        //Description
        postDescription.setText(post.description);
        //Time
        postTime.setText(Time.getInstance().getDate(post.timestamp));
        //Tags
        postTags.setTagList(Arrays.asList(post.tags.keySet().toArray(new String[0])));
        postTags.setEditable(false);
        //Vote Count
        postVoteCount.setText(((int) post.voteCount) + "");
        //Vote State
        currentVoteState = getVoteState(post);
        Log.d("Debug", "[VOTE-CURRENT]: " + currentVoteState.value);
        setupInitialVoteState(post);
        setupVoteListener(post);
        setPostCardListener(post);

        userPicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!post.userId.equals(FirebaseAuth.getInstance().getCurrentUser().getUid())) {
                    Intent i = new Intent(view.getContext(), ProfilePreviewActivity.class);
                    i.putExtra("userId", post.userId);
                    i.putExtra("cityId", post.cityId);
                    view.getContext().startActivity(i);
                }
            }
        });

    }

    public void setCardClickable(boolean flag) {
        postCard.setClickable(flag);
    }

    public void setPostCardListener(final PostModel postModel) {
        postCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(view.getContext(), ExploreActivity.class);
                i.putExtra("cityId", postModel.cityId);
                i.putExtra("postId", postModel.id);
                view.getContext().startActivity(i);
            }
        });
    }

    @Override
    public void setUserInfo(UserModel user) {
        ImageLoader.getInstance().loadImageFromUrl(view.getContext(),
                AvatarEnum.getAvatar(user.avatar).url, userPicture);
        //Rank
        userRank.setText(RankEnum.getRank(user.rank).name);
        //Name
        userName.setText(user.displayName);


    }

    /**
     * Resets the tags
     */
    public void resetTags() {
        if (postTags != null) {
            List<String> tags = postTags.getTagList();
            postTags.removeTag(tags.toArray(new String[tags.size()]));

        }
    }

    public void resetTagsBrute() {
        if (postTags != null) {
            Iterator<String> i = postTags.getTagList().iterator();
            while (i.hasNext()) {
                postTags.removeTag(i.next());
            }
        }
    }

    /**
     * Sets up the listener for each vote button
     *
     * @param postModel
     */
    private void setupVoteListener(final PostModel postModel) {
        View.OnClickListener voteListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                VoteEnum resultEnum;
                switch (view.getId()) {
                    case R.id.btn_upvote:
                        resultEnum = VoteEnum.Up.resolve(currentVoteState);
                        Log.d("Debug", "[VOTE-UP] current: " + currentVoteState.value + "  result: " + resultEnum.value);
                        postCardPresenter.votePost(currentUser, postModel.id, postModel.cityId,
                                resultEnum, currentVoteState);
                        changeVoteCount(currentVoteState, resultEnum);
                        setVoteState(resultEnum);
                        break;
                    case R.id.btn_downvote:
                        resultEnum = VoteEnum.Down.resolve(currentVoteState);
                        Log.d("Debug", "[VOTE-DOWN] current: " + currentVoteState.value + "  result: " + resultEnum.value);
                        postCardPresenter.votePost(currentUser, postModel.id, postModel.cityId,
                                resultEnum, currentVoteState);
                        changeVoteCount(currentVoteState, resultEnum);
                        setVoteState(resultEnum);
                        break;
                }
            }
        };


        btnDownvote.setOnClickListener(voteListener);
        btnUpvote.setOnClickListener(voteListener);

    }

    /**
     * Sets up the explore if the current user already upvoted/downvoted on the post
     *
     * @param postModel
     */
    private void setupInitialVoteState(PostModel postModel) {
        setVoteState(currentVoteState);
    }


    /**
     * Sets vote button to None
     */
    public void noneVoteState() {
        btnUpvoteActive.setVisibility(View.INVISIBLE);
        btnDownvoteActive.setVisibility(View.INVISIBLE);
    }

    /**
     * Sets vote button to Up
     */
    public void upVoteState() {
        btnUpvoteActive.setVisibility(View.VISIBLE);
        btnDownvoteActive.setVisibility(View.INVISIBLE);
    }

    /**
     * Sets vote button to Down
     */
    public void downVoteState() {
        btnUpvoteActive.setVisibility(View.INVISIBLE);
        btnDownvoteActive.setVisibility(View.VISIBLE);
    }

    /**
     * Gets the initial vote state of the post
     */
    private VoteEnum getVoteState(PostModel postModel) {
        VoteEnum state = VoteEnum.None;

        if (postModel.votes != null) {
            if (postModel.votes.containsKey(currentUser)) {
                state = VoteEnum.getVoteEnum(postModel.votes.get(currentUser));
            }
        }

        return state;
    }

    /**
     * Changes the vote button based on result
     *
     * @param result To be based on
     */
    public void setVoteState(VoteEnum result) {
        if (result.value == VoteEnum.Up.value) {
            upVoteState();
            currentVoteState = VoteEnum.Up;
        } else if (result.value == VoteEnum.Down.value) {
            downVoteState();
            currentVoteState = VoteEnum.Down;
        } else {
            noneVoteState();
            currentVoteState = VoteEnum.None;
        }
    }

    /**
     * Changes the vote count in the ui according to currentVoteState and the desiredState
     *
     * @param currentVoteState
     * @param result
     */
    public void changeVoteCount(VoteEnum currentVoteState, VoteEnum result) {
        int voteCount = Integer.parseInt(postVoteCount.getText().toString());
        switch (currentVoteState) {
            case None:
                if (result == VoteEnum.Up) {
                    voteCount++;
                } else if (result == VoteEnum.Down) {
                    voteCount--;
                }
                break;
            case Up:
                if (result == VoteEnum.None) {
                    voteCount--;
                } else if (result == VoteEnum.Down) {
                    voteCount -= 2;
                }
                break;
            case Down:
                if (result == VoteEnum.None) {
                    voteCount++;
                } else if (result == VoteEnum.Up) {
                    voteCount += 2;
                }
                break;
        }
        postVoteCount.setText(voteCount + "");

    }

}
