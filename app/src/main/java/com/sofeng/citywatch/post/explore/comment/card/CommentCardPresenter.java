package com.sofeng.citywatch.post.explore.comment.card;

import com.sofeng.citywatch.model.UserModel;
import com.sofeng.citywatch.net.ResponseListener;

/**
 * Created by JKMT on 09/19/2017.
 */

public class CommentCardPresenter implements ICommentCardPresenter {

    private ICommentCardView commentCardView;
    private ICommentCardData commentCardData;

    public CommentCardPresenter(ICommentCardView commentCardView) {

        this.commentCardView = commentCardView;

        commentCardData = new CommentCardData();
    }

    @Override
    public void getUser(String userId) {
        commentCardData.getUserInfo(userId, new UserListener());
    }

    class UserListener implements ResponseListener<UserModel>{

        @Override
        public void onSuccess(UserModel object) {
            commentCardView.setUserInfo(object);
        }

        @Override
        public void onCancel() {

        }

        @Override
        public void onError() {

        }
    }
}
