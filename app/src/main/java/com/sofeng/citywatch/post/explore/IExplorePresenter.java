package com.sofeng.citywatch.post.explore;

/**
 * Created by JKMT on 09/19/2017.
 */

public interface IExplorePresenter {

    /**
     * Request to server to load the post
     * @param cityId Id of the city where the post belongs
     * @param postId Id of the post
     */
    void loadPost(String cityId, String postId);

    /**
     * Loads the comments of the post
     * @param cityId Id of the city where the post belongs
     * @param postId Id of the post
     */
    void loadComments(String cityId, String postId);

    /**
     * Loads the current user
     * @param userId Id of the current user
     */
    void loadUser(String userId);

    /**
     * Requests to server to upload a comment
     * @param cityID Id of the city where the post belongs
     * @param postId Id ofthe post
     * @param userId Id of the current user
     * @param text Content of the comment
     */
    void uploadComment(String cityID, String postId, String userId, String text);

    /**
     * Request to server to load related posts
     * @param cityId
     * @param postId
     */
    void loadRelatedPosts(String cityId, String postId);
}
