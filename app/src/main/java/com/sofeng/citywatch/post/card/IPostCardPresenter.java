package com.sofeng.citywatch.post.card;

import com.sofeng.citywatch.model.VoteEnum;

/**
 * Created by JKMT on 09/19/2017.
 */

public interface IPostCardPresenter {

    /**
     * Gets the user information from the server
     * @param userId
     */
    public void getUser(String userId);

    /**
     * Requests to vote a post
     * @param userId Current user
     * @param postId Id of the post
     * @param cityId Id of the current city
     * @param desiredState Desired vote state
     * @param currentState Current vote state
     */
    public void votePost(String userId, String postId, String cityId,
                     VoteEnum desiredState, VoteEnum currentState);
}
