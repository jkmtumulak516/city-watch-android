package com.sofeng.citywatch.post.explore;

import android.content.Context;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.sofeng.citywatch.R;
import com.sofeng.citywatch.model.AvatarEnum;
import com.sofeng.citywatch.model.CommentModel;
import com.sofeng.citywatch.model.PostModel;
import com.sofeng.citywatch.model.RankEnum;
import com.sofeng.citywatch.model.UserModel;
import com.sofeng.citywatch.post.card.PostCardViewHolder;
import com.sofeng.citywatch.post.explore.comment.CommentAdapter;
import com.sofeng.citywatch.post.explore.related.RelatedAdapter;
import com.sofeng.citywatch.util.ImageLoader;

import java.util.List;

public class ExploreActivity
        extends AppCompatActivity
        implements IExploreView {

    private IExplorePresenter viewPostPresenter;

    private CardView postCard;
    private RecyclerView commentRecyclerView;
    private RecyclerView relatedRecyclerView;
    private LinearLayout relatedLayout;
    private ImageView commentAvatar;
    private TextView commentRank;
    private Button commentPost;
    private TextView commentText;
    protected SwipeRefreshLayout swipeContainer;
    private PostCardViewHolder viewHolder;

    private String cityId;
    private String postId;
    private String userId;

    private List<CommentModel> oldComments;
    private List<CommentModel> newComments;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_explore);

        viewPostPresenter = new ExplorePresenter(this);

        cityId = getIntent().getExtras().getString("cityId");
        postId = getIntent().getExtras().getString("postId");
        userId = FirebaseAuth.getInstance().getCurrentUser().getUid();

        postCard = (CardView) findViewById(R.id.post_card);
        setupCommentRecycler();
        setupRelatedRecycler();
        setupSwipeContainer();

        commentAvatar = (ImageView) findViewById(R.id.explore_comment_avatar);
        commentRank = (TextView) findViewById(R.id.explore_comment_rank);
        commentPost = (Button) findViewById(R.id.explore_comment_post);
        commentText = (TextView) findViewById(R.id.explore_comment_text);

        commentPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String text = commentText.getText().toString();
                CommentModel commentModel = new CommentModel();
                commentModel.text = text;
                commentModel.userId = userId;
                commentModel.timestamp = -1;
                commentText.setText("");
                hideSoftKeyboard(ExploreActivity.this, view);
                updateLastCommentRecyclerView(commentModel);
                viewPostPresenter.uploadComment(cityId, postId, userId, text);

            }
        });

       refreshAll();

    }

    private void refreshAll(){
        viewPostPresenter.loadUser(userId);
        viewPostPresenter.loadPost(cityId, postId);
        viewPostPresenter.loadComments(cityId, postId);
        viewPostPresenter.loadRelatedPosts(cityId, postId);
    }

    private void setupSwipeContainer() {
        swipeContainer = (SwipeRefreshLayout) findViewById(R.id.explore_refresh);
        swipeContainer.setColorSchemeResources(R.color.colorPrimary);
        // Setup refresh listener which triggers new data loading
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            public void onRefresh() {
                Log.d("Debug", "[EXPLORE] Refreshing...");
                refreshAll();
                swipeContainer.setRefreshing(false);
            }

        });
    }

    private void setupCommentRecycler() {
        commentRecyclerView = (RecyclerView) findViewById(R.id.explore_comment_recycler);

        if (commentRecyclerView != null) {
            commentRecyclerView.setHasFixedSize(true);
        }

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        DividerItemDecoration mDividerItemDecoration = new DividerItemDecoration(
                commentRecyclerView.getContext(),
                DividerItemDecoration.VERTICAL
        );
        commentRecyclerView.setNestedScrollingEnabled(false);
        commentRecyclerView.setLayoutManager(mLayoutManager);
        commentRecyclerView.addItemDecoration(mDividerItemDecoration);
    }

    private void setupRelatedRecycler() {
        relatedLayout = (LinearLayout) findViewById(R.id.explore_related_layout);
        relatedLayout.setVisibility(View.GONE);
        relatedRecyclerView = (RecyclerView) findViewById(R.id.explore_related_recycler);

        if (relatedRecyclerView != null) {
            relatedRecyclerView.setHasFixedSize(true);
        }

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        mLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);

        relatedRecyclerView.setNestedScrollingEnabled(false);
        relatedRecyclerView.setLayoutManager(mLayoutManager);
    }


    public static void hideSoftKeyboard (AppCompatActivity activity, View view)
    {
        InputMethodManager imm = (InputMethodManager)activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getApplicationWindowToken(), 0);
    }

    public void updateLastCommentRecyclerView(CommentModel commentModel){
        CommentAdapter adapter = (CommentAdapter) commentRecyclerView.getAdapter();
        adapter.addComment(commentModel);
        adapter.notifyItemInserted(adapter.getItemCount() - 1);
    }

    @Override
    public void updatePostCard(PostModel postModel) {
        if(viewHolder == null)
            viewHolder = new PostCardViewHolder(postCard);
        viewHolder.resetTags();
        viewHolder.setView(postModel);
        viewHolder.setCardClickable(false);
    }

    @Override
    public void updateCommentRecyclerView(List<CommentModel> commentModelList) {
        oldComments = newComments;
        newComments = commentModelList;
        CommentAdapter commentAdapter = new CommentAdapter(commentModelList);
        commentRecyclerView.setAdapter(commentAdapter);
        commentAdapter.notifyDataSetChanged();
    }

    @Override
    public void updateRelatedPostsRecyclerView(List<PostModel> relatedPosts) {
        relatedLayout.setVisibility(View.VISIBLE);
        RelatedAdapter relatedAdapter = new RelatedAdapter(relatedPosts);
        relatedRecyclerView.setAdapter(relatedAdapter);
        relatedAdapter.notifyDataSetChanged();
    }

    @Override
    public void updateCurrentUser(UserModel userModel) {
        ImageLoader.getInstance().loadImageFromUrl(this, AvatarEnum.getAvatar(userModel.avatar).url,
                commentAvatar);
        commentRank.setText(RankEnum.getRank(userModel.rank).name);
    }

    @Override
    public List<CommentModel> getCommentList() {
        return oldComments;
    }

    @Override
    public Context getContext() {
        return getBaseContext();
    }
}
