package com.sofeng.citywatch.post.card;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.sofeng.citywatch.model.UserModel;
import com.sofeng.citywatch.model.VoteEnum;
import com.sofeng.citywatch.net.ResponseListener;
import com.sofeng.citywatch.util.FirebaseUtil;

/**
 * Created by JKMT on 09/19/2017.
 */

public class PostCardData implements IPostCardData {

    private DatabaseReference rootReference;

    private DatabaseReference oldUserReference;
    private ValueEventListener oldUserListener;

    public PostCardData() {

        rootReference = FirebaseDatabase.getInstance().getReference();
    }

    @Override
    public void getUserInfo(final String userId, final ResponseListener<UserModel> listener) {
        // sets the database reference of user
        DatabaseReference userReference = rootReference.child("users").child(userId);

        // defines an anonymous implementation of a ValueEventListener
        ValueEventListener valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // simply converts the dataSnapshot into a UserModel
                UserModel user = dataSnapshot.getValue(UserModel.class);

                // then passes it into the listener
                listener.onSuccess(user);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

                listener.onCancel();
            }
        };
        // adds the listener to the user reference
        userReference.addListenerForSingleValueEvent(valueEventListener);

        // disposes the old listener
        FirebaseUtil.disposeListener(oldUserReference, oldUserListener);

        // sets the current reference and value event listener
        // so it can be disposed of later, similar to the above
        oldUserReference = userReference;
        oldUserListener = valueEventListener;
    }

    @Override
    public void vote(String userID, String postId, String cityID, VoteEnum desiredState, VoteEnum currentState) {
        // gets the predetermined result of the two vote states
        VoteEnum result = desiredState.resolve(currentState);

        // sets the database reference for the vote
        DatabaseReference voteReference = rootReference.child("posts").child(cityID)
                .child("content").child(postId)
                .child("votes").child(userID);

        // sets the vote's value to the result above
        voteReference.setValue(result.value);
    }
}
