package com.sofeng.citywatch.post.explore;

import com.sofeng.citywatch.model.CommentModel;
import com.sofeng.citywatch.model.PostModel;
import com.sofeng.citywatch.model.UserModel;
import com.sofeng.citywatch.net.ResponseListener;

import java.util.List;

/**
 * Created by JKMT on 09/19/2017.
 */

public interface IExploreData {
    /**
     * Gets a specific post and then executes the listener.
     * @param cityId The city's id the post belongs to.
     * @param postId The post's id.
     * @param listener The listener on response.
     */
    void getPost(String cityId, String postId, final ResponseListener<PostModel> listener);

    /**
     * Gets a specific user and then executes the listener.
     * @param userId The user's id.
     * @param listener The listener on response.
     */
    void getUser(String userId, final ResponseListener<UserModel> listener);

    /**
     * Gets the comments that belong to a post.
     * @param cityId The city's id.
     * @param postId The post's id.
     * @param listener The listener on response.
     */
    void getComments(String cityId, String postId, final ResponseListener<List<CommentModel>> listener);

    /**
     * Adds a comment to the specified post under the given user's id.
     * @param cityID The city's id.
     * @param postId The post's id.
     * @param userId The user's id.
     * @param text The comment's text.
     */
    void addComment(String cityID, String postId, String userId, String text);

    /**
     * Synchronizes or desynchronizes the user's that own a post in the list of given comments.
     * @param comments List of comments.
     * @param value Value which determines synchronization or desynchronization.
     */
    void syncUsers(List<CommentModel> comments, boolean value);

    /**
     * Gets the related posts set by the user
     * @param cityId The city's id.
     * @param postId The post's id.
     * @param listener The listener on response.
     */
    void getRelatedPosts(String cityId, String postId, final ResponseListener<List<PostModel>> listener);
}
