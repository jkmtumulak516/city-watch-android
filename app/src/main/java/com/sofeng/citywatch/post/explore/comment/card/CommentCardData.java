package com.sofeng.citywatch.post.explore.comment.card;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.sofeng.citywatch.model.UserModel;
import com.sofeng.citywatch.net.ResponseListener;
import com.sofeng.citywatch.util.FirebaseUtil;

/**
 * Created by JKMT on 09/19/2017.
 */

public class CommentCardData implements ICommentCardData {

    private DatabaseReference rootReference;

    private DatabaseReference oldUserReference;
    private ValueEventListener oldUserListener;

    public CommentCardData() {

        rootReference = FirebaseDatabase.getInstance().getReference();
    }

    @Override
    public void getUserInfo(final String userId, final ResponseListener<UserModel> listener) {
        // sets the database reference of user
        DatabaseReference userReference = rootReference.child("users").child(userId);

        // defines an anonymous implementation of a ValueEventListener
        ValueEventListener valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // simply converts the dataSnapshot into a UserModel
                UserModel user = dataSnapshot.getValue(UserModel.class);

                // then passes it into the listener
                listener.onSuccess(user);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

                listener.onCancel();
            }
        };
        // adds the listener to the user reference
        userReference.addListenerForSingleValueEvent(valueEventListener);

        // disposes the old listener
        FirebaseUtil.disposeListener(oldUserReference, oldUserListener);

        // sets the current reference and value event listener
        // so it can be disposed of later, similar to the above
        oldUserReference = userReference;
        oldUserListener = valueEventListener;
    }
}
