package com.sofeng.citywatch.post.explore;

import android.content.Context;

import com.sofeng.citywatch.model.CommentModel;
import com.sofeng.citywatch.model.PostModel;
import com.sofeng.citywatch.model.UserModel;

import java.util.List;

/**
 * Created by JKMT on 09/19/2017.
 */

public interface IExploreView {

    /**
     * Updates the post card with the details in post model
     * @param postModel Post to be put in the post card
     */
    void updatePostCard(PostModel postModel);

    /**
     * Updates the recyclerview of comments in the post
     * @param commentModelList List of Comments
     */
    void updateCommentRecyclerView(List<CommentModel> commentModelList);

    /**
     * Updates the recyclerview of the related posts of the posts
     * @param relatedPosts
     */
    void updateRelatedPostsRecyclerView(List<PostModel> relatedPosts);

    /**
     * Updates the current user viewing the post
     * @param userModel User
     */
    void updateCurrentUser(UserModel userModel);

    /**
     * Gets the old comment list for desynchronization
     * @return List of comments
     */
    List<CommentModel> getCommentList();

    /**
     * Gets the view's context.
     * @return A context.
     */
    Context getContext();
}
