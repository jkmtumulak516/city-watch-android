package com.sofeng.citywatch.post.explore.comment;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sofeng.citywatch.R;
import com.sofeng.citywatch.model.CommentModel;
import com.sofeng.citywatch.post.explore.comment.card.CommentCardViewHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by JKMT on 09/19/2017.
 */

public class CommentAdapter extends RecyclerView.Adapter<CommentCardViewHolder> {

    private List<CommentModel> comments;

    public CommentAdapter(List<CommentModel> comments) {
        this.comments = comments;
    }

    public void setComments(List<CommentModel> comments) {
        this.comments = comments;
    }

    @Override
    public CommentCardViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View rootView = LayoutInflater.from(parent.getContext()).inflate(R.layout.post_comment, parent, false);

        return new CommentCardViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(CommentCardViewHolder holder, int position) {

        CommentModel comment = comments.get(position);

        holder.setView(comment);
    }

    @Override
    public int getItemCount() {
        return comments != null ? comments.size() : 0;
    }

    public void addComment(CommentModel commentModel) {
        if (comments == null)
            comments = new ArrayList<CommentModel>();

        comments.add(commentModel);
    }
}
