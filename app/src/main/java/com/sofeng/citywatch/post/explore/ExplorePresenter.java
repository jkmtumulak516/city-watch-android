package com.sofeng.citywatch.post.explore;

import android.util.Log;

import com.sofeng.citywatch.model.CommentModel;
import com.sofeng.citywatch.model.PostModel;
import com.sofeng.citywatch.model.UserModel;
import com.sofeng.citywatch.net.ResponseListener;

import java.util.List;

/**
 * Created by JKMT on 09/19/2017.
 */

public class ExplorePresenter implements IExplorePresenter {

    private IExploreView exploreView;
    private IExploreData exploreData;

    public ExplorePresenter(IExploreView viewPostView) {

        this.exploreView = viewPostView;

        exploreData = new ExploreData(viewPostView.getContext());
    }

    @Override
    public void loadPost(String cityId, String postId) {
        exploreData.getPost(cityId, postId, new PostListener());
    }

    @Override
    public void loadComments(String cityId, String postId) {
        exploreData.getComments(cityId, postId, new CommentListener());
    }

    @Override
    public void loadUser(String userId) {
        exploreData.getUser(userId, new UserListener());
    }

    @Override
    public void uploadComment(String cityID, String postId, String userId, String text) {
        exploreData.addComment(cityID, postId, userId, text);
    }

    @Override
    public void loadRelatedPosts(String cityId, String postId) {
        exploreData.getRelatedPosts(cityId, postId, new RelatedListener());

    }

    class PostListener implements ResponseListener<PostModel> {

        @Override
        public void onSuccess(PostModel object) {
            exploreView.updatePostCard(object);
        }

        @Override
        public void onCancel() {

        }

        @Override
        public void onError() {

        }
    }

    class CommentListener implements ResponseListener<List<CommentModel>> {

        @Override
        public void onSuccess(List<CommentModel> object) {
            exploreData.syncUsers(exploreView.getCommentList(), false);
            exploreData.syncUsers(object, true);
            exploreView.updateCommentRecyclerView(object);
        }

        @Override
        public void onCancel() {

        }

        @Override
        public void onError() {

        }
    }

    class UserListener implements ResponseListener<UserModel> {

        @Override
        public void onSuccess(UserModel object) {
            exploreView.updateCurrentUser(object);
        }

        @Override
        public void onCancel() {

        }

        @Override
        public void onError() {

        }
    }

    class RelatedListener implements ResponseListener<List<PostModel>> {

        @Override
        public void onSuccess(List<PostModel> object) {
            if (object != null) {
                if (!object.isEmpty()) {
                    exploreView.updateRelatedPostsRecyclerView(object);
                    Log.d("Debug", "[EXPLORE-RELATED] has related posts");
                } else
                    Log.d("Debug", "[EXPLORE-RELATED] No related posts");
            } else {
                Log.d("Debug", "[EXPLORE-RELATED] No related posts");
            }
        }

        @Override
        public void onCancel() {
            Log.d("Debug", "[EXPLORE-RELATED] on cancel");
        }

        @Override
        public void onError() {
            Log.d("Debug", "[EXPLORE-RELATED] on error");
        }
    }
}
