package com.sofeng.citywatch.post;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sofeng.citywatch.R;
import com.sofeng.citywatch.model.PostModel;
import com.sofeng.citywatch.post.card.PostCardViewHolder;

import java.util.List;

/**
 * Created by Francis on 26/07/2017.
 */

public class PostAdapter extends RecyclerView.Adapter<PostCardViewHolder> {

    private List<PostModel> posts;

    public PostAdapter(List<PostModel> postList){
        this.posts = postList;
    }

    public void setPosts(List<PostModel> posts) {
        this.posts = posts;
    }

    @Override
    public PostCardViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        // @TODO change layout as needed
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.post_card,parent,false);

        return new PostCardViewHolder(v);
    }

    @Override
    public void onBindViewHolder(PostCardViewHolder holder, int position) {

        PostModel post = posts.get(position);

        holder.setView(post);
    }

    @Override
    public int getItemCount() {
        return posts != null ? posts.size() : 0;
    }

    @Override
    public void onViewRecycled(PostCardViewHolder holder) {
        holder.resetTags();
        super.onViewRecycled(holder);

    }
}