package com.sofeng.citywatch.post.explore.related.card;

import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sofeng.citywatch.R;
import com.sofeng.citywatch.model.AvatarEnum;
import com.sofeng.citywatch.model.CommentModel;
import com.sofeng.citywatch.model.PostModel;
import com.sofeng.citywatch.model.RankEnum;
import com.sofeng.citywatch.model.UserModel;
import com.sofeng.citywatch.post.explore.ExploreActivity;
import com.sofeng.citywatch.post.explore.comment.card.CommentCardPresenter;
import com.sofeng.citywatch.post.explore.comment.card.ICommentCardPresenter;
import com.sofeng.citywatch.post.explore.comment.card.ICommentCardView;
import com.sofeng.citywatch.util.ImageLoader;
import com.sofeng.citywatch.util.Time;

import java.util.Arrays;

import me.originqiu.library.EditTag;

/**
 * Created by JKMT on 09/19/2017.
 */

public class RelatedCardViewHolder
        extends RecyclerView.ViewHolder{

    private View view;
    private TextView relatedTitle;
    private TextView relatedDescription;
    private EditTag relatedTags;
    private CardView relatedCard;



    public RelatedCardViewHolder(View itemView) {
        super(itemView);
        view = itemView;
    }


    public void setView(final PostModel postModel) {
        relatedTitle = (TextView) view.findViewById(R.id.related_post_title);
        relatedDescription = (TextView) view.findViewById(R.id.related_post_description);
        relatedTags = (EditTag) view.findViewById(R.id.related_post_tags);
        relatedCard = (CardView) view.findViewById(R.id.related_post_card);


        relatedCard.setClickable(true);
        relatedCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(view.getContext(), ExploreActivity.class);
                i.putExtra("cityId", postModel.cityId);
                i.putExtra("postId", postModel.id);
                view.getContext().startActivity(i);
            }
        });
        relatedTitle.setText(postModel.title);
        relatedDescription.setText(postModel.description);
        relatedTags.setEditable(false);
        relatedTags.setTagList(Arrays.asList(postModel.tags.keySet().toArray(new String[0])));

    }

}
