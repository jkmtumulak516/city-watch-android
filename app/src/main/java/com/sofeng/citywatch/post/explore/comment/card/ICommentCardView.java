package com.sofeng.citywatch.post.explore.comment.card;

import com.sofeng.citywatch.model.CommentModel;
import com.sofeng.citywatch.model.UserModel;

/**
 * Created by JKMT on 09/19/2017.
 */

public interface ICommentCardView {

    /**
     * Sets the view to the details of comment
     * @param comment Comment to be displayed
     */
    void setView(CommentModel comment);

    /**
     * Sets the user info in the view
     * @param user User to be displayed
     */
    void setUserInfo(UserModel user);
}
