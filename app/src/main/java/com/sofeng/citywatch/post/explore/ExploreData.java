package com.sofeng.citywatch.post.explore;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.Transaction;
import com.google.firebase.database.ValueEventListener;
import com.sofeng.citywatch.model.CommentModel;
import com.sofeng.citywatch.model.PostModel;
import com.sofeng.citywatch.model.UserModel;
import com.sofeng.citywatch.model.filter.PostFilter;
import com.sofeng.citywatch.net.Endpoints;
import com.sofeng.citywatch.net.MultipleObjectsRequest;
import com.sofeng.citywatch.net.ResponseListener;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

/**
 * Created by JKMT on 09/19/2017.
 */

public class ExploreData implements IExploreData {

    private static final String requestTag = "EXPLORE";

    private RequestQueue requestQueue;

    private DatabaseReference rootReference;

    public ExploreData(Context context) {

        rootReference = FirebaseDatabase.getInstance().getReference();

        requestQueue = Volley.newRequestQueue(context);
    }

    @Override
    public void getPost(String cityId, String postId, final ResponseListener<PostModel> listener) {
        // sets the database reference to the specified post
        DatabaseReference postReference = rootReference.child("posts").child(cityId).child("content").child(postId);

        // defines an anonymous ValueEventListener
        ValueEventListener valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // convert dataSnapshot into a PostModel
                PostModel post = dataSnapshot.getValue(PostModel.class);
                // passes converted dataSnapshot as PostModel into the listener
                listener.onSuccess(post);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

                listener.onCancel();
            }
        };

        postReference.addListenerForSingleValueEvent(valueEventListener);
    }

    @Override
    public void getUser(String userId, final ResponseListener<UserModel> listener) {
        // sets the database reference to the specified user
        DatabaseReference userReference = rootReference.child("users").child(userId);

        // defines an anonymous ValueEventListener
        ValueEventListener valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // convert dataSnapshot into a PostModel
                UserModel user = dataSnapshot.getValue(UserModel.class);
                // passes converted dataSnapshot as PostModel into the listener
                listener.onSuccess(user);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

                listener.onCancel();
            }
        };

        userReference.addListenerForSingleValueEvent(valueEventListener);
    }

    @Override
    public void getComments(String cityId, String postId, final ResponseListener<List<CommentModel>> listener) {
        // sets the database reference to the comments of a particular post
        DatabaseReference commentReference = rootReference.child("posts").child(cityId).child("comments").child(postId);

        // defines an anonymous ValueEventListener
        ValueEventListener valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // convert dataSnapshot into a PostModel
                GenericTypeIndicator<Map<String, CommentModel>> type = new GenericTypeIndicator<Map<String, CommentModel>>() {
                };
                if (dataSnapshot.getValue(type) != null) {
                    List<CommentModel> comments = new ArrayList<>(dataSnapshot.getValue(type).values());

                    // sorts the new list from earliest to latest
                    Collections.sort(comments, new CommentComparator());

                    // passes the mew ;ost into the listener
                    listener.onSuccess(comments);
                }else {
                    listener.onSuccess(new ArrayList<CommentModel>());
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

                listener.onCancel();
            }
        };

        commentReference.addListenerForSingleValueEvent(valueEventListener);
    }

    @Override
    public void addComment(String cityId, String postId, String userId, String text) {
        // sets the database reference to a new address for the upcoming comment
        DatabaseReference commentReference = rootReference.child("posts").child(cityId).child("comments").child(postId).push();
        // initializes comment
        CommentModel comment = new CommentModel();

        // sets the comment's properties to the properties given
        comment.id = commentReference.getKey();
        comment.postId = postId;
        comment.userId = userId;
        comment.text = text;

        // runs the transaction to put the comment into the database
        commentReference.runTransaction(new PutCommentTransaction(comment));
    }

    @Override
    public void syncUsers(List<CommentModel> comments, boolean value) {
        // sets the base reference of all users
        DatabaseReference userReference = FirebaseDatabase.getInstance().getReference().child("users");

        // iterates over all comments, gets the userId of a comment, and then
        // syncs or desyncs based on the param value
        if(comments != null) {
            for (CommentModel comment : comments)
                userReference.child(comment.userId).keepSynced(value);
        }
    }

    @Override
    public void getRelatedPosts(String cityId, String postId, final ResponseListener<List<PostModel>> listener) {
        // sets the endpoint's url
        String baseUrl = Endpoints.Posts.RELATED_POSTS;
        String url = baseUrl + "?cityId=" + cityId + "&postId=" + postId;

        MultipleObjectsRequest<PostFilter> postsRequest = new MultipleObjectsRequest<>(Request.Method.GET, url, null, new Response.Listener<Map>() {
            @Override
            public void onResponse(Map response) {

                List<PostModel> posts = PostModel.convertMultiple((List<Map>) response.get("relatedPosts"));

                listener.onSuccess(posts);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                listener.onError();
            }
        });

        postsRequest.setTag(requestTag);

        requestQueue.add(postsRequest);
    }

    private class PutCommentTransaction implements Transaction.Handler {

        private final CommentModel comment;

        public PutCommentTransaction(CommentModel comment) {

            this.comment = comment;
        }

        @Override
        public Transaction.Result doTransaction(MutableData mutableData) {
            // puts the comment into the database
            mutableData.setValue(comment);
            // puts the comment's timestamp
            Log.d("Debug", "[SERVER TIME] " + ServerValue.TIMESTAMP);
            mutableData.child("timestamp").setValue(ServerValue.TIMESTAMP);

            // finishes the transaction
            return Transaction.success(mutableData);
        }

        @Override
        public void onComplete(DatabaseError databaseError, boolean b, DataSnapshot dataSnapshot) {

            //throw databaseError.toException();
        }
    }

    private class CommentComparator implements Comparator<CommentModel> {
        @Override
        public int compare(CommentModel c1, CommentModel c2) {
            return (int) (c1.timestamp - c2.timestamp);
        }
    }
}
