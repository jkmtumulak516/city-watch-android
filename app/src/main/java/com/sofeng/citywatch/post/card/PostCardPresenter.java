package com.sofeng.citywatch.post.card;

import com.sofeng.citywatch.model.UserModel;
import com.sofeng.citywatch.model.VoteEnum;
import com.sofeng.citywatch.net.ResponseListener;

/**
 * Created by JKMT on 09/19/2017.
 */

public class PostCardPresenter implements IPostCardPresenter {

    private IPostCardView postCardView;
    private IPostCardData postCardData;

    public PostCardPresenter(IPostCardView postCardView) {

        this.postCardView = postCardView;

        postCardData = new PostCardData();
    }

    @Override
    public void getUser(String userId) {
        postCardData.getUserInfo(userId, new UserListener());
    }

    @Override
    public void votePost(String userId, String postId, String cityId, VoteEnum desiredState, VoteEnum currentState) {
        postCardData.vote(userId, postId, cityId, desiredState, currentState);
    }

    class UserListener implements ResponseListener<UserModel>{

        @Override
        public void onSuccess(UserModel object) {
            postCardView.setUserInfo(object);
        }

        @Override
        public void onCancel() {

        }

        @Override
        public void onError() {

        }
    }

}
