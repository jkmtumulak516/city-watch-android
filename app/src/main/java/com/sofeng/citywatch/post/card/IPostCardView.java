package com.sofeng.citywatch.post.card;

import com.sofeng.citywatch.model.PostModel;
import com.sofeng.citywatch.model.UserModel;

/**
 * Created by JKMT on 09/19/2017.
 */

public interface IPostCardView {

    /**
     * Sets the view to the details of post
     * @param post Post to be displayed
     */
    void setView(PostModel post);

    /**
     * Sets the user info in the view
     * @param user User to be displayed
     */
    void setUserInfo(UserModel user);

}
