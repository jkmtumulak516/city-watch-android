package com.sofeng.citywatch.post.explore.comment.card;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.sofeng.citywatch.R;
import com.sofeng.citywatch.model.AvatarEnum;
import com.sofeng.citywatch.model.CommentModel;
import com.sofeng.citywatch.model.RankEnum;
import com.sofeng.citywatch.model.UserModel;
import com.sofeng.citywatch.util.ImageLoader;
import com.sofeng.citywatch.util.Time;

/**
 * Created by JKMT on 09/19/2017.
 */

public class CommentCardViewHolder
        extends RecyclerView.ViewHolder
        implements ICommentCardView {

    private ICommentCardPresenter commentCardPresenter;
    private View view;
    private ImageView commentAvatar;
    private TextView commentRank;
    private TextView commentText;
    private TextView commentName;
    private TextView commentTime;


    public CommentCardViewHolder(View itemView) {
        super(itemView);
        view = itemView;
        commentCardPresenter = new CommentCardPresenter(this);
    }

    @Override
    public void setView(CommentModel comment) {
        commentAvatar = (ImageView) view.findViewById(R.id.explore_comment_avatar);
        commentRank = (TextView) view.findViewById(R.id.explore_comment_rank);
        commentName = (TextView) view.findViewById(R.id.comment_name);
        commentText = (TextView) view.findViewById(R.id.explore_comment_text);
        commentTime = (TextView) view.findViewById(R.id.comment_time);

        commentText.setText(comment.text);
        if(comment.timestamp != -1) {
            commentTime.setText(Time.getInstance().getDate(comment.timestamp));
        }else{
            commentTime.setText("Just now");
        }

        commentCardPresenter.getUser(comment.userId);
    }

    @Override
    public void setUserInfo(UserModel user) {
        ImageLoader.getInstance().loadImageFromUrl(view.getContext(), AvatarEnum.getAvatar(user.avatar).url,
                commentAvatar);
        commentName.setText(user.displayName);
        commentRank.setText(RankEnum.getRank(user.rank).name);
    }
}
