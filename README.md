# CityWatch #

# Version 0.1 #
An application that builds and strengthens community awareness within a local city.

### Contribution Guidelines ###
 1. You can only merge in master through pull requests
 2. Please commit as much as possible
 3. Do not create tags without approval
